This is a library of various utilities that is shared between my various CK2 scripting efforts.  

It's in the middle of a refactoring, and might eventually be released to pypi, but for now it's 
distributed as an embedded library in the projects that use it, such as Ck2AuotBuild.

It requires the Jinja2 library to run; currently only a few features require Jinja2, 
but in the future more features will require it.

Current library features fit into the broad categories described below.

Low level code generation helpers:
Various classes/modules intended to make generating mods easier, mostly
under the code folder.  An example is Make_Event, which creates a new empty
event and sets reasonable defaults for things that can be overriden.  With the
switch to Jinja2 templates in the AutoBuild/GiveMinors rewrites, these generators
might see sweeping changes in the future, though the basic functionality will 
probably remain.

Parsing & CK2 Language Constructs:
A set of classes, modules, and functions for reading vanilla and/or mod content
and interacting with it.  The parser is currently under utilities, is a bit of 
a mess, and should probably be rewritten using something like antlr or pyparsing.
The language constructs are both low level (e.g. Block, Assignment, etc) and mid
level (e.g. Building, Technology, Minor Title, etc).  The low level constructs
are a bit of a mess, and I've toyed with several variations on them that aren't
actively used.  The lower level constructs might eventually be replaced with 
equivalent, but cleaner code.  It's possible that the mid level constructs might 
eventually be merged with code generation classes in order to simplify usage.

Mod Generation Library:
A set of classes and functions that take some of the repetitiveness out of 
creating the actual physical mod files and structure.  This is in the modbase
folder, and understands things like where to put events and how to deal with
namespaces.  

Sets & Graph Class:
This is a group of classes that takes a list of mid level constructs (currently 
buildings, landed titles, and traits) and sort them into groups based on their
features.  As an example, AutoBuild makes use of this to answer questions such
as "how many buildings require a given technology."  This will probably be 
expanded to other constructs in the future.  There's also a simple graph class
that understands how to generate dot and gml files, and is mostly used for 
debugging.

Miscellaneous:
Not really a feature set, per sey, but this library has been around for awhile
as a personal dumping ground for experiments (such as low level constructs based
off namedtuples, or that use SQL as a back end) that haven't quite been excised
from the library.  As refactoring continues (and the library approaches the level
of user friendliness where releasing it on pypi might be appropriate)  they will
either be incorporated or removed.


    