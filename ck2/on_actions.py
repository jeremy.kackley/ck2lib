from collections import OrderedDict
from code import base
import copy
import constructs.basic as basic
import utilities.parse_functions as pf

class On_Actions_Events:

    def __init__(self, name):
        self.name = name
        self.clear()

    def clear(self):
        self.events = []
        self.random_events = []

    def process(self, block):
        # print ck2.pretty(block)
        for section in block:
            if hasattr(section, '__name__'):
                try:
                    for event in section:
                        if section.__name__ == 'events':
                            self.events.append(event)
                        else:
                            self.random_events.append(event)
                except:
                    pass
        self.random_events.reverse()
        self.events.reverse()

    def add_event(self, event_id, comment=None):
        if comment:
            comment = basic.Comment(text=comment, parent=None)
        self.events.append(basic.Literal(text=event_id,
                                       comment=comment,
                                       parent=None))

    def add_random_event(self, probability, event_id, comment=None):
        #         value = base.assign(probability, event_id)
        #         if comment:
        #             value = value + '#{}'.format(comment)
        value = basic.ASSIGNMENT(probability,
                               event_id,
                               comment)
        self.random_events.append(value)

    def delete_event(self, event_id):
        to_remove = []
        for thing in self.events:
            if thing.text == event_id:
                to_remove.append(thing)
        for thing in to_remove:
            self.events.remove(thing)
        to_remove = []
        for thing in self.random_events:
            if thing.value == event_id:
                to_remove.append(thing)
        for thing in to_remove:
            self.random_events.remove(thing)

    def __str__(self):
        ret = ''
        if self.events:
            ret += base.make_b('events', '\n'.join((str(x)
                                                    for x in self.events)))
        if self.random_events:
            ret += base.make_b('random_events',
                               '\n'.join((str(x) for x in self.random_events)))
        return base.make_b(self.name, ret)


class On_Actions:

    def __init__(self):
        self.onactions = OrderedDict()

    def load_path(self, path):
        for thing in pf.parse_onactions(path):
            # if thing.__name__ not in self.onactions:
            # always replace, because of the way on_actions work...
            self.onactions[thing.__name__] = On_Actions_Events(thing.__name__)
            self.onactions[thing.__name__].process(thing)

    def get(self, block):
        if block not in self.onactions:
            self.onactions[block] = On_Actions_Events(block)
        return copy.deepcopy(self.onactions[block])

    def update(self, other):
        for block in other.onactions:
            otherblock = other.onactions[block]
            self.onactions[block] = copy.deepcopy(otherblock)
        # self.onactions.update(other.onactions)

    def __str__(self):
        ret = ''
        for thing in self.onactions:
            ret += base.make_b(thing, str(self.onactions[thing]))
        return ret
