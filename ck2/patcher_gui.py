# implement a generic GUI for the patchers...
#    create an .ini to store directories and options...
#        CK2 directory
#        CK2 mod directory
#        Output Directory
#           [if not set, disable all other buttons?]
#        compress output y/n
import ConfigParser
from Tkinter import Frame, Label, Button, Tk, TclError
from threading import Thread
# from there, disable a table of mods, along with parsed information,
#    along with a 'vanilla' option
#    with a refresh button.
#    refresh should parse the mods and check if they are valid.


class patcher_app(Frame):

    def __init__(self, master=None, patcher=None):
        Frame.__init__(self, master)
        self.patcher = patcher
        self.pack()
        # used for refresh tasks and such.
        self.worker = None

        # these are widgets that are actually defined in createWidgets() below
        self.TimerBar = None
        self.MenuBar = None
        self.createWidgets()
        # self.after(500,self.update)
    # def update(self):
#        self.after(500,self.update)

    def createWidgets(self):
        self.__create_menubar__()

        MainArea = Frame(self)
        MainArea.pack({"side": "left"})
        # i need a label, a text box, and browse button
#        managerbar = Frame(MainArea)
#        managerbar.pack({"side": "top"})
#         self.datasources = []
#         for label in ["Explorers", "TagHistory", "Scout", "TagManager", "Looter", "Periodic", "Reactive"]:
#             datasource = eval("self.datasource." + label)
#             self.datasources.append(datasource)
#             self.managers[label] = ManagerGui(managerbar, label, datasource)
#             self.managers[label]["borderwidth"] = 1
#             self.managers[label]["relief"] = "groove"
#             self.managers[label].pack(
#                 {"side": "left", "expand": "yes", "fill": "both"})
#         self.TimerBar = TimerBar(MainArea, self.datasources)
#         self.TimerBar.pack({"side": "top"})

    def __create_menubar__(self):
        self.MenuBar = Frame(self)
        self.MenuBar.pack({"side": "left", "expand": "yes", "fill": "both"})

        QUIT = Button(self.MenuBar)
        QUIT["text"] = "QUIT"
        QUIT["fg"] = "red"
        QUIT["command"] = self.quit
        QUIT.pack({"side": "top"})

#     def loot(self):
#         if not self.LootThread or not self.LootThread.is_alive():
#             # if not hasattr(self, "LootThread") or not self.LootThread.is_alive():
#             # only start looting if i'm not already looting..
#             self.LootThread = Thread(target=self.__loot_thread__, args=())
#             self.LootThread.start()


class patcher_gui_base:

    def __init__(self, patcher_name):
        '''
        Initialzie the config file, among other things.
        :param patcher_name: used in case multiple patchers are ran from the
        same directory, e.g. autobuild, etc
        '''
        self.config = ConfigParser.ConfigParser()
        self.config.read('{}.ini'.format(patcher_name))

    # overloaded functions
    def is_mod_patchable(self):
        '''
        This function should be called with parsed mod information and
        return True if the patch
        in question is applicable to the mod.
        '''
        return False

    def mod_needs_patch(self):
        '''
        This function is called prior to parsing a mod to apply per-patcher
        exceptions, for instance the oddly named mercenary group in Lux Invicta.
        '''
        return False
