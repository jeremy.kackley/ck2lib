# heres an idea...
#    scopes = premade 'socpe' blocks
#    cmd = premade 'command' blocks
#    cond = premade 'condition' blocks
from ..lru_cache_backport_recipe import lfu_cache
from _common import make_b, assign, log

from ScriptedBaseContainer import ScriptedBaseContainer as SCB
from ScriptedBaseType import make_scripted_object

from Make_Dec import Make_Dec
from Make_Event import Make_Event
from Make_Building import Make_Building

ScriptedTriggers = SCB
ScriptedEffects = SCB


def character_event(aid, desc=None, picture='', namespace=None,comment = ''):
    return Make_Event('character_event', aid, desc, picture, namespace,comment)


def province_event(aid, desc=None, picture='', namespace=None,comment = ''):
    return Make_Event('province_event', aid, desc, picture, namespace,comment)


def make_case(conditions, actions, default=None):
    ret = ''
    for cond, act in zip(conditions, actions):
        other = [x for x in conditions
                 if x != cond]
        orl = or_list(other)
        norl = make_b('NOT', orl)
        ret += make_if(cond + norl, act)
    if default:
        norl = make_b('NOT', or_list(cond))
        ret += make_if(norl, default)
    return ret


def make_scope(name, limit, string):
    limstr = ''
    if limit:
        limstr = make_b('limit', limit)
    return make_b(name, limstr + string)

# here's an idea...
# instead of a string, return a class...
# maybe subclassed off OrderedDict


def make_if_else(limit, ifblock, elseblock):
    '''
    Returns IF = {limit = limit} ifblock}
            IF = {limit=NOT={limit} elseblock}
    '''
    return make_if(limit, ifblock) + make_if_not(limit, elseblock)


def make_if_not(limit, string):
    return make_if(make_b('NOT', limit), string)


def make_if(limit, string):
    '''
    Returns IF{ limit = {} string }
    :param limit: block text defining condition.
    :param string: block text to be executed.
    '''
    lim_block = make_b('limit', limit)
    if '\n' not in limit:
        lim_block = lim_block.replace('\n', ' ')
    string = str(string)
    return make_b('IF', lim_block + '\n' + string)


def set_event_target(event_target_string):
    return assign('save_event_target_as', event_target_string)


def make_event_target(event_target_string, content):
    return make_b('event_target:{}'.format(event_target_string), content)


def do_only_one(conditions, actions):
    '''
    Returns IF {
        limit = { condition[x]
                  NOT = { conditions[x+1:]
                          conditions[:x-1]
                         }
                  }
                actions[0]
                }
    ....
    :param conditions:
    :param actions:
    '''
    ret = ''
    for index in range(0, len(actions)):
        pos = conditions[index]
        neg = ''
        # print index, index + 1, len(conditions), index - 1, len(conditions) >
        # (index + 1), (index - 1) >= 0
        if len(conditions) >= (index + 1):
            neg += '\n'.join(conditions[index + 1:])
        if index > 0:
            neg += '\n'.join(conditions[:index])
        ret += make_if(pos + make_b('NOT', neg), actions[index])
    return ret


def or_list(a_list):
    ret = '\n'.join(str(x) for x in list(a_list))
    return make_b('OR', ret)


def and_list(a_list):
    ret = '\n'.join(str(x) for x in list(a_list))
    return make_b('AND', ret)


def not_list(a_list):
    ret = '\n'.join(str(x) for x in list(a_list))
    return make_b('NOT', ret)


def trigger(event_type, event_id, period='', tooltip='', random=''):
    # events can be triggered instantly.
    if period and '=' not in str(period):
        period = assign('days', period)
    if tooltip:
        tooltip = assign('tooltip', tooltip)
    if random:
        random = assign('random', random)
    body = assign('id', event_id) + period + tooltip
    return make_b(event_type, body)

# aliases:
IF = make_if
IFNOT = make_if_not
IFELSE = make_if_else
Event = Make_Event
CharEvent = character_event
ProvEvent = province_event
Decision = Make_Dec
scope = make_scope
Building = Make_Building
