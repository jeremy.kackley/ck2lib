
from ..lru_cache_backport_recipe import lfu_cache


@lfu_cache(maxsize=5000)
def assign(var, value):
    '''
    Returns var = value
    :param var: variable name.
    :param value: Variable value.
    '''
    return '{} = {} \n'.format(var, value)


@lfu_cache(maxsize=5000)
def make_b(cmd, string):
    '''
    Returns cmd = { string }
    :param cmd: command name.
    :param string: block text.
    '''
    block = cmd + ' = {\n' + string + '\n}\n'
    if len(string) < 100:  # and string.count('\n')<=4:
        # no need to do return lines if only a few things.
        if '#' not in string:
            # can't mess with comments.
            return block.replace('\n', ' ')+'\n'
    return block


def log(name, string):
    return assign('log', '"{}: '.format(name)+string+'"')
