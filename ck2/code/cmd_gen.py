# -*- coding: utf-8 -*-
from base import assign


def abandon_heresy(value):
    '''
        abandon_heresy = value
        from: character
abandon_heresy = yes

    '''
    return assign('abandon_heresy', value)


def abdicate(value):
    '''
        abdicate = value
        from: character
abdicate to current heir
abdicate = yes

    '''
    return assign('abdicate', value)


def abdicate_to(value):
    '''
        abdicate_to = value

    '''
    return assign('abdicate_to', value)


def abdicate_to_most_liked_by(value):
    '''
        abdicate_to_most_liked_by = value

    '''
    return assign('abdicate_to_most_liked_by', value)


def activate_plot(value):
    '''
        activate_plot = value

    '''
    return assign('activate_plot', value)


def add_ambition(value):
    '''
        add_ambition = value
        from: character
add_ambition = obj_become_chancellor

    '''
    return assign('add_ambition', value)


def add_betrothal(value):
    '''
        add_betrothal = value

    '''
    return assign('add_betrothal', value)


def add_building(value):
    '''
        add_building = value

    '''
    return assign('add_building', value)


def add_claim(value):
    '''
        add_claim = value
        from: title
add a strong, non-inheritable claim to character
county = { add_claim = ROOT }

    '''
    return assign('add_claim', value)


def add_consort(value):
    '''
        add_consort = value
        from: character

    '''
    return assign('add_consort', value)


def add_law(value):
    '''
        add_law = value
        from: title
random_demesne_title = { add_law = centralization_0 }

    '''
    return assign('add_law', value)


def add_law_w_cooldown(value):
    '''
        add_law_w_cooldown = value
        any_demesne_title = { add_law_w_cooldown = investiture_law_0 }

    '''
    return assign('add_law_w_cooldown', value)


def add_lover(value):
    '''
        add_lover = value
        from: character
add_lover = PREV

    '''
    return assign('add_lover', value)


def add_number_to_name(value):
    '''
        add_number_to_name = value

    '''
    return assign('add_number_to_name', value)


def add_objective(value):
    '''
        add_objective = value

    '''
    return assign('add_objective', value)


def add_piety_modifier(value):
    '''
        add_piety_modifier = value

    '''
    return assign('add_piety_modifier', value)


def add_pressed_claim(value):
    '''
        add_pressed_claim = value
        from: title
add a strong, inheritable claim
primary_title = { add_pressed_claim = ROOT }

    '''
    return assign('add_pressed_claim', value)


def add_prestige_modifier(value):
    '''
        add_prestige_modifier = value

    '''
    return assign('add_prestige_modifier', value)


def add_spouse(value):
    '''
        add_spouse = value
        from: character
add_spouse = FROM

    '''
    return assign('add_spouse', value)


def add_spouse_matrilineal(value):
    '''
        add_spouse_matrilineal = value
        from: character
add_spouse_matrilineal = FROM

    '''
    return assign('add_spouse_matrilineal', value)


def add_trait(value):
    '''
        add_trait = value
        Adds a trait to the character, using an ID from the traits files
add_trait = skilled_tactician

    '''
    return assign('add_trait', value)


def add_weak_claim(value):
    '''
        add_weak_claim = value
        add a weak, non-inheritable claim

    '''
    return assign('add_weak_claim', value)


def add_weak_pressed_claim(value):
    '''
        add_weak_pressed_claim = value
        add a weak, inheritable claim
add_weak_pressed_claim = FROM

    '''
    return assign('add_weak_pressed_claim', value)


def approve_law(value):
    '''
        approve_law = value
        from: character, title
primary_title = { approve_law = city_tax_0 }

    '''
    return assign('approve_law', value)


def back_plot(value):
    '''
        back_plot = value
        from: character
ROOT = { back_plot = PREV }

    '''
    return assign('back_plot', value)


def banish(value):
    '''
        banish = value
        from: character
banish character to random court
FROM = { banish = yes }

    '''
    return assign('banish', value)


def banish_religion(value):
    '''
        banish_religion = value
        from: character
all characters of the targeted religion will be banished to random court
banish_religion = jewish

    '''
    return assign('banish_religion', value)


def become_heretic(value):
    '''
        become_heretic = value
        from: character
changes religion to random heresy of current religion
become_heretic = yes

    '''
    return assign('become_heretic', value)


def break_betrothal(value):
    '''
        break_betrothal = value

    '''
    return assign('break_betrothal', value)


def cancel_ambition(value):
    '''
        cancel_ambition = value
        from: character
cancel_ambition = yes

    '''
    return assign('cancel_ambition', value)


def cancel_job_action(value):
    '''
        cancel_job_action = value
        from: character
FROM = { cancel_job_action = action_inquisition }

    '''
    return assign('cancel_job_action', value)


def cancel_objective(value):
    '''
        cancel_objective = value

    '''
    return assign('cancel_objective', value)


def cancel_plot(value):
    '''
        cancel_plot = value
        from: character
cancel_plot = plot_gain_title

    '''
    return assign('cancel_plot', value)


def change_diplomacy(value):
    '''
        change_diplomacy = value
        from: character
change_diplomacy = 1

    '''
    return assign('change_diplomacy', value)


def change_intrigue(value):
    '''
        change_intrigue = value
        from: character
change_intrigue = 1

    '''
    return assign('change_intrigue', value)


def change_learning(value):
    '''
        change_learning = value
        from: character
change_learning = 1

    '''
    return assign('change_learning', value)


def change_martial(value):
    '''
        change_martial = value
        from: character
change_martial = 2

    '''
    return assign('change_martial', value)


def change_phase_to(value):
    '''
        change_phase_to = value

    '''
    return assign('change_phase_to', value)


def change_random_civ_tech(value):
    '''
        change_random_civ_tech = value

    '''
    return assign('change_random_civ_tech', value)


def change_random_eco_tech(value):
    '''
        change_random_eco_tech = value

    '''
    return assign('change_random_eco_tech', value)


def change_random_mil_tech(value):
    '''
        change_random_mil_tech = value

    '''
    return assign('change_random_mil_tech', value)


def change_stewardship(value):
    '''
        change_stewardship = value
        from: character
change_stewardship = 1

    '''
    return assign('change_stewardship', value)


def check_actor_direct_vassals(value):
    '''
        check_actor_direct_vassals = value

    '''
    return assign('check_actor_direct_vassals', value)


def clear_revolt(value):
    '''
        clear_revolt = value
        clear_revolt = yes

    '''
    return assign('clear_revolt', value)


def clear_wealth(value):
    '''
        clear_wealth = value
        from: character
clear_wealth = yes

    '''
    return assign('clear_wealth', value)


def clr_character_flag(value):
    '''
        clr_character_flag = value
        from: character
clr_character_flag = flag_promised_marriage

    '''
    return assign('clr_character_flag', value)


def clr_dynasty_flag(value):
    '''
        clr_dynasty_flag = value
        from: char'
clr_dynasty_flag = strange_chest

    '''
    return assign('clr_dynasty_flag', value)


def clr_global_flag(value):
    '''
        clr_global_flag = value
        from: any
clr_global_flag = shepherds_crusade_active

    '''
    return assign('clr_global_flag', value)


def clr_province_flag(value):
    '''
        clr_province_flag = value
        from: province
clr_province_flag = tournament_60_days_furusiyya

    '''
    return assign('clr_province_flag', value)


def conquest_culture(value):
    '''
        conquest_culture = value
        from: province
conquest_culture = PREVPREV

    '''
    return assign('conquest_culture', value)


def copy_title_history(value):
    '''
        copy_title_history = value
        from: title
copy_title_history = k_asturias

    '''
    return assign('copy_title_history', value)


def copy_title_laws(value):
    '''
        copy_title_laws = value
        from: title
copy_title_laws = k_asturias

    '''
    return assign('copy_title_laws', value)


def create_character(value):
    '''
        create_character = value
        from: character, province
creates a random character
create_character = { ... }

    '''
    return assign('create_character', value)


def create_random_diplomat(value):
    '''
        create_random_diplomat = value
        from: character, province
creates a random character with diplomacy education
create_random_diplomat = { ... }

    '''
    return assign('create_random_diplomat', value)


def create_random_intriguer(value):
    '''
        create_random_intriguer = value
        from: character, province
creates a random character with intrigue education
create_random_intriguer = { ... }

    '''
    return assign('create_random_intriguer', value)


def create_random_priest(value):
    '''
        create_random_priest = value
        from: character, province
creates a random character with learning education
create_random_priest = { ... }

    '''
    return assign('create_random_priest', value)


def create_random_soldier(value):
    '''
        create_random_soldier = value
        from: character, province
creates a random character with martial education
create_random_soldier = { ... }

    '''
    return assign('create_random_soldier', value)


def create_random_steward(value):
    '''
        create_random_steward = value
        from: character, province
creates a random character with stewardship education
create_random_steward = { ... }

    '''
    return assign('create_random_steward', value)


def culture_techpoints(value):
    '''
        culture_techpoints = value
        from: character
culture_techpoints = 50

    '''
    return assign('culture_techpoints', value)


def cure_illness(value):
    '''
        cure_illness = value
        from: character
cure_illness = yes

    '''
    return assign('cure_illness', value)


def de_jure_liege(value):
    '''
        de_jure_liege = value
        from: title
any_direct_de_jure_vassal_title = { de_jure_liege = e_roman_empire }

    '''
    return assign('de_jure_liege', value)


def decadence(value):
    '''
        decadence = value
        from: character
decadence = -50

    '''
    return assign('decadence', value)


def destroy_landed_title(value):
    '''
        destroy_landed_title = value
        from: title
k_asturias = { destroy_landed_title = THIS }

    '''
    return assign('destroy_landed_title', value)


def destroy_random_building(value):
    '''
        destroy_random_building = value
        from: province
random_demesne_title = { destroy_random_building = yes }

    '''
    return assign('destroy_random_building', value)


def destroy_tradepost(value):
    '''
        destroy_tradepost = value
        from: province
destroy_tradepost = FROMFROM

    '''
    return assign('destroy_tradepost', value)


def disband_event_forces(value):
    '''
        disband_event_forces = value
        from: character
disband_event_forces = prepared_invasion

    '''
    return assign('disband_event_forces', value)


def economy_techpoints(value):
    '''
        economy_techpoints = value
        from: character
economy_techpoints = -50

    '''
    return assign('economy_techpoints', value)


def end_war(value):
    '''
        end_war = value
        from: war
end_war = invalid

    '''
    return assign('end_war', value)


def excommunicate(value):
    '''
        excommunicate = value
        from: character
excommunicate = no

    '''
    return assign('excommunicate', value)


def fertility(value):
    '''
        fertility = value
        from: character
fertility = 0.4

    '''
    return assign('fertility', value)


def gain_all_occupied_titles(value):
    '''
        gain_all_occupied_titles = value

    '''
    return assign('gain_all_occupied_titles', value)


def gain_title(value):
    '''
        gain_title = value
        from: title
random_demesne_title = { gain_title = FROM }

    '''
    return assign('gain_title', value)


def gain_title_plus_barony_if_unlanded(value):
    '''
        gain_title_plus_barony_if_unlanded = value

    '''
    return assign('gain_title_plus_barony_if_unlanded', value)


def give_job_title(value):
    '''
        give_job_title = value
        from: character
give_job_title = job_chancellor

    '''
    return assign('give_job_title', value)


def give_minor_title(value):
    '''
        give_minor_title = value
        from: character
give_minor_title = title_chief_qadi

    '''
    return assign('give_minor_title', value)


def give_nickname(value):
    '''
        give_nickname = value
        from: character
gives the character a nickname
give_nickname = nick_the_great

    '''
    return assign('give_nickname', value)


def gold(value):
    '''
        gold = value

    '''
    return assign('gold', value)


def grant_kingdom_w_adjudication(value):
    '''
        grant_kingdom_w_adjudication = value
        from: title
random_demesne_title = { grant_kingdom_w_adjudication = FROM }

    '''
    return assign('grant_kingdom_w_adjudication', value)


def grant_title(value):
    '''
        grant_title = value
        from: title
random_claim = { grant_title = FROM }

    '''
    return assign('grant_title', value)


def grant_title_no_opinion(value):
    '''
        grant_title_no_opinion = value

    '''
    return assign('grant_title_no_opinion', value)


def health(value):
    '''
        health = value
        from: character
health = -1

    '''
    return assign('health', value)


def impregnate(value):
    '''
        impregnate = value
        from: character
impregnate = ROOT

    '''
    return assign('impregnate', value)


def impregnate_cuckoo(value):
    '''
        impregnate_cuckoo = value
        from: character
impregnate_cuckoo = ROOT

    '''
    return assign('impregnate_cuckoo', value)


def imprison(value):
    '''
        imprison = value
        from: character
scoped character will be imprisoned by target
imprison = ROOT

    '''
    return assign('imprison', value)


def inherit(value):
    '''
        inherit = value
        from: character
scoped character will inherit targets titles
random_courtier = { inherit = ROOT }

    '''
    return assign('inherit', value)


def join_attacker_wars(value):
    '''
        join_attacker_wars = value
        from: character
ROOT = { join_attacker_wars = PREV }

    '''
    return assign('join_attacker_wars', value)


def join_defender_wars(value):
    '''
        join_defender_wars = value
        from: character
FROM = { join_defender_wars = ROOT }

    '''
    return assign('join_defender_wars', value)


def leave_plot(value):
    '''
        leave_plot = value
        from: character
leave_plot = FROM

    '''
    return assign('leave_plot', value)


def make_primary_spouse(value):
    '''
        make_primary_spouse = value
        from: character
scoped character will become primary spouse
FROM = { make_primary_spouse = yes }

    '''
    return assign('make_primary_spouse', value)


def make_primary_title(value):
    '''
        make_primary_title = value
        from: title
k_leon = { make_primary_title = yes }

    '''
    return assign('make_primary_title', value)


def military_techpoints(value):
    '''
        military_techpoints = value
        from: character
military_techpoints = -50

    '''
    return assign('military_techpoints', value)


def move_character(value):
    '''
        move_character = value
        from: character
scoped character will be moved to targeted characters court
FROM = { move_character = ROOT }

    '''
    return assign('move_character', value)


def occupy_minors_of_occupied_settlements(value):
    '''
        occupy_minors_of_occupied_settlements = value
        from: character
occupy_minors_of_occupied_settlements = FROM

    '''
    return assign('occupy_minors_of_occupied_settlements', value)


def piety(value):
    '''
        piety = value
        from: character
piety = 10

    '''
    return assign('piety', value)


def prestige(value):
    '''
        prestige = value
        from: character
prestige = -20

    '''
    return assign('prestige', value)


def random(value):
    '''
        random = value
        from: any
random = { chance = 5 add_trait = wounded }

    '''
    return assign('random', value)


def random_list(value):
    '''
        random_list = value
        from: any
random_list = { 50 = { ... } 50 = { ... } }

    '''
    return assign('random_list', value)


def rebel_defection(value):
    '''
        rebel_defection = value
        from: title
rebel_defection = yes

    '''
    return assign('rebel_defection', value)


def recalc_succession(value):
    '''
        recalc_succession = value
        from: any
recalc_succession = yes

    '''
    return assign('recalc_succession', value)


def reduce_disease(value):
    '''
        reduce_disease = value
        from: province
random_demesne_province = { reduce_disease = 0.33 }

    '''
    return assign('reduce_disease', value)


def remove_building(value):
    '''
        remove_building = value

    '''
    return assign('remove_building', value)


def remove_character_modifier(value):
    '''
        remove_character_modifier = value
        from: character
remove_character_modifier = promised_a_title

    '''
    return assign('remove_character_modifier', value)


def remove_claim(value):
    '''
        remove_claim = value
        from: character
remove_claim = k_papal_state ; any_claim = { remove_claim = PREV }

    '''
    return assign('remove_claim', value)


def remove_friend(value):
    '''
        remove_friend = value
        from: character
remove_friend = FROM

    '''
    return assign('remove_friend', value)


def remove_holding_modifier(value):
    '''
        remove_holding_modifier = value
        from: title
remove_holding_modifier = recently_conquered

    '''
    return assign('remove_holding_modifier', value)


def remove_lover(value):
    '''
        remove_lover = value
        from: character
remove scoped character's lover
remove_lover = yes

    '''
    return assign('remove_lover', value)


def remove_opinion(value):
    '''
        remove_opinion = value
        from: character
remove_opinion = { who = FROM modifier = opinion_friend }

    '''
    return assign('remove_opinion', value)


def remove_province_modifier(value):
    '''
        remove_province_modifier = value
        from: province
remove_province_modifier = thieves_guild

    '''
    return assign('remove_province_modifier', value)


def remove_settlement(value):
    '''
        remove_settlement = value

    '''
    return assign('remove_settlement', value)


def remove_spouse(value):
    '''
        remove_spouse = value
        from: character
remove scoped character's spouse
remove_spouse = spouse

    '''
    return assign('remove_spouse', value)


def remove_title(value):
    '''
        remove_title = value
        from: character
remove_title = job_chancellor

    '''
    return assign('remove_title', value)


def remove_trait(value):
    '''
        remove_trait = value
        from: character
remove_trait = charitable

    '''
    return assign('remove_trait', value)


def reveal_plot(value):
    '''
        reveal_plot = value
        from: character
reveal scoped characters plot
reveal_plot = yes

    '''
    return assign('reveal_plot', value)


def reveal_plot_w_message(value):
    '''
        reveal_plot_w_message = value
        from: character
reveal scoped characters plot
reveal_plot_w_message = yes

    '''
    return assign('reveal_plot_w_message', value)


def reverse_banish(value):
    '''
        reverse_banish = value
        from: character
random_realm_lord = { reverse_banish = FROM }

    '''
    return assign('reverse_banish', value)


def reverse_culture(value):
    '''
        reverse_culture = value
        from: character, province
change the target's culture to the one of current scope
reverse_culture = PREV

    '''
    return assign('reverse_culture', value)


def reverse_imprison(value):
    '''
        reverse_imprison = value
        from: character
FROM = { reverse_imprison = ROOT }

    '''
    return assign('reverse_imprison', value)


def reverse_religion(value):
    '''
        reverse_religion = value
        from: character
reverse_religion = PREV

    '''
    return assign('reverse_religion', value)


def reverse_remove_opinion(value):
    '''
        reverse_remove_opinion = value
        from: character
reverse_remove_opinion = { who = ROOT modifier = opinion_friend }

    '''
    return assign('reverse_remove_opinion', value)


def revoke_law(value):
    '''
        revoke_law = value
        from: title
revoke_law = investiture_law_1

    '''
    return assign('revoke_law', value)


def scaled_wealth(value):
    '''
        scaled_wealth = value
        from: character
scaled_wealth = -0.15

    '''
    return assign('scaled_wealth', value)


def seize_trade_post(value):
    '''
        seize_trade_post = value
        from: province
any_neighbor_province = { seize_trade_post = ROOT }

    '''
    return assign('seize_trade_post', value)


def send_assassin(value):
    '''
        send_assassin = value
        from: character
send_assassin = FROMFROM

    '''
    return assign('send_assassin', value)


def set_allow_free_infidel_revokation(value):
    '''
        set_allow_free_infidel_revokation = value

    '''
    return assign('set_allow_free_infidel_revokation', value)


def set_allow_free_revokation(value):
    '''
        set_allow_free_revokation = value

    '''
    return assign('set_allow_free_revokation', value)


def set_allow_title_revokation(value):
    '''
        set_allow_title_revokation = value

    '''
    return assign('set_allow_title_revokation', value)


def set_appoint_generals(value):
    '''
        set_appoint_generals = value

    '''
    return assign('set_appoint_generals', value)


def set_appoint_regents(value):
    '''
        set_appoint_regents = value

    '''
    return assign('set_appoint_regents', value)


def set_character_flag(value):
    '''
        set_character_flag = value
        from: character
set_character_flag = decadence_drunkard

    '''
    return assign('set_character_flag', value)


def set_defacto_liege(value):
    '''
        set_defacto_liege = value
        from: character
set_defacto_liege = FROM

    '''
    return assign('set_defacto_liege', value)


def set_defacto_vassal(value):
    '''
        set_defacto_vassal = value
        from: character
holder_scope = { set_defacto_vassal = PREVPREV }

    '''
    return assign('set_defacto_vassal', value)


def set_dynasty_flag(value):
    '''
        set_dynasty_flag = value
        from: character
set_dynasty_flag = the_victims

    '''
    return assign('set_dynasty_flag', value)


def set_father(value):
    '''
        set_father = value
        from: character
set_father = PREV

    '''
    return assign('set_father', value)


def set_global_flag(value):
    '''
        set_global_flag = value
        from: any
set_global_flag = west_francia_renamed

    '''
    return assign('set_global_flag', value)


def set_guardian(value):
    '''
        set_guardian = value
        from: character
set_guardian = ROOT

    '''
    return assign('set_guardian', value)


def set_investiture(value):
    '''
        set_investiture = value

    '''
    return assign('set_investiture', value)


def set_mother(value):
    '''
        set_mother = value
        from: character
set_mother = PREV

    '''
    return assign('set_mother', value)


def set_name(value):
    '''
        set_name = value
        from: character, title
b_kolbatz = { set_name = Jomsborg }

    '''
    return assign('set_name', value)


def set_protected_inheritance(value):
    '''
        set_protected_inheritance = value

    '''
    return assign('set_protected_inheritance', value)


def set_province_flag(value):
    '''
        set_province_flag = value
        from: province
set_province_flag = home_of_jeanne_darc

    '''
    return assign('set_province_flag', value)


def set_the_kings_full_peace(value):
    '''
        set_the_kings_full_peace = value
        allow/disallow vassals to wage war
set_the_kings_full_peace = yes

    '''
    return assign('set_the_kings_full_peace', value)


def set_the_kings_peace(value):
    '''
        set_the_kings_peace = value
        allow/disallow vassals to wage war between them
set_the_kings_peace = no

    '''
    return assign('set_the_kings_peace', value)


def steal_random_tech(value):
    '''
        steal_random_tech = value

    '''
    return assign('steal_random_tech', value)


def succession(value):
    '''
        succession = value
        from: title
succession = primogeniture

    '''
    return assign('succession', value)


def succession_w_cooldown(value):
    '''
        succession_w_cooldown = value
        from: title
succession_w_cooldown = primogeniture

    '''
    return assign('succession_w_cooldown', value)


def treasury(value):
    '''
        treasury = value
        from: character
treasury = -20

    '''
    return assign('treasury', value)


def usurp_title(value):
    '''
        usurp_title = value
        from: title
c_pest = { usurp_title = ROOT }

    '''
    return assign('usurp_title', value)


def usurp_title_only(value):
    '''
        usurp_title_only = value
        from: title
usurp_title_only = ROOT

    '''
    return assign('usurp_title_only', value)


def usurp_title_plus_barony_if_unlanded(value):
    '''
        usurp_title_plus_barony_if_unlanded = value
        from: title
plot_target_title = { usurp_title_plus_barony_if_unlanded = PREV }

    '''
    return assign('usurp_title_plus_barony_if_unlanded', value)


def usurp_title_plus_barony_if_unlanded_and_vassals(value):
    '''
        usurp_title_plus_barony_if_unlanded_and_vassals = value
        from: title
targeted character will usurp scoped title
c_byzantion = { usurp_title_plus_barony_if_unlanded_and_vassals = PREV }

    '''
    return assign('usurp_title_plus_barony_if_unlanded_and_vassals', value)


def wealth(value):
    '''
        wealth = value
        from: character
wealth = -50

    '''
    return assign('wealth', value)
