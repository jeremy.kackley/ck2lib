from _common import make_b, assign, log


class Make_Dec:

    def __init__(self, name):
        self.name = name
        self.type = 'decisions'  # can also be vassal_decisions
        #        or dynasty_decisions
        self.from_potential = ''
        self.potential = ''
        self.allow = ''
        self.effect = ''
        self.revoke_allowed = ''#deprecated
        self.ai_will_do = ''
        self.other = ''
        self.desc_loc = ''
        self.name_loc = ''

    def add_loc_to_mod(self, target):
        if self.name_loc:
            target.add_loc(self.name, self.name_loc)
        if self.desc_loc:
            target.add_loc(self.name + '_desc', self.desc_loc)

    def _blockstr(self):
        ret = ''
        if self.from_potential != '':
            ret += make_b('from_potential', self.from_potential)
        if self.potential != '':
            ret += make_b('potential', self.potential)
        if self.allow != '':
            ret += make_b('allow', self.allow)
        ret += make_b('effect', self.effect)
        #if self.revoke_allowed != '':
            #ret += make_b('revoke_allowed', self.revoke_allowed)
        #else:
            #ret += make_b('revoke_allowed', assign('always', 'no'))
        if self.ai_will_do != '':
            ret += make_b('ai_will_do', self.ai_will_do)
        else:
            ret += make_b('ai_will_do', assign('factor', '0'))
        return ret

    def __str__(self):
        return make_b(self.name, self._blockstr()) + self.other

    def __add__(self, other):
        self.other += str(other)
        return self
