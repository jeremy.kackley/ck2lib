from collections import OrderedDict
from ScriptedBaseType import ScriptedBaseType
from _common import make_b, assign
from prettify import prettify


class ScriptedBaseContainer:

    def __init__(self, items=None, categories=None):
        self._items = items or OrderedDict()
        self._categories = categories or []
        self._cached_sorts = {
            'name': [],
            'category': [],
        }
        self._longest_name=0

    def __len__(self):
        return len(self._items)

    def exists(self, name):
        return self._items.has_key(name)

    def update(self, name, string, category='general', comments=''):
        # while identical right now, the internals of this class might change
        #   leading to a difference between add and update
        return self.add(name, string, category, comments)

    def add_object(self, object):
        self._items[object.name] = object
        if object.category not in self._categories:
            self._categories.append(object.category)
        if len(object.name)>self._longest_name:
            self._longest_name=len(object.name)

    def add(self, name, string, category='general', comments=''):
        self._items[name] = ScriptedBaseType(name, string, category, comments)
        if len(name)>self._longest_name:
            self._longest_name=len(name)
        if category not in self._categories:
            self._categories.append(category)    

    def call(self, name):
        if self._items.has_key(name):
            return assign(name, 'yes')
        self._items[name]

    def __str__(self):
        ret = ''
        cur_cat = None
        mask='#{:'+str(self._longest_name+4)+'}{}\n'
        for thing in self.get_items_by_category():
            if thing.category != cur_cat:
                if cur_cat:
                    ret += '\n'
                cur_cat = thing.category
                ret += '###{}###\n'.format(cur_cat)
                #ret+=mask.format('name','comments')
            if thing.category == cur_cat:
                ret += mask.format(thing.name, thing.comments)
        ret+='\n'
        for thing in self.get_items_by_category():
            #if there is only one category in the file, no reason to
            # denote sections
            if thing.category != cur_cat:
                ret+='\n'
                cur_cat = thing.category
                ret += '###{}###\n'.format(cur_cat)
            if thing.category == cur_cat:
                ret += prettify(str(thing))
        ret+='\n'
        return ret

    def __repr__(self):
        return 'ScriptedBaseContainer({},{})'.format(repr(self._items), repr(self._categories))

    def get_items_by_name(self):
        if len(self._cached_sorts['name']) < len(self._items):
            self._last_sorted_len = len(self._items)
            self._items = OrderedDict(sorted(self._items.items()))
        return self._items.values()

    def get_items_by_category(self):
        if len(self._cached_sorts['category']) < len(self._items):
            ret = []
            for cat in self._categories:
                for thing in self.get_items_by_name():
                    if thing.category == cat:
                        ret.append(thing)
            self._cached_sorts['category'] = ret
        return self._cached_sorts['category']
