
from jinja2 import Environment, FileSystemLoader, Template
from os.path import abspath, dirname, join

from ..on_actions import On_Actions_Events

from prettify import prettify
from base import CharEvent

env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')),
    lstrip_blocks=True,
    trim_blocks=True)
#the concept here is to generate a set of events / flags
#   that will loop a decision on a character, and pass settings
#   down to their heir.  
#   This is a pretty common need in 'auto-do' scripts where
#   an action is periodically automatically taken.


class ControlLoop:
    def __init__(self,namespace,debug=False):
        self.namespace=namespace
        self.init=CharEvent(0,namespace=self.namespace,comment='init')
        self.start=CharEvent(1,namespace=self.namespace,comment='start')
        self.react=CharEvent(2,namespace=self.namespace,comment='react')
        self.on_death=CharEvent(3,namespace=self.namespace,comment='on death')#transfer settings to heir(s) on death
        self.restart=CharEvent(4,namespace=self.namespace,comment='restart')#call init on heir(s)
        self.yearly=CharEvent(5,namespace=self.namespace,comment='yearly pulse')#yearly pulse event
        self.cleanup=CharEvent(6,namespace=self.namespace,comment='cleanup')#cleans up any leftover flags.
        self.events=[self.init,
                    self.start,
                    self.react,
                    self.on_death,
                    self.restart,
                    self.yearly,
                    self.cleanup,
                    ]
        for event in self.events:
            if debug:
                event.log="called on [This.GetBestName]."
            event.hide_window='yes'
            event.is_triggered_only='yes'
        self.created=7#e.g. id 6 and up are available in this namespace.
        #these are reasonable defaults, and can be overriden.
        self.start_flag = '{}_started'.format(self.namespace) 
        self.success_flag = '{}_success'.format(self.namespace)
        self.react_flag = '{}_reacting'.format(self.namespace)
        self.control_flags=[self.start_flag,
                            self.react_flag,
                            self.success_flag]
        self.enabled_flag = '{}_enabled'.format(self.namespace)
        self.settings_flags=[]#e.g. to be copied/reset
        self.other_char_flags=[]#e.g. to be cleaned up on 'stop'
        self.other_prov_flags=[]#e.g. to be cleaned up on 'stop'
        self._generated=False 

    def generate_on_actions(self):        
        od=On_Actions_Events('on_death')
        od.add_event(self.on_death.id)
        oyp=On_Actions_Events('on_yearly_pulse')
        oyp.add_event(self.yearly.id)
        return (od,oyp)

    def generate(self):
        if self._generated:
            return #prevent generate from being called multiple times.
        self.init.immediate=env.get_template('init.j2').render(model=self)
        self.start.immediate=env.get_template('start.j2').render(model=self)
        self.react.immediate=env.get_template('react.j2').render(model=self)
        self.on_death.immediate=env.get_template('on_death.j2').render(model=self)
        self.restart.immediate=env.get_template('restart.j2').render(model=self)
        self.yearly.immediate=env.get_template('yearly.j2').render(model=self)
        self.cleanup.immediate=env.get_template('cleanup.j2').render(model=self)
        #these 'on action' events are only valid for 
        #   on_death and yearly
        self.on_death.has_character_flag = self.enabled_flag
        self.yearly.has_character_flag = self.enabled_flag
        self._generated=True

