#@PydevCodeAnalysisIgnore
# this file facilitates easily 'tweaking' generated commands, without
# any chance of accidentally overwriting manual edits.
from cmd_gen import *  # @UnusedWildImport  # @IgnorePep8


untentable = ['religion_authority',
              'spawn_fleet',
              'spawn_unit',
              'reverse_war',
              ]

from base import trigger, assign, make_b


def province_event(event_id, delay='', tooltip='', random=''):
    return trigger('province_event', event_id, delay, tooltip, random)


def narrative_event(event_id, delay='', tooltip='', random=''):
    return trigger('narrative_event', event_id, delay, tooltip, random)


def letter_event(event_id, delay='', tooltip='', random=''):
    return trigger('letter_event', event_id, delay, tooltip, random)


def character_event(event_id, delay='', tooltip='', random=''):
    return trigger('character_event', event_id, delay, tooltip, random)


def activate_title(title, status):
    return make_b('activate_title',
                  ssign('title', title) +
                  assign('status', status))


def add_character_modifier(name, duration):
    return make_b('add_character_modifier',
                  assign('name', name) +
                  assign('duration', duration))


def add_province_modifier(name, duration):
    # one liner province modifiers are neater
    ret = make_b('add_province_modifier',
                 assign('name', name) +
                 assign('duration', duration))
    return ret.replace('\n', ' ')+'\n'


def change_variable(which, value):
    return make_b('change_variable',
                  assign('which', which) +
                  assign('value', value))


def set_variable(which, value):
    return make_b('set_variable',
                  assign('which', which) +
                  assign('value', value))


def build_holding(title, atype, holder):
    return make_b('build_holding',
                  assign('title', title) +
                  assign('type', atype) +
                  assign('holder', holder))


def create_title(tier, name, holder):
    return make_b('create_title',
                  assign('tier', tier) +
                  assign('name', name) +
                  assign('holder', holder))


def death(death_reason, killer):
    return make_b('death',
                  assign('death_reason', death_reason) +
                  assign('killer', killer))


def opinion(modifier, who, years):
    if '=' in years:
        rest = years
    else:
        rest = assign('years', years)
    return make_b('opinion',
                  assign('who', who) +
                  assign('modifier', modifier)+rest)


def reverse_opinion(modifier, who, years):
    if '=' in years:
        rest = years
    else:
        rest = assign('years', years)
    return make_b('reverse_opinion',
                  assign('who', who) +
                  assign('modifier', modifier)+rest)


def remove_opinion(who, modifier):
    return make_b('remove_opinion',
                  assign('who', who) +
                  assign('modifier', modifier))


def reverse_remove_opinion(modifier, who):
    return make_b('reverse_remove_opinion',
                  assign('who', who) +
                  assign('modifier', modifier))


def set_opinion_levy_raised_days(who, days):
    return make_b('set_opinion_levy_raised_days',
                  assign('who', who) +
                  assign('days', days))


def set_parent_religion(religion, parent):
    return make_b('set_parent_religion',
                  assign('religion', parent) +
                  assign('religion', parent))


def transfer_scaled_wealth(to, value):
    return make_b('transfer_scaled_wealth',
                  assign('to', to) +
                  assign('value', value))


def gain_settlements_under_title(title, enemy):
    return make_b('gain_settlements_under_title',
                  assign('title', title) +
                  assign('enemy', enemy))


def subjugate_or_take_under_title(title, enemy):
    return make_b('subjugate_or_take_under_title',
                  assign('title', title) +
                  assign('enemy', enemy))


def vassalize_or_take_under_title(title, enemy):
    return make_b('vassalize_or_take_under_title',
                  assign('title', title) +
                  assign('enemy', enemy))


def vassalize_or_take_under_title_destroy_duchies(title, enemy):
    return make_b('vassalize_or_take_under_title_destroy_duchies',
                  assign('title', title) +
                  assign('enemy', enemy))
