from _common import make_b, assign, log


class Make_Event:

    def __init__(self, name, aid, desc=None, picture='', namespace=None,comment = ''):
        self.name = name
        if namespace:
            aid = '{}.{}'.format(namespace, aid)
        if not desc:
            desc = '{}.desc'.format(aid)
        self.id = aid
        self.desc = desc
        self.picture = picture
        self._assign_params = ['hide_window',
                               'is_triggered_only',
                               'notification',
                               'major',
                               'is_friendly',
                               'is_hostile',
                               'hide_from',
                               'hide_new',
                               'show_root',
                               'show_from_from',
                               'show_from_from_from',
                               ]
        for thing in self._assign_params:
            setattr(self, thing, None)
        self._pre_triggers = ['only_playable',
                              'is_part_of_plot',
                              'only_rulers',
                              'religion',
                              'religion_group',
                              'min_age',
                              'max_age',
                              'only_independent',
                              'only_men',
                              'only_capable',
                              'capable_only',
                              'lacks_dlc',
                              'has_dlc',
                              'friends',
                              'rivals',
                              'prisoner',
                              'ai',
                              'is_patrician',
                              'is_female',
                              'is_married',
                              'is_sick',
                              'has_character_flag',
                              'has_global_flag',
                              'war',
                              'culture',
                              'culture_group',
                              ]
        for thing in self._pre_triggers:
            setattr(self, thing, None)
        self._block_params = ['weight_multiplier',
                              'mean_time_to_happen',
                              'trigger',
                              'immediate',
                              ]
        # Warning: when firing an event with no delay within immediate block,
        # the new event actually uses the original event's scope (sort of like a sub-routine),
        # rather than creating a copy of these scopes.
        # This can lead to unexpected effects when used with event targets
        for thing in self._block_params:
            setattr(self, thing, [])
        self.immediate = ''
        self.option = []
        self.other = ''
        self.comment = comment
        self.log = ''  # added to immediate block

    def namespace(self):
        return self.id.partition('.')[0]

    def _handle_log(self):
        if self.log:
            if '=' not in self.log:
                self.log = log(self.id, self.log)
            if not self.immediate:
                self.immediate = self.log
            elif self.log not in self.immediate:
                self.immediate = self.log+self.immediate

    def _blockstr(self):
        self._handle_log()
        ret = ''
        ret += assign('id', self.id)
        if self.comment:
            ret = ret.replace('\n', ' ')+'#{}\n'.format(self.comment)
        if self.hide_window != 'yes':
            # hidden events don't need a picture.
            #   don't think it matters, but causes some validation errors.
            ret += assign('desc', self.desc)
            if self.picture:
                ret += assign('picture', self.picture)
            else:
                ret += assign('picture', 'GFX_evt_council')
        for param in self._assign_params:
            if getattr(self, param):
                ret += assign(param, str(getattr(self, param)))
        for param in self._pre_triggers:
            if getattr(self, param):
                ret += assign(param, str(getattr(self, param)))
        for param in self._block_params:
            if getattr(self, param):
                ret += make_b(param, str(getattr(self, param)))
        if not self.option:
            # add a default option
            self.add_option('"ACCEPT"', '')
        for opt in self.option:
            ret += make_b('option', opt)
        ret = make_b(self.name, ret)
        return ret

    def add_option(self, name, option):
        if self.hide_window != 'yes':
            # again, hidden events don't need option names
            # this casues a validation error.
            option = assign('name', name)+option
        self.option.append(option)

    def __str__(self):
        return self._blockstr() + self.other

    def __add__(self, other):
        self.other += str(other)
        return self
    
    def __call__(self,days=None):
        contents=assign('id',self.id)
        if days:
            contents=contents+assign('days',days)
        return make_b(self.name, contents)
