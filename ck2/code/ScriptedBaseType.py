from _common import make_b, assign
from functools import wraps


class ScriptedBaseType:

    def __init__(self, name, string, category='general', comments=''):
        self.name = name
        self.string = string
        self.category = category
        self.comments = comments

    def __repr__(self):
        return 'ScriptedBaseType("{}","{}","{}","{}")'.format(repr(self.name),
                                                              repr(
                                                                  self.string),
                                                              repr(
                                                                  self.category),
                                                              repr(self.comments))

    def __str__(self):
        ret = make_b(self.name, self.string)
        if self.comments:
            ret = '#{}\n'.format(self.comments)+ret
        return ret

    def call(self):
        return assign(self.name, 'yes')


def make_key(name, args, kwargs):
    args_str = [str(x) for x in args]
    kwv_str = [str(x)+'_'+str(kwargs[x]) for x in sorted(kwargs)]
    key = '{}_{}'.format('_'.join(args_str),
                         '_'.join(kwv_str))
    key = key.replace('__', '_')
    if key[-1] == '_':
        key = key[:-1]
    if len(key) > 0:
        key = '_'+key
    return '{}{}'.format(name, key)


def make_scripted_object(name='things', category='general', comments=''):
    """
    This makes it easy to turn global functions that take no, few, or short parameters into 
    scripted effects or scripted triggers.

    While it will function with big parameters, no guarntees that it's going to be pretty.
    """
    def decorating_function(some_function):
        cache = {}

        @wraps(some_function)
        def wrapper(*args, **kwargs):
            key = make_key(name, args, kwargs)
            try:
                cache[key]
            except:
                cache[key] = some_function(*args, **kwargs)
            return assign(key, 'yes')

        def get_result(*args, **kwargs):
            return some_function(*args, **kwargs)

        def get_object(*args, **kwargs):
            try:
                if not cache:
                    args_str = [str(x) for x in args]
                    kwv_str = [str(x)+'_'+str(kwargs[x])
                               for x in sorted(kwargs)]
                    key = make_key(name, args, kwargs)
                    cache[key] = some_function(*args, **kwargs)
            except:
                pass
            ret = []
            for key in cache:
                ret.append(
                    ScriptedBaseType(key, cache[key], category, comments))
            return ret
        wrapper.get_object = get_object
        wrapper.get_result = get_result
        return wrapper
    return decorating_function
