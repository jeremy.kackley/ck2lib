import re
from .. import defines

__RE_PRETTY_SPACES = {}
for spc in '={':
    __RE_PRETTY_SPACES[' ' + spc + ' '] = re.compile('[ ]+' + spc + '[ ]+')
__RE_PRETTY_SPACES['}'] = re.compile('[ ]+}[ ]+')
__RE_BRACKERS = re.compile('([{}])')
__RE_NL = re.compile('(\n)')

def prettify(*strings):
    if defines.PRETTIFY_WITH_SPACES:
        spacer = ' ' * defines.PRETTY_SPACES_TO_USE
    else:
        spacer = '\t'
    lists = []
    for string in strings:
        if isinstance(string, type([])):
            lists = lists + string
        elif isinstance(string, type(())):
            lists = lists + list(string)
        elif isinstance(string, type(set())):
            lists = lists + list(string)
        else:
            lists.append(string)
    ret = ''
    for string in lists:
        if not isinstance(string, type('')):
            string = str(string)  # force it to a string
        # ret += __cfunc__.cpretty(string,spacer)
        # get rid of sketchy tabbing
        # string = string.replace('\r\n', '\n')
         # get rid of double returns
        for spc in __RE_PRETTY_SPACES:
            string = __RE_PRETTY_SPACES[spc].sub(spc, string)
        string = string.replace('\t', '')
        string = string.replace('\r', '')
        string = string.replace('\n\n', '\n')
#     for spc in '={}':
# strip out inconsistent spacing after =,{ and }
#         string = string.replace(spc + ' ', spc)
#         string = string.replace(' ' + spc, spc)
#     for spc in '={':
# add padding spaces to =,{
#         string = string.replace(spc, ' ' + spc + ' ')
        # get rid of double spaces
        string = string.replace('  ', ' ')
        indent = 0
        for block in __RE_BRACKERS.split(string):
            for nl_b in __RE_NL.split(block):
                ret += nl_b
                if '\n' in nl_b:
                    ret += spacer * indent
                    # add a space between each top level block
                    if indent == 0:
                        ret += '\n'
            if '{' in block:
                indent += 1
            if '}' in block:
                indent -= 1
    return ret.replace('\n\n', '\n')