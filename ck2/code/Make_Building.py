from _common import make_b, assign, log


class Make_Building:

    def __init__(self, name, building_desc, building_type):
        self.name = name
        self.type = building_type
        self.desc = building_desc
        self._assign_params = ['add_number_to_name',
                               'replaces',
                               'port',
                               'upgrades_from',
                               'gold_cost',
                               'prestige_cost',
                               'build_time',
                               'ai_creation_factor',
                               'ai_feudal_modifier',
                               'ai_republic_modifier',
                               'extra_tech_building_start',
                               'scouting',
                               'convert_to_city',
                               'convert_to_castle',
                               'convert_to_temple',
                               'convert_to_tribal',
                               'scouting',
                               ]
        for thing in self._assign_params:
            setattr(self, thing, None)
        self._list_params = ['potential',
                             'prerequisites',
                             'not_if_x_exists',
                             'is_active_trigger',
                             'trigger',
                             ]
        for thing in self._list_params:
            setattr(self, thing, [])
        self.modifiers = {}
        self.other = ''

    def _blockstr(self):
        ret = assign('desc', self.desc)
        if not self.gold_cost or self.prestige_cost:
            # because no gold cost is invalid
            self.gold_cost = 1
        if not self.build_time:
            # because no build time is invalid
            self.build_time = 1
        for param in self._list_params:
            if getattr(self, param):
                ret += make_b(param,
                              ' '.join([str(x) for x in getattr(self, param)]))
        for param in self._assign_params:
            if getattr(self, param):
                ret += assign(param, str(getattr(self, param)))
        for mod in self.modifiers:
            ret += assign(mod, self.modifiers[mod])
        return ret

    def add_potential(self, condition):
        '''
        Decides if the building shows up on the buildings list of a holding
            Province scope.
            FROM is the settlement holder (a character),
            FROMFROM is the holding (a barony title).
        '''
        self.potential.append(condition)

    def add_trigger(self, condition):
        '''
        Requirements to be able to build the building.
            Province scope.
        '''
        self.trigger.append(condition)

    def add_is_active_trigger(self, condition):
        '''
        The building will not apply its modifiers to the holding, unless the trigger conditions are satisfied.
            Province scope.
        '''
        self.is_active_trigger.append(condition)

    def add_prerequisites(self, building):
        '''
            Requires the presence of these buildings
        '''
        self.prerequisites.append(building)

    def add_not_if_x_exists(self, building):
        '''
            Cannot build this building if another is (or others are) built.
        '''
        self.not_if_x_exists.append(building)

    def add_modifier(self, modifier_name, modifier_value):
        '''
            Modifiers provided by the building, in case is_active_trigger is valid.
            Note that a negative tax_income might be used to simulate an upkeep for the building.
        '''
        self.modifiers[modifier_name] = modifier_value

    def __str__(self):
        return make_b(self.name, self._blockstr()) + self.other

    def __add__(self, other):
        self.other += str(other)
        return self
