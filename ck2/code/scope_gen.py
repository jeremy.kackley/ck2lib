from base import make_scope


def character_event(a_limit, a_block):
    '''character_event = {limit = {a_limit} a_block}'''
    return make_scope('character_event', a_limit, a_block)


def letter_event(a_limit, a_block):
    '''letter_event = {limit = {a_limit} a_block}'''
    return make_scope('letter_event', a_limit, a_block)


def narrative_event(a_limit, a_block):
    '''narrative_event = {limit = {a_limit} a_block}'''
    return make_scope('narrative_event', a_limit, a_block)


def any_attacker(a_limit, a_block):
    '''any_attacker = {limit = {a_limit} a_block}'''
    return make_scope('any_attacker', a_limit, a_block)


def any_backed_character(a_limit, a_block):
    '''any_backed_character = {limit = {a_limit} a_block}'''
    return make_scope('any_backed_character', a_limit, a_block)


def any_child(a_limit, a_block):
    '''any_child = {limit = {a_limit} a_block}'''
    return make_scope('any_child', a_limit, a_block)


def any_courtier(a_limit, a_block):
    '''any_courtier = {limit = {a_limit} a_block}'''
    return make_scope('any_courtier', a_limit, a_block)


def any_current_enemy(a_limit, a_block):
    '''any_current_enemy = {limit = {a_limit} a_block}'''
    return make_scope('any_current_enemy', a_limit, a_block)


def any_de_jure_vassal(a_limit, a_block):
    '''any_de_jure_vassal = {limit = {a_limit} a_block}'''
    return make_scope('any_de_jure_vassal', a_limit, a_block)


def any_defender(a_limit, a_block):
    '''any_defender = {limit = {a_limit} a_block}'''
    return make_scope('any_defender', a_limit, a_block)


def any_dynasty_member(a_limit, a_block):
    '''any_dynasty_member = {limit = {a_limit} a_block}'''
    return make_scope('any_dynasty_member', a_limit, a_block)


def any_friend(a_limit, a_block):
    '''any_friend = {limit = {a_limit} a_block}'''
    return make_scope('any_friend', a_limit, a_block)


def any_independent_ruler(a_limit, a_block):
    '''any_independent_ruler = {limit = {a_limit} a_block}'''
    return make_scope('any_independent_ruler', a_limit, a_block)


def any_liege(a_limit, a_block):
    '''any_liege = {limit = {a_limit} a_block}'''
    return make_scope('any_liege', a_limit, a_block)


def any_lover(a_limit, a_block):
    '''any_lover = {limit = {a_limit} a_block}'''
    return make_scope('any_lover', a_limit, a_block)


def any_playable_ruler(a_limit, a_block):
    '''any_playable_ruler = {limit = {a_limit} a_block}'''
    return make_scope('any_playable_ruler', a_limit, a_block)


def any_plot_backer(a_limit, a_block):
    '''any_plot_backer = {limit = {a_limit} a_block}'''
    return make_scope('any_plot_backer', a_limit, a_block)


def any_province_character(a_limit, a_block):
    '''any_province_character = {limit = {a_limit} a_block}'''
    return make_scope('any_province_character', a_limit, a_block)


def any_province_lord(a_limit, a_block):
    '''any_province_lord = {limit = {a_limit} a_block}'''
    return make_scope('any_province_lord', a_limit, a_block)


def any_realm_character(a_limit, a_block):
    '''any_realm_character = {limit = {a_limit} a_block}'''
    return make_scope('any_realm_character', a_limit, a_block)


def any_realm_lord(a_limit, a_block):
    '''any_realm_lord = {limit = {a_limit} a_block}'''
    return make_scope('any_realm_lord', a_limit, a_block)


def any_rival(a_limit, a_block):
    '''any_rival = {limit = {a_limit} a_block}'''
    return make_scope('any_rival', a_limit, a_block)


def any_sibling(a_limit, a_block):
    '''any_sibling = {limit = {a_limit} a_block}'''
    return make_scope('any_sibling', a_limit, a_block)


def any_spouse(a_limit, a_block):
    '''any_spouse = {limit = {a_limit} a_block}'''
    return make_scope('any_spouse', a_limit, a_block)


def any_consort(a_limit, a_block):
    '''any_consort = {limit = {a_limit} a_block}'''
    return make_scope('any_consort', a_limit, a_block)


def any_trade_post(a_limit, a_block):
    '''any_trade_post = {limit = {a_limit} a_block}'''
    return make_scope('any_trade_post', a_limit, a_block)


def any_vassal(a_limit, a_block):
    '''any_vassal = {limit = {a_limit} a_block}'''
    return make_scope('any_vassal', a_limit, a_block)


def any_ward(a_limit, a_block):
    '''any_ward = {limit = {a_limit} a_block}'''
    return make_scope('any_ward', a_limit, a_block)


def attacker(a_limit, a_block):
    '''attacker = {limit = {a_limit} a_block}'''
    return make_scope('attacker', a_limit, a_block)


def controller(a_limit, a_block):
    '''controller = {limit = {a_limit} a_block}'''
    return make_scope('controller', a_limit, a_block)


def current_heir(a_limit, a_block):
    '''current_heir = {limit = {a_limit} a_block}'''
    return make_scope('current_heir', a_limit, a_block)


def defender(a_limit, a_block):
    '''defender = {limit = {a_limit} a_block}'''
    return make_scope('defender', a_limit, a_block)


def employer(a_limit, a_block):
    '''employer = {limit = {a_limit} a_block}'''
    return make_scope('employer', a_limit, a_block)


def father(a_limit, a_block):
    '''father = {limit = {a_limit} a_block}'''
    return make_scope('father', a_limit, a_block)


def father_even_if_dead(a_limit, a_block):
    '''father_even_if_dead = {limit = {a_limit} a_block}'''
    return make_scope('father_even_if_dead', a_limit, a_block)


def father_of_unborn(a_limit, a_block):
    '''father_of_unborn = {limit = {a_limit} a_block}'''
    return make_scope('father_of_unborn', a_limit, a_block)


def guardian(a_limit, a_block):
    '''guardian = {limit = {a_limit} a_block}'''
    return make_scope('guardian', a_limit, a_block)


def heir_under_seniority_law(a_limit, a_block):
    '''heir_under_seniority_law = {limit = {a_limit} a_block}'''
    return make_scope('heir_under_seniority_law', a_limit, a_block)


def heir_under_primogeniture_law(a_limit, a_block):
    '''heir_under_primogeniture_law = {limit = {a_limit} a_block}'''
    return make_scope('heir_under_primogeniture_law', a_limit, a_block)


def holder_scope(a_limit, a_block):
    '''holder_scope = {limit = {a_limit} a_block}'''
    return make_scope('holder_scope', a_limit, a_block)


def host(a_limit, a_block):
    '''host = {limit = {a_limit} a_block}'''
    return make_scope('host', a_limit, a_block)


def leader(a_limit, a_block):
    '''leader = {limit = {a_limit} a_block}'''
    return make_scope('leader', a_limit, a_block)


def liege(a_limit, a_block):
    '''liege = {limit = {a_limit} a_block}'''
    return make_scope('liege', a_limit, a_block)


def liege_before_war(a_limit, a_block):
    '''liege_before_war = {limit = {a_limit} a_block}'''
    return make_scope('liege_before_war', a_limit, a_block)


def lover(a_limit, a_block):
    '''lover = {limit = {a_limit} a_block}'''
    return make_scope('lover', a_limit, a_block)


def most_participating_attacker(a_limit, a_block):
    '''most_participating_attacker = {limit = {a_limit} a_block}'''
    return make_scope('most_participating_attacker', a_limit, a_block)


def most_participating_defender(a_limit, a_block):
    '''most_participating_defender = {limit = {a_limit} a_block}'''
    return make_scope('most_participating_defender', a_limit, a_block)


def mother(a_limit, a_block):
    '''mother = {limit = {a_limit} a_block}'''
    return make_scope('mother', a_limit, a_block)


def mother_even_if_dead(a_limit, a_block):
    '''mother_even_if_dead = {limit = {a_limit} a_block}'''
    return make_scope('mother_even_if_dead', a_limit, a_block)


def new_character(a_limit, a_block):
    '''new_character = {limit = {a_limit} a_block}'''
    return make_scope('new_character', a_limit, a_block)


def owner(a_limit, a_block):
    '''owner = {limit = {a_limit} a_block}'''
    return make_scope('owner', a_limit, a_block)


def parent_religion_head(a_limit, a_block):
    '''parent_religion_head = {limit = {a_limit} a_block}'''
    return make_scope('parent_religion_head', a_limit, a_block)


def plot_target_char(a_limit, a_block):
    '''plot_target_char = {limit = {a_limit} a_block}'''
    return make_scope('plot_target_char', a_limit, a_block)


def random_backed_character(a_limit, a_block):
    '''random_backed_character = {limit = {a_limit} a_block}'''
    return make_scope('random_backed_character', a_limit, a_block)


def random_child(a_limit, a_block):
    '''random_child = {limit = {a_limit} a_block}'''
    return make_scope('random_child', a_limit, a_block)


def random_courtier(a_limit, a_block):
    '''random_courtier = {limit = {a_limit} a_block}'''
    return make_scope('random_courtier', a_limit, a_block)


def random_current_enemy(a_limit, a_block):
    '''random_current_enemy = {limit = {a_limit} a_block}'''
    return make_scope('random_current_enemy', a_limit, a_block)


def random_dynasty_member(a_limit, a_block):
    '''random_dynasty_member = {limit = {a_limit} a_block}'''
    return make_scope('random_dynasty_member', a_limit, a_block)


def random_friend(a_limit, a_block):
    '''random_friend = {limit = {a_limit} a_block}'''
    return make_scope('random_friend', a_limit, a_block)


def random_independent_ruler(a_limit, a_block):
    '''random_independent_ruler = {limit = {a_limit} a_block}'''
    return make_scope('random_independent_ruler', a_limit, a_block)


def random_lover(a_limit, a_block):
    '''random_lover = {limit = {a_limit} a_block}'''
    return make_scope('random_lover', a_limit, a_block)


def random_playable_ruler(a_limit, a_block):
    '''random_playable_ruler = {limit = {a_limit} a_block}'''
    return make_scope('random_playable_ruler', a_limit, a_block)


def random_province_character(a_limit, a_block):
    '''random_province_character = {limit = {a_limit} a_block}'''
    return make_scope('random_province_character', a_limit, a_block)


def random_province_lord(a_limit, a_block):
    '''random_province_lord = {limit = {a_limit} a_block}'''
    return make_scope('random_province_lord', a_limit, a_block)


def random_realm_character(a_limit, a_block):
    '''random_realm_character = {limit = {a_limit} a_block}'''
    return make_scope('random_realm_character', a_limit, a_block)


def random_realm_lord(a_limit, a_block):
    '''random_realm_lord = {limit = {a_limit} a_block}'''
    return make_scope('random_realm_lord', a_limit, a_block)


def random_rival(a_limit, a_block):
    '''random_rival = {limit = {a_limit} a_block}'''
    return make_scope('random_rival', a_limit, a_block)


def random_sibling(a_limit, a_block):
    '''random_sibling = {limit = {a_limit} a_block}'''
    return make_scope('random_sibling', a_limit, a_block)


def random_spouse(a_limit, a_block):
    '''random_spouse = {limit = {a_limit} a_block}'''
    return make_scope('random_spouse', a_limit, a_block)


def random_consort(a_limit, a_block):
    '''random_consort = {limit = {a_limit} a_block}'''
    return make_scope('random_consort', a_limit, a_block)


def random_trade_post(a_limit, a_block):
    '''random_trade_post = {limit = {a_limit} a_block}'''
    return make_scope('random_trade_post', a_limit, a_block)


def random_vassal(a_limit, a_block):
    '''random_vassal = {limit = {a_limit} a_block}'''
    return make_scope('random_vassal', a_limit, a_block)


def random_ward(a_limit, a_block):
    '''random_ward = {limit = {a_limit} a_block}'''
    return make_scope('random_ward', a_limit, a_block)


def regent(a_limit, a_block):
    '''regent = {limit = {a_limit} a_block}'''
    return make_scope('regent', a_limit, a_block)


def religion_head(a_limit, a_block):
    '''religion_head = {limit = {a_limit} a_block}'''
    return make_scope('religion_head', a_limit, a_block)


def rightful_religious_head_scope(a_limit, a_block):
    '''rightful_religious_head_scope = {limit = {a_limit} a_block}'''
    return make_scope('rightful_religious_head_scope', a_limit, a_block)


def ruler(a_limit, a_block):
    '''ruler = {limit = {a_limit} a_block}'''
    return make_scope('ruler', a_limit, a_block)


def spouse(a_limit, a_block):
    '''spouse = {limit = {a_limit} a_block}'''
    return make_scope('spouse', a_limit, a_block)


def spouse_even_if_dead(a_limit, a_block):
    '''spouse_even_if_dead = {limit = {a_limit} a_block}'''
    return make_scope('spouse_even_if_dead', a_limit, a_block)


def supported_claimant(a_limit, a_block):
    '''supported_claimant = {limit = {a_limit} a_block}'''
    return make_scope('supported_claimant', a_limit, a_block)


def top_liege(a_limit, a_block):
    '''top_liege = {limit = {a_limit} a_block}'''
    return make_scope('top_liege', a_limit, a_block)


def trade_post_owner(a_limit, a_block):
    '''trade_post_owner = {limit = {a_limit} a_block}'''
    return make_scope('trade_post_owner', a_limit, a_block)


def job_chancellor(a_limit, a_block):
    '''job_chancellor = {limit = {a_limit} a_block}'''
    return make_scope('job_chancellor', a_limit, a_block)


def job_marshal(a_limit, a_block):
    '''job_marshal = {limit = {a_limit} a_block}'''
    return make_scope('job_marshal', a_limit, a_block)


def job_treasurer(a_limit, a_block):
    '''job_treasurer = {limit = {a_limit} a_block}'''
    return make_scope('job_treasurer', a_limit, a_block)


def job_spymaster(a_limit, a_block):
    '''job_spymaster = {limit = {a_limit} a_block}'''
    return make_scope('job_spymaster', a_limit, a_block)


def job_spiritual(a_limit, a_block):
    '''job_spiritual = {limit = {a_limit} a_block}'''
    return make_scope('job_spiritual', a_limit, a_block)


def any_claim(a_limit, a_block):
    '''any_claim = {limit = {a_limit} a_block}'''
    return make_scope('any_claim', a_limit, a_block)


def any_de_jure_vassal_title(a_limit, a_block):
    '''any_de_jure_vassal_title = {limit = {a_limit} a_block}'''
    return make_scope('any_de_jure_vassal_title', a_limit, a_block)


def any_demesne_title(a_limit, a_block):
    '''any_demesne_title = {limit = {a_limit} a_block}'''
    return make_scope('any_demesne_title', a_limit, a_block)


def any_heir_title(a_limit, a_block):
    '''any_heir_title = {limit = {a_limit} a_block}'''
    return make_scope('any_heir_title', a_limit, a_block)


def any_potential_tribal_county(a_limit, a_block):
    '''any_potential_tribal_county = {limit = {a_limit} a_block}'''
    return make_scope('any_potential_tribal_county', a_limit, a_block)


def any_pretender_title(a_limit, a_block):
    '''any_pretender_title = {limit = {a_limit} a_block}'''
    return make_scope('any_pretender_title', a_limit, a_block)


def any_realm_title(a_limit, a_block):
    '''any_realm_title = {limit = {a_limit} a_block}'''
    return make_scope('any_realm_title', a_limit, a_block)


def capital_holding(a_limit, a_block):
    '''capital_holding = {limit = {a_limit} a_block}'''
    return make_scope('capital_holding', a_limit, a_block)


def county(a_limit, a_block):
    '''county = {limit = {a_limit} a_block}'''
    return make_scope('county', a_limit, a_block)


def crownlaw_title(a_limit, a_block):
    '''crownlaw_title = {limit = {a_limit} a_block}'''
    return make_scope('crownlaw_title', a_limit, a_block)


def crusade_target(a_limit, a_block):
    '''crusade_target = {limit = {a_limit} a_block}'''
    return make_scope('crusade_target', a_limit, a_block)


def dejure_liege_title(a_limit, a_block):
    '''dejure_liege_title = {limit = {a_limit} a_block}'''
    return make_scope('dejure_liege_title', a_limit, a_block)


def duchy(a_limit, a_block):
    '''duchy = {limit = {a_limit} a_block}'''
    return make_scope('duchy', a_limit, a_block)


def empire(a_limit, a_block):
    '''empire = {limit = {a_limit} a_block}'''
    return make_scope('empire', a_limit, a_block)


def invasion_target(a_limit, a_block):
    '''invasion_target = {limit = {a_limit} a_block}'''
    return make_scope('invasion_target', a_limit, a_block)


def kingdom(a_limit, a_block):
    '''kingdom = {limit = {a_limit} a_block}'''
    return make_scope('kingdom', a_limit, a_block)


def plot_target_title(a_limit, a_block):
    '''plot_target_title = {limit = {a_limit} a_block}'''
    return make_scope('plot_target_title', a_limit, a_block)


def primary_title(a_limit, a_block):
    '''primary_title = {limit = {a_limit} a_block}'''
    return make_scope('primary_title', a_limit, a_block)


def random_claim(a_limit, a_block):
    '''random_claim = {limit = {a_limit} a_block}'''
    return make_scope('random_claim', a_limit, a_block)


def random_demesne_title(a_limit, a_block):
    '''random_demesne_title = {limit = {a_limit} a_block}'''
    return make_scope('random_demesne_title', a_limit, a_block)


def random_potential_tribal_county(a_limit, a_block):
    '''random_potential_tribal_county = {limit = {a_limit} a_block}'''
    return make_scope('random_potential_tribal_county', a_limit, a_block)


def supported_claimant_title(a_limit, a_block):
    '''supported_claimant_title = {limit = {a_limit} a_block}'''
    return make_scope('supported_claimant_title', a_limit, a_block)


def realm(a_limit, a_block):
    '''realm = {limit = {a_limit} a_block}'''
    return make_scope('realm', a_limit, a_block)


def any_demesne_province(a_limit, a_block):
    '''any_demesne_province = {limit = {a_limit} a_block}'''
    return make_scope('any_demesne_province', a_limit, a_block)


def any_neighbor_province(a_limit, a_block):
    '''any_neighbor_province = {limit = {a_limit} a_block}'''
    return make_scope('any_neighbor_province', a_limit, a_block)


def any_realm_province(a_limit, a_block):
    '''any_realm_province = {limit = {a_limit} a_block}'''
    return make_scope('any_realm_province', a_limit, a_block)


def capital_scope(a_limit, a_block):
    '''capital_scope = {limit = {a_limit} a_block}'''
    return make_scope('capital_scope', a_limit, a_block)


def location(a_limit, a_block):
    '''location = {limit = {a_limit} a_block}'''
    return make_scope('location', a_limit, a_block)


def plot_target_province(a_limit, a_block):
    '''plot_target_province = {limit = {a_limit} a_block}'''
    return make_scope('plot_target_province', a_limit, a_block)


def random_demesne_province(a_limit, a_block):
    '''random_demesne_province = {limit = {a_limit} a_block}'''
    return make_scope('random_demesne_province', a_limit, a_block)


def random_neighbor_province(a_limit, a_block):
    '''random_neighbor_province = {limit = {a_limit} a_block}'''
    return make_scope('random_neighbor_province', a_limit, a_block)


def random_realm_province(a_limit, a_block):
    '''random_realm_province = {limit = {a_limit} a_block}'''
    return make_scope('random_realm_province', a_limit, a_block)


def tooltip(a_limit, a_block):
    '''tooltip = {limit = {a_limit} a_block}'''
    return make_scope('tooltip', a_limit, a_block)


def hidden_tooltip(a_limit, a_block):
    '''hidden_tooltip = {limit = {a_limit} a_block}'''
    return make_scope('hidden_tooltip', a_limit, a_block)


def any_war(a_limit, a_block):
    '''any_war = {limit = {a_limit} a_block}'''
    return make_scope('any_war', a_limit, a_block)


def enemy(a_limit, a_block):
    '''enemy = {limit = {a_limit} a_block}'''
    return make_scope('enemy', a_limit, a_block)


def siege(a_limit, a_block):
    '''siege = {limit = {a_limit} a_block}'''
    return make_scope('siege', a_limit, a_block)


def any_empty_neighbor_province(a_limit, a_block):
    '''any_empty_neighbor_province = {limit = {a_limit} a_block}'''
    return make_scope('any_empty_neighbor_province', a_limit, a_block)


def any_garrisoned_province(a_limit, a_block):
    '''any_garrisoned_province = {limit = {a_limit} a_block}'''
    return make_scope('any_garrisoned_province', a_limit, a_block)


def any_host_courtier(a_limit, a_block):
    '''any_host_courtier = {limit = {a_limit} a_block}'''
    return make_scope('any_host_courtier', a_limit, a_block)


def any_owned(a_limit, a_block):
    '''any_owned = {limit = {a_limit} a_block}'''
    return make_scope('any_owned', a_limit, a_block)


def any_province(a_limit, a_block):
    '''any_province = {limit = {a_limit} a_block}'''
    return make_scope('any_province', a_limit, a_block)


def any_title(a_limit, a_block):
    '''any_title = {limit = {a_limit} a_block}'''
    return make_scope('any_title', a_limit, a_block)


def any_title_under(a_limit, a_block):
    '''any_title_under = {limit = {a_limit} a_block}'''
    return make_scope('any_title_under', a_limit, a_block)


def arranged_marriages(a_limit, a_block):
    '''arranged_marriages = {limit = {a_limit} a_block}'''
    return make_scope('arranged_marriages', a_limit, a_block)


def empty_provinces(a_limit, a_block):
    '''empty_provinces = {limit = {a_limit} a_block}'''
    return make_scope('empty_provinces', a_limit, a_block)


def enemy_provinces(a_limit, a_block):
    '''enemy_provinces = {limit = {a_limit} a_block}'''
    return make_scope('enemy_provinces', a_limit, a_block)


def independent_titles(a_limit, a_block):
    '''independent_titles = {limit = {a_limit} a_block}'''
    return make_scope('independent_titles', a_limit, a_block)


def location_ruler_title(a_limit, a_block):
    '''location_ruler_title = {limit = {a_limit} a_block}'''
    return make_scope('location_ruler_title', a_limit, a_block)


def occupier(a_limit, a_block):
    '''occupier = {limit = {a_limit} a_block}'''
    return make_scope('occupier', a_limit, a_block)


def other_de_jure_claim(a_limit, a_block):
    '''other_de_jure_claim = {limit = {a_limit} a_block}'''
    return make_scope('other_de_jure_claim', a_limit, a_block)


def preferred_heir(a_limit, a_block):
    '''preferred_heir = {limit = {a_limit} a_block}'''
    return make_scope('preferred_heir', a_limit, a_block)


def random_empty_neighbor_province(a_limit, a_block):
    '''random_empty_neighbor_province = {limit = {a_limit} a_block}'''
    return make_scope('random_empty_neighbor_province', a_limit, a_block)


def realm_titles(a_limit, a_block):
    '''realm_titles = {limit = {a_limit} a_block}'''
    return make_scope('realm_titles', a_limit, a_block)


def unborn(a_limit, a_block):
    '''unborn = {limit = {a_limit} a_block}'''
    return make_scope('unborn', a_limit, a_block)


def assimilating_liege(a_limit, a_block):
    '''assimilating_liege = {limit = {a_limit} a_block}'''
    return make_scope('assimilating_liege', a_limit, a_block)


def family(a_limit, a_block):
    '''family = {limit = {a_limit} a_block}'''
    return make_scope('family', a_limit, a_block)


def known_plots(a_limit, a_block):
    '''known_plots = {limit = {a_limit} a_block}'''
    return make_scope('known_plots', a_limit, a_block)


def lasthost(a_limit, a_block):
    '''lasthost = {limit = {a_limit} a_block}'''
    return make_scope('lasthost', a_limit, a_block)


def liege_titles(a_limit, a_block):
    '''liege_titles = {limit = {a_limit} a_block}'''
    return make_scope('liege_titles', a_limit, a_block)


def family_palace(a_limit, a_block):
    '''family_palace = {limit = {a_limit} a_block}'''
    return make_scope('family_palace', a_limit, a_block)
