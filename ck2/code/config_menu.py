'''
Code generators built around generationg configuration
menu's for mods.
'''
from collections import OrderedDict, defaultdict, namedtuple
from os.path import abspath, dirname, join

from jinja2 import Environment, FileSystemLoader, Template

from prettify import prettify

#left here for now more to remind me that I intended to add this.
# class event_menu(menu_base):
#     '''
#     event based version:
#     e.g. auto build version:
#            set_wealth_reserve (50, 500, 1000, more...)
#            set_prestige_reserve (50, 500, 1000, more...)
#            set_targets.
#     '''

#     def __init__(self, name, desc):
#         menu_base.__init__(self, name, desc)
#         self._menu_class = event_menu



env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')),
    lstrip_blocks=True,
    trim_blocks=True)

Toggle = namedtuple(
    'Toggle', ['name','on', 'off', 'flag'],
verbose=False)

class ConfigMenu:
    def __init__(self, name ,on_loca, off_loca, open_desc, close_desc):
        self.name =name #for generating open/close
        self.descriptions = {}
        self.open = 'open_' + name
        self.close = 'close_' + name
        self.descriptions[self.open]=on_loca
        self.descriptions[self.close]=off_loca
        self.descriptions[self.open+'_desc']=open_desc
        self.descriptions[self.close+'_desc']=close_desc
        self.flag = name + '_is_open'  #menu only visible when flag enabled.
        self._toggles = []  #e.g. toggle a flag on or off.
        self._options = []  #visible whenever menu is open.
        #note that defaultdict(lambda: None) returns None on nonexistent keys.
        self._on_actions = defaultdict(lambda: None)  #actions to take when toggled on. 
        self._off_actions = defaultdict(lambda: None)  #actions to take when toggled off.
        self._potentials = defaultdict(lambda: None)  #extra potential options for toggles
        self._sets={}
        self._submenus=OrderedDict()
        self.template = env.get_template('config_menu.j2')
        self._flag_append='_is_enabled'
        self._on_append='_on'
        self._off_append='_off'
        self.default_potential=None #added to every option/toggle created, used by submenu's
        self.is_submenu=False #true if this is a submenu.

    def add_option(self,name,loca,desc):
        self._options.append(name)
        self.descriptions[name]=loca
        self.descriptions[name+'_desc']=desc
        if self.default_potential:
            self.add_potential(name,self.default_potential)

    def add_toggle(self,name,on_loca,off_loca,on_desc,off_desc):
        self._toggles.append(name)
        self.descriptions[self.get_on_name(name)]=on_loca
        self.descriptions[self.get_off_name(name)]=off_loca
        self.descriptions[self.get_on_desc(name)]=on_desc
        self.descriptions[self.get_off_desc(name)]=off_desc
        if self.default_potential:
            self.add_potential(name,self.default_potential)
    
    def add_potential(self,name,potential):
        if self._potentials.has_key(name):
            potential=potential+self._potentials[name]
        self._potentials[name]=potential
    
    def add_to_exclusion_set(self,name,set_name):
        if not self._sets.has_key(set_name):
            self._sets[set_name]=set()
        self._sets[set_name].add(name)
    
    def add_on_action(self,option,action):
        self._on_actions[option]=action
    
    def add_off_action(self,option,action):
        self._off_actions[option]=action

    def add_action(self,option,action):
        self.add_on_action(option,action)
        if option in self._toggles:
            self.add_off_action(option,action)
    
    def add_submenu(self,submenu_name,on_loca, off_loca, open_desc, close_desc):
        self._submenus[submenu_name]=ConfigMenu(submenu_name,on_loca, off_loca, open_desc, close_desc)  
        self._submenus[submenu_name].default_potential='has_character_flag = {}\n'.format(self.flag)  
        self._submenus[submenu_name].is_submenu=True
        return self._submenus[submenu_name]
    
    def get_exclusive_flags(self,name):
        for set_name in self._sets:
            if name in self._sets[set_name]:
                return [self.get_flag_name(x) for x in self._sets[set_name]]
        return []

    def get_toggles(self):
        ret = []
        for thing in self._toggles:
            ret.append(
                Toggle(thing, 
                    self.get_on_name(thing),
                    self.get_off_name(thing), 
                    self.get_flag_name(thing),
                    ))
        return ret

    def get_on_name(self,name):
        return name+self._on_append

    def get_on_desc(self,name):
        return self.get_on_name(name)+'_desc'

    def get_off_name(self,name):
        return name+self._off_append
    
    def get_off_desc(self,name):
        return self.get_off_name(name)+'_desc'
    
    def get_flag_name(self,name):
        return name + self._flag_append

    def get_all_submenus(self):
        ret=[]
        for thing in self._submenus.values():
            ret.append(thing)
        for thing in self._submenus.values():
            ret=ret+thing.get_all_submenus()
        return ret

    def render(self):
        return self.template.render(menu=self)

    def __str__(self):
        ret=prettify(self.render())
        return ret
