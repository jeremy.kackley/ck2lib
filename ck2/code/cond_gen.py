# -*- coding: utf-8 -*-
from base import assign, make_b


def age(value):
    '''
        age = value
        type: int
from: character
The age of a character. Can be any whole number
    '''
    return assign('age', value)


def ai(value):
    '''
        ai = value
        type: bool
from: character
If yes, will only fire for ai characters
    '''
    return assign('ai', value)


def always(value):
    '''
        always = value

    '''
    return assign('always', value)


def at_location(value):
    '''
        at_location = value

    '''
    return assign('at_location', value)


def at_sea(value):
    '''
        at_sea = value

    '''
    return assign('at_sea', value)


def attribute_diff(value):
    '''
        attribute_diff = value

    '''
    return assign('attribute_diff', value)


def can_be_given_away(value):
    '''
        can_be_given_away = value

    '''
    return assign('can_be_given_away', value)


def can_change_religion(value):
    '''
        can_change_religion = value

    '''
    return assign('can_change_religion', value)


def can_have_more_leadership_traits(value):
    '''
        can_have_more_leadership_traits = value

    '''
    return assign('can_have_more_leadership_traits', value)


def can_marry(value):
    '''
        can_marry = value

    '''
    return assign('can_marry', value)


def capable_only(value):
    '''
        capable_only = value
        type: bool
If yes, only picks characters without the Incapable trait
    '''
    return assign('capable_only', value)


def capital(value):
    '''
        capital = value

    '''
    return assign('capital', value)


def character(value):
    '''
        character = value

    '''
    return assign('character', value)


def check_variable(value):
    '''
        check_variable = value

    '''
    return assign('check_variable', value)


def claimed_by(value):
    '''
        claimed_by = value

    '''
    return assign('claimed_by', value)


def completely_controls(value):
    '''
        completely_controls = value

    '''
    return assign('completely_controls', value)


def conquest_culture(value):
    '''
        conquest_culture = value

    '''
    return assign('conquest_culture', value)


def continent(value):
    '''
        continent = value

    '''
    return assign('continent', value)


def controlled_by(value):
    '''
        controlled_by = value

    '''
    return assign('controlled_by', value)


def controls_religion(value):
    '''
        controls_religion = value
        type: bool
from: character
If yes, only picks characters who are the head of their religion
    '''
    return assign('controls_religion', value)


def count(value):
    '''
        count = value

    '''
    return assign('count', value)


def culture(value):
    '''
        culture = value
        type: string
from: character
Checks if the character has this specific culture
    '''
    return assign('culture', value)


def culture_group(value):
    '''
        culture_group = value
        type: string
from: character
Checks if the character is part of this culturegroup
    '''
    return assign('culture_group', value)


def de_facto_liege(value):
    '''
        de_facto_liege = value

    '''
    return assign('de_facto_liege', value)


def de_jure_liege(value):
    '''
        de_jure_liege = value

    '''
    return assign('de_jure_liege', value)


def de_jure_liege_or_above(value):
    '''
        de_jure_liege_or_above = value

    '''
    return assign('de_jure_liege_or_above', value)


def de_jure_vassal_or_below(value):
    '''
        de_jure_vassal_or_below = value

    '''
    return assign('de_jure_vassal_or_below', value)


def decadence(value):
    '''
        decadence = value

    '''
    return assign('decadence', value)


def defending_against_claimant(value):
    '''
        defending_against_claimant = value

    '''
    return assign('defending_against_claimant', value)


def demesne_efficiency(value):
    '''
        demesne_efficiency = value

    '''
    return assign('demesne_efficiency', value)


def demesne_size(value):
    '''
        demesne_size = value
        type: int
from: character
Checks if the character has a minimum of this demesne size
    '''
    return assign('demesne_size', value)


def diplomacy(value):
    '''
        diplomacy = value
        type: int
from: character
The character's Diplomacy stat.
    '''
    return assign('diplomacy', value)


def dismissal_trigger(value):
    '''
        dismissal_trigger = value

    '''
    return assign('dismissal_trigger', value)


def distance(value):
    '''
        distance = value
        type: int
from: character
Determines the minimum distance from the scope (Example : distance = { who = ROOT distance = 100 } } (if you use NOT, it becomes maximum distance )
    '''
    return assign('distance', value)


def dynasty(value):
    '''
        dynasty = value
        type: int
from: character
The character's dynasty, indexed by an integer value.
    '''
    return assign('dynasty', value)


def dynasty_realm_power(value):
    '''
        dynasty_realm_power = value

    '''
    return assign('dynasty_realm_power', value)


def excommunicated_for(value):
    '''
        excommunicated_for = value

    '''
    return assign('excommunicated_for', value)


def faction_power(value):
    '''
        faction_power = value

    '''
    return assign('faction_power', value)


def family(value):
    '''
        family = value

    '''
    return assign('family', value)


def father_of_unborn_known(value):
    '''
        father_of_unborn_known = value
        type: bool
from: character
Checks if a pregnant woman's unborn child has a known father.
    '''
    return assign('father_of_unborn_known', value)


def fertility(value):
    '''
        fertility = value
        type: int
from: character
The character's fertility value. In-game, can only be seen through the "charinfo" console command.
    '''
    return assign('fertility', value)


def flank_has_leader(value):
    '''
        flank_has_leader = value

    '''
    return assign('flank_has_leader', value)


def gold(value):
    '''
        gold = value
        type: int
from: character
How much money a character has.
    '''
    return assign('gold', value)


def graphical_culture(value):
    '''
        graphical_culture = value
        from: character
A character's graphical culture. In-game, can only be seen through the "charinfo" console command.
    '''
    return assign('graphical_culture', value)


def had_character_flag(flag, period):
    '''
        had_character_flag = value

    '''
    if '=' not in str(period):
        period = assign('days', period)
    flag = assign('flag', flag)
    return make_b('had_character_flag', flag + period)


def had_dynasty_flag(flag, period):
    '''
        had_dynasty_flag = value

    '''
    if '=' not in str(period):
        period = assign('days', period)
    flag = assign('flag', flag)
    return make_b('had_dynasty_flag', flag + period)


def had_global_flag(flag, period):
    '''
        had_global_flag = value

    '''
    if '=' not in str(period):
        period = assign('days', period)
    flag = assign('flag', flag)
    return make_b('had_global_flag', flag + period)


def had_province_flag(flag, period):
    '''
        had_province_flag = value

    '''
    if '=' not in str(period):
        period = assign('days', period)
    flag = assign('flag', flag)
    return make_b('had_province_flag', flag + period)


def has_ambition(value):
    '''
        has_ambition = value

    '''
    return assign('has_ambition', value)


def has_building(value):
    '''
        has_building = value

    '''
    return assign('has_building', value)


def has_called_crusade(value):
    '''
        has_called_crusade = value

    '''
    return assign('has_called_crusade', value)


def has_character_flag(value):
    '''
        has_character_flag = value
        type: string
from: character
Checks if a character has a specified flag. NOTE: "flags" in CKII scripting are not pre-determined or stored anywhere, they are dynamically created in the script. A mistyped flag will not be caught by the Validator, as there are no "invalid" flags.
    '''
    return assign('has_character_flag', value)


def has_character_modifier(value):
    '''
        has_character_modifier = value
        type: string
from: character
Checks if the character has this modifier
    '''
    return assign('has_character_modifier', value)


def has_claim(value):
    '''
        has_claim = value

    '''
    return assign('has_claim', value)


def has_crown_law_title(value):
    '''
        has_crown_law_title = value
        type: bool
from: title
Used to check crown laws for a specified title. Is not a condition itself though, it must be used as a scope to check crown laws by category (succession, for instance).
    '''
    return assign('has_crown_law_title', value)


def has_de_jure_pretension(value):
    '''
        has_de_jure_pretension = value

    '''
    return assign('has_de_jure_pretension', value)


def has_disease(value):
    '''
        has_disease = value

    '''
    return assign('has_disease', value)


def has_dlc(value):
    '''
        has_dlc = value
        type: string/bool
checks if the user has a specified DLC. While it returns a bool, it must be checked against a string (example code: has_dlc = "The Old Gods").
    '''
    return assign('has_dlc', value)


def has_dynasty_flag(value):
    '''
        has_dynasty_flag = value

    '''
    return assign('has_dynasty_flag', value)


def has_earmarked_regiments(value):
    '''
        has_earmarked_regiments = value

    '''
    return assign('has_earmarked_regiments', value)


def has_empty_holding(value):
    '''
        has_empty_holding = value

    '''
    return assign('has_empty_holding', value)


def has_epidemic(value):
    '''
        has_epidemic = value

    '''
    return assign('has_epidemic', value)


def has_global_flag(value):
    '''
        has_global_flag = value

    '''
    return assign('has_global_flag', value)


def has_guardian(value):
    '''
        has_guardian = value

    '''
    return assign('has_guardian', value)


def has_heresies(value):
    '''
        has_heresies = value

    '''
    return assign('has_heresies', value)


def has_higher_tech_than(value):
    '''
        has_higher_tech_than = value

    '''
    return assign('has_higher_tech_than', value)


def has_holder(value):
    '''
        has_holder = value

    '''
    return assign('has_holder', value)


def has_horde_culture(value):
    '''
        has_horde_culture = value

    '''
    return assign('has_horde_culture', value)


def has_job_action(value):
    '''
        has_job_action = value

    '''
    return assign('has_job_action', value)


def has_job_title(value):
    '''
        has_job_title = value

    '''
    return assign('has_job_title', value)


def has_landed_title(value):
    '''
        has_landed_title = value

    '''
    return assign('has_landed_title', value)


def has_law(value):
    '''
        has_law = value

    '''
    return assign('has_law', value)


def has_lover(value):
    '''
        has_lover = value
        type: bool
from: character
Yes if the character has a lover
    '''
    return assign('has_lover', value)


def has_minor_title(value):
    '''
        has_minor_title = value
        type: bool
from: character
Yes if the character has a minor title
    '''
    return assign('has_minor_title', value)


def has_nickname(value):
    '''
        has_nickname = value

    '''
    return assign('has_nickname', value)


def has_objective(value):
    '''
        has_objective = value

    '''
    return assign('has_objective', value)


def has_opinion_modifier(value):
    '''
        has_opinion_modifier = value

    '''
    return assign('has_opinion_modifier', value)


def has_owner(value):
    '''
        has_owner = value

    '''
    return assign('has_owner', value)


def has_plot(value):
    '''
        has_plot = value

    '''
    return assign('has_plot', value)


def has_province_flag(value):
    '''
        has_province_flag = value

    '''
    return assign('has_province_flag', value)


def has_province_modifier(value):
    '''
        has_province_modifier = value

    '''
    return assign('has_province_modifier', value)


def has_raised_levies(value):
    '''
        has_raised_levies = value

    '''
    return assign('has_raised_levies', value)


def has_regent(value):
    '''
        has_regent = value

    '''
    return assign('has_regent', value)


def has_siege(value):
    '''
        has_siege = value

    '''
    return assign('has_siege', value)


def has_strong_claim(value):
    '''
        has_strong_claim = value

    '''
    return assign('has_strong_claim', value)


def has_trade_post(value):
    '''
        has_trade_post = value

    '''
    return assign('has_trade_post', value)


def has_truce(value):
    '''
        has_truce = value

    '''
    return assign('has_truce', value)


def has_weak_claim(value):
    '''
        has_weak_claim = value

    '''
    return assign('has_weak_claim', value)


def health(value):
    '''
        health = value
        from: character
A character's health value. In-game, can only be seen through the "charinfo" console command.
    '''
    return assign('health', value)


def health_traits(value):
    '''
        health_traits = value

    '''
    return assign('health_traits', value)


def higher_tier_than(value):
    '''
        higher_tier_than = value

    '''
    return assign('higher_tier_than', value)


def holy_order(value):
    '''
        holy_order = value

    '''
    return assign('holy_order', value)


def imprisoned_days(value):
    '''
        imprisoned_days = value

    '''
    return assign('imprisoned_days', value)


def in_battle(value):
    '''
        in_battle = value

    '''
    return assign('in_battle', value)


def in_command(value):
    '''
        in_command = value

    '''
    return assign('in_command', value)


def in_faction(value):
    '''
        in_faction = value

    '''
    return assign('in_faction', value)


def in_revolt(value):
    '''
        in_revolt = value

    '''
    return assign('in_revolt', value)


def in_siege(value):
    '''
        in_siege = value

    '''
    return assign('in_siege', value)


def independent(value):
    '''
        independent = value

    '''
    return assign('independent', value)


def intrigue(value):
    '''
        intrigue = value

    '''
    return assign('intrigue', value)


def is_abroad(value):
    '''
        is_abroad = value
        type: bool
Valid if character is 'abroad' (not at liege's court)
    '''
    return assign('is_abroad', value)


def is_adult(value):
    '''
        is_adult = value
        type: bool
from: character
Checks if the character is an adult.
    '''
    return assign('is_adult', value)


def is_alive(value):
    '''
        is_alive = value
        type: bool
If yes, will only affect alive characters
    '''
    return assign('is_alive', value)


def is_at_sea(value):
    '''
        is_at_sea = value

    '''
    return assign('is_at_sea', value)


def is_attacker(value):
    '''
        is_attacker = value

    '''
    return assign('is_attacker', value)


def is_betrothed(value):
    '''
        is_betrothed = value
        type: bool
from: character
If yes, will only affect bethrothed characters
    '''
    return assign('is_betrothed', value)


def is_capital(value):
    '''
        is_capital = value
        type: bool
If yes, will only affect the capital HOLDING
    '''
    return assign('is_capital', value)


def is_chancellor(value):
    '''
        is_chancellor = value
        type: bool
If yes, will only trigger if the character is a chancellor
    '''
    return assign('is_chancellor', value)


def is_child_of(value):
    '''
        is_child_of = value
        type: string
from: character
Checks if the scope is a child of ...
    '''
    return assign('is_child_of', value)


def is_close_relative(value):
    '''
        is_close_relative = value
        type: string
from: character
checks if the scope is a close relative of ...
    '''
    return assign('is_close_relative', value)


def is_conquered(value):
    '''
        is_conquered = value

    '''
    return assign('is_conquered', value)


def is_consort(value):
    '''
        is_consort = value
        type: bool
from: character

    '''
    return assign('is_consort', value)


def is_contested(value):
    '''
        is_contested = value

    '''
    return assign('is_contested', value)


def is_councillor(value):
    '''
        is_councillor = value

    '''
    return assign('is_councillor', value)


def is_father(value):
    '''
        is_father = value

    '''
    return assign('is_father', value)


def is_father_real_father(value):
    '''
        is_father_real_father = value

    '''
    return assign('is_father_real_father', value)


def is_female(value):
    '''
        is_female = value
        type: bool
from: character
If yes, will only affect female characters
    '''
    return assign('is_female', value)


def is_feudal(value):
    '''
        is_feudal = value
        type: bool
from: character/title
Checks if a character's primary title (or if a title itself) is a feudal one.
    '''
    return assign('is_feudal', value)


def is_friend(value):
    '''
        is_friend = value
        type: bool
from: character
Checks if this character is a friend of the given character (e.g. is_friend = ROOT)
    '''
    return assign('is_friend', value)


def is_guardian(value):
    '''
        is_guardian = value

    '''
    return assign('is_guardian', value)


def is_heresy_of(value):
    '''
        is_heresy_of = value

    '''
    return assign('is_heresy_of', value)


def is_heretic(value):
    '''
        is_heretic = value

    '''
    return assign('is_heretic', value)


def is_holy_war(value):
    '''
        is_holy_war = value

    '''
    return assign('is_holy_war', value)


def is_hostile(value):
    '''
        is_hostile = value

    '''
    return assign('is_hostile', value)


def is_ill(value):
    '''
        is_ill = value

    '''
    return assign('is_ill', value)


def is_independence(value):
    '''
        is_independence = value

    '''
    return assign('is_independence', value)


def is_ironman(value):
    '''
        is_ironman = value

    '''
    return assign('is_ironman', value)


def is_land(value):
    '''
        is_land = value

    '''
    return assign('is_land', value)


def is_landed(value):
    '''
        is_landed = value

    '''
    return assign('is_landed', value)


def is_landless_type_title(value):
    '''
        is_landless_type_title = value

    '''
    return assign('is_landless_type_title', value)


def is_liege_of(value):
    '''
        is_liege_of = value

    '''
    return assign('is_liege_of', value)


def is_liege_or_above(value):
    '''
        is_liege_or_above = value

    '''
    return assign('is_liege_or_above', value)


def is_located_in(value):
    '''
        is_located_in = value

    '''
    return assign('is_located_in', value)


def is_lover(value):
    '''
        is_lover = value
        type: bool
from: character
Checks if this character is a lover of the given character (Example : is_lover = ROOT)
    '''
    return assign('is_lover', value)


def is_main_spouse(value):
    '''
        is_main_spouse = value

    '''
    return assign('is_main_spouse', value)


def is_marriage_adult(value):
    '''
        is_marriage_adult = value

    '''
    return assign('is_marriage_adult', value)


def is_married(value):
    '''
        is_married = value
        type: bool
from: character
Yes, will only affect married charactes
    '''
    return assign('is_married', value)


def is_marshal(value):
    '''
        is_marshal = value

    '''
    return assign('is_marshal', value)


def is_merchant_republic(value):
    '''
        is_merchant_republic = value

    '''
    return assign('is_merchant_republic', value)


def is_mother(value):
    '''
        is_mother = value

    '''
    return assign('is_mother', value)


def is_occupied(value):
    '''
        is_occupied = value

    '''
    return assign('is_occupied', value)


def is_older_than(value):
    '''
        is_older_than = value

    '''
    return assign('is_older_than', value)


def is_parent_religion(value):
    '''
        is_parent_religion = value

    '''
    return assign('is_parent_religion', value)


def is_patrician(value):
    '''
        is_patrician = value
        type: bool
from: character
Checks if a character is the head of a Merchant Republic patrician family.
    '''
    return assign('is_patrician', value)


def is_playable(value):
    '''
        is_playable = value
        type: bool
from: character
Only affects playable characters
    '''
    return assign('is_playable', value)


def is_plot_active(value):
    '''
        is_plot_active = value

    '''
    return assign('is_plot_active', value)


def is_plot_target_of(value):
    '''
        is_plot_target_of = value

    '''
    return assign('is_plot_target_of', value)


def is_pregnant(value):
    '''
        is_pregnant = value

    '''
    return assign('is_pregnant', value)


def is_pretender(value):
    '''
        is_pretender = value

    '''
    return assign('is_pretender', value)


def is_priest(value):
    '''
        is_priest = value

    '''
    return assign('is_priest', value)


def is_primary_heir(value):
    '''
        is_primary_heir = value

    '''
    return assign('is_primary_heir', value)


def is_primary_holder_title(value):
    '''
        is_primary_holder_title = value

    '''
    return assign('is_primary_holder_title', value)


def is_primary_holder_title_tier(value):
    '''
        is_primary_holder_title_tier = value

    '''
    return assign('is_primary_holder_title_tier', value)


def is_primary_type_title(value):
    '''
        is_primary_type_title = value

    '''
    return assign('is_primary_type_title', value)


def is_primary_war_attacker(value):
    '''
        is_primary_war_attacker = value

    '''
    return assign('is_primary_war_attacker', value)


def is_primary_war_defender(value):
    '''
        is_primary_war_defender = value

    '''
    return assign('is_primary_war_defender', value)


def is_recent_grant(value):
    '''
        is_recent_grant = value

    '''
    return assign('is_recent_grant', value)


def is_reformed_religion(value):
    '''
        is_reformed_religion = value

    '''
    return assign('is_reformed_religion', value)


def is_republic(value):
    '''
        is_republic = value

    '''
    return assign('is_republic', value)


def is_ruler(value):
    '''
        is_ruler = value
        type: bool
from: character
Yes, will only affect characters who have a title (who are a ruler)
    '''
    return assign('is_ruler', value)


def is_spiritual(value):
    '''
        is_spiritual = value

    '''
    return assign('is_spiritual', value)


def is_spymaster(value):
    '''
        is_spymaster = value
        type: bool
from: character
Yes, will only affect characters who are a spymaster
    '''
    return assign('is_spymaster', value)


def is_theocracy(value):
    '''
        is_theocracy = value

    '''
    return assign('is_theocracy', value)


def is_title_active(value):
    '''
        is_title_active = value

    '''
    return assign('is_title_active', value)


def is_titular(value):
    '''
        is_titular = value

    '''
    return assign('is_titular', value)


def is_treasurer(value):
    '''
        is_treasurer = value

    '''
    return assign('is_treasurer', value)


def is_tribal_type_title(value):
    '''
        is_tribal_type_title = value

    '''
    return assign('is_tribal_type_title', value)


def is_valid(value):
    '''
        is_valid = value

    '''
    return assign('is_valid', value)


def is_valid_attraction(value):
    '''
        is_valid_attraction = value

    '''
    return assign('is_valid_attraction', value)


def is_vassal_or_below(value):
    '''
        is_vassal_or_below = value

    '''
    return assign('is_vassal_or_below', value)


def leads_faction(value):
    '''
        leads_faction = value

    '''
    return assign('leads_faction', value)


def learning(value):
    '''
        learning = value
        type: int
from: character
A character's Learning stat.
    '''
    return assign('learning', value)


def lifestyle_traits(value):
    '''
        lifestyle_traits = value

    '''
    return assign('lifestyle_traits', value)


def likes_better_than(value):
    '''
        likes_better_than = value

    '''
    return assign('likes_better_than', value)


def loot(value):
    '''
        loot = value

    '''
    return assign('loot', value)


def lower_tier_than(value):
    '''
        lower_tier_than = value
        type: string
from: character
Yes, will only affect characters who are of a lower tier
    '''
    return assign('lower_tier_than', value)


def martial(value):
    '''
        martial = value
        type: int
from: character
A character's Martial stat.
    '''
    return assign('martial', value)


def max_age(value):
    '''
        max_age = value
        type: string
from: character
Maximum age of the character (for this event)
    '''
    return assign('max_age', value)


def mercenary(value):
    '''
        mercenary = value

    '''
    return assign('mercenary', value)


def min_age(value):
    '''
        min_age = value
        type: string
from: character
minimum age of the character (for this event)
    '''
    return assign('min_age', value)


def month(value):
    '''
        month = value

    '''
    return assign('month', value)


def not_if_x_exists(value):
    '''
        not_if_x_exists = value

    '''
    return assign('not_if_x_exists', value)


def num_culture_realm_provs(value):
    '''
        num_culture_realm_provs = value

    '''
    return assign('num_culture_realm_provs', value)


def num_of_baron_titles(value):
    '''
        num_of_baron_titles = value

    '''
    return assign('num_of_baron_titles', value)


def num_of_buildings(value):
    '''
        num_of_buildings = value

    '''
    return assign('num_of_buildings', value)


def num_of_children(value):
    '''
        num_of_children = value

    '''
    return assign('num_of_children', value)


def num_of_claims(value):
    '''
        num_of_claims = value

    '''
    return assign('num_of_claims', value)


def num_of_count_titles(value):
    '''
        num_of_count_titles = value

    '''
    return assign('num_of_count_titles', value)


def num_of_duke_titles(value):
    '''
        num_of_duke_titles = value

    '''
    return assign('num_of_duke_titles', value)


def num_of_emperor_titles(value):
    '''
        num_of_emperor_titles = value

    '''
    return assign('num_of_emperor_titles', value)


def num_of_extra_landed_titles(value):
    '''
        num_of_extra_landed_titles = value

    '''
    return assign('num_of_extra_landed_titles', value)


def num_of_faction_backers(value):
    '''
        num_of_faction_backers = value

    '''
    return assign('num_of_faction_backers', value)


def num_of_friends(value):
    '''
        num_of_friends = value

    '''
    return assign('num_of_friends', value)


def num_of_holy_sites(value):
    '''
        num_of_holy_sites = value

    '''
    return assign('num_of_holy_sites', value)


def num_of_king_titles(value):
    '''
        num_of_king_titles = value

    '''
    return assign('num_of_king_titles', value)


def num_of_plot_backers(value):
    '''
        num_of_plot_backers = value

    '''
    return assign('num_of_plot_backers', value)


def num_of_prisoners(value):
    '''
        num_of_prisoners = value

    '''
    return assign('num_of_prisoners', value)


def num_of_realm_counties(value):
    '''
        num_of_realm_counties = value

    '''
    return assign('num_of_realm_counties', value)


def num_of_rivals(value):
    '''
        num_of_rivals = value

    '''
    return assign('num_of_rivals', value)


def num_of_settlements(value):
    '''
        num_of_settlements = value

    '''
    return assign('num_of_settlements', value)


def num_of_titles(value):
    '''
        num_of_titles = value

    '''
    return assign('num_of_titles', value)


def num_of_trade_post_diff(value):
    '''
        num_of_trade_post_diff = value

    '''
    return assign('num_of_trade_post_diff', value)


def num_of_trade_posts(value):
    '''
        num_of_trade_posts = value

    '''
    return assign('num_of_trade_posts', value)


def num_of_traits(value):
    '''
        num_of_traits = value

    '''
    return assign('num_of_traits', value)


def num_of_vassals(value):
    '''
        num_of_vassals = value

    '''
    return assign('num_of_vassals', value)


def num_title_realm_provs(value):
    '''
        num_title_realm_provs = value

    '''
    return assign('num_title_realm_provs', value)


def num_traits(value):
    '''
        num_traits = value

    '''
    return assign('num_traits', value)


def only_capable(value):
    '''
        only_capable = value

    '''
    return assign('only_capable', value)


def only_independent(value):
    '''
        only_independent = value

    '''
    return assign('only_independent', value)


def only_men(value):
    '''
        only_men = value

    '''
    return assign('only_men', value)


def only_playable(value):
    '''
        only_playable = value

    '''
    return assign('only_playable', value)


def only_rulers(value):
    '''
        only_rulers = value

    '''
    return assign('only_rulers', value)


def only_women(value):
    '''
        only_women = value

    '''
    return assign('only_women', value)


def opinion(value):
    '''
        opinion = value
        type: string
from: character
Checks if the opinion is at least this value (Example opinion = { who = ROOT value = 25 } )
    '''
    return assign('opinion', value)


def opinion_diff(value):
    '''
        opinion_diff = value

    '''
    return assign('opinion_diff', value)


def opinion_levy_raised_days(value):
    '''
        opinion_levy_raised_days = value

    '''
    return assign('opinion_levy_raised_days', value)


def overlord_of(value):
    '''
        overlord_of = value

    '''
    return assign('overlord_of', value)


def owns(value):
    '''
        owns = value

    '''
    return assign('owns', value)


def personality_traits(value):
    '''
        personality_traits = value

    '''
    return assign('personality_traits', value)


def piety(value):
    '''
        piety = value
        type: int
from: character
The amount of piety a character has.
    '''
    return assign('piety', value)


def plot_power(value):
    '''
        plot_power = value

    '''
    return assign('plot_power', value)


def port(value):
    '''
        port = value

    '''
    return assign('port', value)


def preparing_invasion(value):
    '''
        preparing_invasion = value

    '''
    return assign('preparing_invasion', value)


def prestige(value):
    '''
        prestige = value
        type: int
from: character
The amount of prestige a character has.
    '''
    return assign('prestige', value)


def prisoner(value):
    '''
        prisoner = value
        type: bool
from: character
If No, can't be applied to imprisoned characters
    '''
    return assign('prisoner', value)


def province(value):
    '''
        province = value

    '''
    return assign('province', value)


def province_capital(value):
    '''
        province_capital = value

    '''
    return assign('province_capital', value)


def province_id(value):
    '''
        province_id = value

    '''
    return assign('province_id', value)


def realm_diplomacy(value):
    '''
        realm_diplomacy = value

    '''
    return assign('realm_diplomacy', value)


def realm_intrigue(value):
    '''
        realm_intrigue = value

    '''
    return assign('realm_intrigue', value)


def realm_learning(value):
    '''
        realm_learning = value

    '''
    return assign('realm_learning', value)


def realm_martial(value):
    '''
        realm_martial = value

    '''
    return assign('realm_martial', value)


def realm_size(value):
    '''
        realm_size = value

    '''
    return assign('realm_size', value)


def realm_stewardship(value):
    '''
        realm_stewardship = value

    '''
    return assign('realm_stewardship', value)


def rebel(value):
    '''
        rebel = value

    '''
    return assign('rebel', value)


def relative_power(value):
    '''
        relative_power = value

    '''
    return assign('relative_power', value)


def relative_power_to_liege(value):
    '''
        relative_power_to_liege = value

    '''
    return assign('relative_power_to_liege', value)


def religion(value):
    '''
        religion = value

    '''
    return assign('religion', value)


def religion_authority(value):
    '''
        religion_authority = value

    '''
    return assign('religion_authority', value)


def religion_group(value):
    '''
        religion_group = value

    '''
    return assign('religion_group', value)


def reverse_has_opinion_modifier(value):
    '''
        reverse_has_opinion_modifier = value

    '''
    return assign('reverse_has_opinion_modifier', value)


def reverse_has_truce(value):
    '''
        reverse_has_truce = value

    '''
    return assign('reverse_has_truce', value)


def reverse_opinion(value):
    '''
        reverse_opinion = value

    '''
    return assign('reverse_opinion', value)


def revolt_risk(value):
    '''
        revolt_risk = value

    '''
    return assign('revolt_risk', value)


def rightful_religious_head(value):
    '''
        rightful_religious_head = value

    '''
    return assign('rightful_religious_head', value)


def ruled_years(value):
    '''
        ruled_years = value

    '''
    return assign('ruled_years', value)


def same_guardian(value):
    '''
        same_guardian = value

    '''
    return assign('same_guardian', value)


def same_liege(value):
    '''
        same_liege = value

    '''
    return assign('same_liege', value)


def same_realm(value):
    '''
        same_realm = value

    '''
    return assign('same_realm', value)


def same_religion(value):
    '''
        same_religion = value

    '''
    return assign('same_religion', value)


def scaled_wealth(value):
    '''
        scaled_wealth = value

    '''
    return assign('scaled_wealth', value)


def stewardship(value):
    '''
        stewardship = value
        type: int
from: character
A character's Stewardship stat.
    '''
    return assign('stewardship', value)


def temporary(value):
    '''
        temporary = value

    '''
    return assign('temporary', value)


def terrain(value):
    '''
        terrain = value

    '''
    return assign('terrain', value)


def their_opinion(value):
    '''
        their_opinion = value

    '''
    return assign('their_opinion', value)


def tier(value):
    '''
        tier = value

    '''
    return assign('tier', value)


def title(value):
    '''
        title = value

    '''
    return assign('title', value)


def total_claims(value):
    '''
        total_claims = value

    '''
    return assign('total_claims', value)


def trait(value):
    '''
        trait = value
        type: string
from: character
Checks if the character has this trait, example : ( trait = shy )
    '''
    return assign('trait', value)


def treasury(value):
    '''
        treasury = value

    '''
    return assign('treasury', value)


def using_cb(value):
    '''
        using_cb = value

    '''
    return assign('using_cb', value)


def vassal_of(value):
    '''
        vassal_of = value

    '''
    return assign('vassal_of', value)


def war(value):
    '''
        war = value

    '''
    return assign('war', value)


def war_score(value):
    '''
        war_score = value

    '''
    return assign('war_score', value)


def war_with(value):
    '''
        war_with = value

    '''
    return assign('war_with', value)


def was_conceived_a_bastard(value):
    '''
        was_conceived_a_bastard = value

    '''
    return assign('was_conceived_a_bastard', value)


def wealth(value):
    '''
        wealth = value
        type: int
from: character
The minimum amount of gold this character should have
    '''
    return assign('wealth', value)


def would_be_heir_under_law(value):
    '''
        would_be_heir_under_law = value

    '''
    return assign('would_be_heir_under_law', value)


def year(value):
    '''
        year = value
        type: int
Returns the current year.
    '''
    return assign('year', value)
