import os
import re
from collections import OrderedDict

from ..code.prettify import prettify as pretty
from ..defines import EXTRA_HOLDING_TYPES
from ..graph import Graph

# idea for speeding this up.
#   first, look at how titlesets is implemented.
#   consider accessors for the "bys"
#       this would let self.by_type_set
#   consider dictionaries of dictionaries rather than sets.
#   consider memoizing get_buildings_in_set


class BuildingSets:
    def __init__(self):
        # need to account for not_if_x_exists
        #    and  Trigger
        self.by_name = {}
        self.by_type = {}
        self.by_gold = {}
        self.by_prestige = {}
        # these are unlikely to have any overlap, but might
        self.by_prerequisites = {}
        # self.sortable_preqs={}
        # self._sorted_preqs=None
        self.by_potential = {}
        # its possible that tech requirements could have overlap
        #    but so far, it hasn't occured much; and is generaly sublimated
        #    by preq, potential, or cost requirements.
        self.by_tech = {}
        # it's rather unlikely that upgrades_from would have any
        #   overlap
        self.by_upgrades = {}
        self.by_exclusive = {}
        # the set assigns an arbitrary integer ID to each
        #   string block.
        self.potenital_ids = OrderedDict()
        self.prerequisites_ids = OrderedDict()
        self.tech_ids = OrderedDict()
        self.upgrade_ids = OrderedDict()
        self.exclusive_with_ids = OrderedDict()
        self._cached_sorts = {'name': [], 'type': [], 'gold': []}

    def get_buildings_with_type(self, atype):
        return self.get_buildings_in_set(self.by_type[atype])

    def get_buildings_with_gold(self, gold):
        return self.get_buildings_in_set(self.by_gold[gold])

    def get_buildings_with_prestige(self, atype):
        return self.get_buildings_in_set(self.by_prestige[atype])

    def get_buildings_with_prerequisite(self, atype):
        return self.get_buildings_in_set(self.by_prerequisites[atype])

    def get_buildings_with_potential(self, atype):
        return self.get_buildings_in_set(self.by_potential[atype])

    def get_buildings_with_tech(self, atype):
        return self.get_buildings_in_set(self.by_tech[atype])

    def get_buildings_with_upgrade(self, atype):
        return self.get_buildings_in_set(self.by_upgrades[atype])

    def get_tree_graphs(self, loca=None):
        # builds a graph class that maps
        #   building prerequisities per type.
        graphs = {}
        if not loca:
            loca = []
        for atype in self.by_type:
            if len(self.by_type[atype]) > 0:
                ng = Graph(directed=True)
                for thing in self.by_type[atype]:
                    bldg = self.by_name[thing]
                    ng.add_label(thing,bldg.name_l())
                    # if thing in loca:
                    # thing=loca[thing][0]
                    if bldg.upgrades_from():
                        origin = bldg.upgrades_from()
                        ng.add(origin, thing)
                    if bldg.prerequisites():
                        for origin in bldg.prerequisites():
                            # if origin in loca:
                            # origin=loca[origin][0]
                            ng.add(origin, thing)
                graphs[atype] = ng
        return graphs

    def get_buildings_sorted_by_gold(self):
        if len(self._cached_sorts['gold']) < len(self.by_name):
            ret = []
            for gold in sorted(self.by_gold):
                # since get_buildings is sorted, this is
                #   probably faster. marginally.
                for building in self.get_buildings_sorted_by_name():
                    if building.build_cost_int() == gold:
                        ret.append(building)
            self._cached_sorts['gold'] = ret
        return self._cached_sorts['gold']

    def get_buildings_sorted_by_type(self):
        if len(self._cached_sorts['type']) < len(self.by_name):
            ret = []
            for atype in sorted(self.by_type):
                # since get_buildings is sorted, this is
                #   probably faster. marginally.
                for building in self.get_buildings_sorted_by_name():
                    if building.type() == atype:
                        ret.append(building)
            self._cached_sorts['type'] = ret
        return self._cached_sorts['type']

    def get_buildings_sorted_by_name(self):
        if len(self._cached_sorts['name']) < len(self.by_name):
            self._len_last_sort = len(self.by_name)
            self.by_name = OrderedDict(sorted(self.by_name.items()))
        # consider changing the way this works and adding sorted
        #   buildings.
        return self.by_name.values()

    def get_all_buildings(self):
        return self.get_buildings_sorted_by_name()

    # on reflection, decided this might be useful/generic enough to leave
    #   in base.
    def gen_building_trees(self, destination='', loca=None):
        ret = ''
        destination = destination + 'building_trees\\'
        if not os.path.exists(destination):
            os.makedirs(destination)
        for atype, graph in self.get_tree_graph(loca).items():
            with open(destination + atype + '.dot', 'w') as _fp:
                _fp.write(graph.to_dot())
            ret = ''
        with open(destination + 'readme.txt', 'w') as _fp:
            _fp.write("GraphViz dot files of building upgrade paths"
                      " and prerequisites.  Initially created to assist "
                      "debugging; continued inclusion due to potential for"
                      "end user utility.  GraphViz is available for free from"
                      " http://www.graphviz.org/.  A basic bat file is "
                      "included that calls dot to generate images.")
        with open(destination + 'run_dot.bat', 'w') as _fp:
            dotmask = '"{}" -Tjpg {}.dot -o {}.jpg\n'
            dot_path = 'C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe'
            for atype in self.by_type:
                _fp.write(dotmask.format(dot_path, atype, atype))
            _fp.write('pause\n')

    def set_has_extra_holding_types(self, aset):
        rem = aset
        for atype in EXTRA_HOLDING_TYPES:
            if atype in self.by_type:
                rem = rem.intersection(self.by_type[atype])
            if len(rem) == 0:
                return False
        return True

    def reduce_set_by_type(self, aset, atype):
        return aset.intersection(self.by_type[atype])

    def add(self, building):
        self.by_name[building.name()] = building
        if building.type() not in self.by_type:
            self.by_type[building.type()] = set()
        self.by_type[building.type()].add(building.name())

        bc_int = building.build_cost_int()
        if not self.by_gold.has_key(bc_int):
            self.by_gold[bc_int] = set()
        self.by_gold[bc_int].add(building.name())

        pc_int = building.prestige_cost_int()
        if not self.by_prestige.has_key(pc_int):
            self.by_prestige[pc_int] = set()
        self.by_prestige[pc_int].add(building.name())

        if building.upgrades_from():
            if building.upgrades_from() not in self.by_upgrades:
                self.by_upgrades[building.upgrades_from()] = set()
            self.by_upgrades[building.upgrades_from()].add(building.name())
            if not self.upgrade_ids.has_key(building.upgrades_from()):
                self.upgrade_ids[building.upgrades_from()] = building.name()

        if building.potential():
            key = pretty(building.potential())
            if key not in self.by_potential:
                self.by_potential[key] = set()
            self.by_potential[key].add(building.name())
            if not self.potenital_ids.has_key(key):
                self.potenital_ids[key] = building.name()

        if building.tech_required():
            key = ''.join(building.tech_required())
            if key not in self.by_tech:
                self.by_tech[key] = set()
            self.by_tech[key].add(building.name())
            if not self.tech_ids.has_key(key):
                self.tech_ids[key] = building.name()

        if building.prerequisites():
            key = str(set(building.prerequisites()))
            if key not in self.by_prerequisites:
                self.by_prerequisites[key] = set()
            self.by_prerequisites[key].add(building.name())
            if not self.prerequisites_ids.has_key(key):
                self.prerequisites_ids[key] = building.name()

        if building.prerequisites():
            key = str(set(building.prerequisites()))
            if key not in self.by_prerequisites:
                self.by_prerequisites[key] = set()
            self.by_prerequisites[key].add(building.name())
            if not self.prerequisites_ids.has_key(key):
                self.prerequisites_ids[key] = building.name()

        if building.exclusive_with():
            key = str(set(building.exclusive_with()))
            if key not in self.by_exclusive:
                self.by_exclusive[key] = set()
            self.by_exclusive[key].add(building.name())
            if not self.exclusive_with_ids.has_key(key):
                self.exclusive_with_ids[key] = building.name()

    def get_buildings_in_set(self, aset):
        ret = []
        for building in aset:
            ret.append(self.by_name[building])
        return ret

    # retained for debuging purposes
    def report(self):
        ret = '{} total buildings:'.format(len(self.by_name)) + '\n'
        for name, container in [
            ('Type', self.by_type),
            ('Gold', self.by_gold),
            ('Prestige', self.by_prestige),
            ('Preq', self.by_prerequisites),
            ('Potential', self.by_potential),
            ('Upgrades', self.by_upgrades),
            ('Tech', self.by_tech),
            ('Exclusive', self.by_exclusive),
        ]:
            in_cont = 0
            ret += '{} set:\n'.format(name)
            count = 0
            for thing in OrderedDict(
                    sorted(
                        container.items(), key=lambda x: len(x[1]))):
                if len(container[thing]) > 1:
                    count += 1
                    if name in ['Gold', 'Prestige', 'Type']:
                        string = str(thing)
                    else:
                        string = sorted(
                            list(container[thing]), key=lambda x: len(x))[0]
                        if len(string) > 15:
                            string = string[:12] + '...'
                        #string = str(count)
                    ret += '{:15} {:5} {}\n'.format(string,
                                                    str(len(container[thing])),
                                                    str(container[thing]))
                    in_cont += len(container[thing])
            ret += str(in_cont) + 'in container' + '\n\n'
        return ret
