from ..code.base import assign, make_b, make_if
from .. import utilities as ck2
from collections import OrderedDict

class TraitSet:

    '''
    Some utility functions for dealing with groups of traits.

    TODO: Ensure this si rewritten to pass "Trait" construct's and not string.
    '''

    def __init__(self):
        self._positive_genetic = set()
        self._agnatic = set()
        self._no_marriage = set()
        self._bad_traits = set()
        self._all_genetic = set()
        self.all_traits = {}
        self.sprites = {}
        self._localizations = {}
        # optimization for total benefit calculations
        self._total_benefits_memo = {}        
        # I really can't think of anything better than hardcoding.
        self._traitseries = 'genius->quick imbecile->slow'
        def _by_bonus = {}
        def _by_malus = {}

    def add(self, trait):
        # for my purposes, I can only add the 'second' version,
        #   so lets treat it as replacement
        if trait.name() in self.all_traits:
            # clear previous values, if they exist
            self._all_genetic.pop(trait.name(), 'None')
            self._positive_genetic.pop(trait.name(), 'None')
            self._bad_traits.pop(trait.name(), 'None')
            self._agnatic.pop(trait.name(), 'None')
            self._no_marriage.pop(trait.name(), 'None')
        self.all_traits[trait.name()] = trait        
        if trait.is_genetic():
            self._all_genetic.add(trait.name())

            val = self._total_benefit(trait.name())
            if val > 0:
                self._positive_genetic.add(trait.name())
            else:
                self._bad_traits.add(trait.name())
        if trait.agnatic:
            self._agnatic.add(trait.name())
        if trait.cannot_marry:
            self._no_marriage.add(trait.name())
        if trait.inbred:
            self._bad_traits.add(trait.name())
        #example of the right way to extend this generically
        for effect,value in trait.get_all_effects().items():
            if value>0:
                if effect not in self._by_bonus:
                    self._by_bonus[effect]=set()
                self._by_bonus[effect].add(trait.name())
            if value<0:
                if effect not in self._by_malus:
                    self._by_malus[effect]=set()
                self._by_malus[effect].add(trait.name())
    
    def get_traits_in_set(self,set_of_traits):
        ret={}
        for thing in set_of_traits:
            if thing in self.all_traits:
                ret[thing]=self.all_traits[thing]
        return ret

    def best_of_set(self, set_of_traits):
        top = []
        top_v = 0
        for trait in set_of_traits:
            ben = sum(self.all_traits[trait].get_all_effects().values())
            if ben > top_v:
                top_v = ben
                top = [trait]
            elif ben == top_v:
                top.append(trait)
        return top

    def get_trait_block(self, trait_name):
        return str(self.all_traits[trait])
        return make_b(trait, str(self.all_traits[trait]))

    def has_best_of_set(self, set_of_traits):
        '''
        Returns a generated decision, either trait = best
        or OR = {trait = tie1 trait = tie2} if tied.
        :param set_of_traits:
        '''
        top = self.best_of_set(set_of_traits)
        if len(top) > 1:
            return make_b('OR', '\n'.join([assign('trait', x) for x in top]))
        else:
            return assign('trait', top[0])

    def worst_of_set(self, list_of_traits):
        top = []
        top_v = 1000000000
        for trait in list_of_traits:
            ben = sum(self.all_traits[trait].get_all_effects().values())
            if ben < top_v:
                top_v = ben
                top = [trait]
            elif ben == top_v:
                top.append(trait)
        return top

    def has_worst_of_set(self, set_of_traits):
        '''
        Returns a generated decision, either trait = best
        or OR = {trait = tie1 trait = tie2} if tied.
        :param set_of_traits:
        '''
        top = self.worst_of_set(set_of_traits)
        if len(top) > 1:
            return make_b('OR', '\n'.join([assign('trait', x) for x in top]))
        else:
            return assign('trait', top[0])

### NOTHING BELOW THIS LINE IS REWRITTEN ###
### TODO: Finish rewriting methods below.

    def perfect_genes(self):
        ret = ''
        for set in self.get_pos_gen_traitsets():
            if len(set) > 1:
                ret += self.has_best_of_set(set)
            else:
                ret += assign('trait', list(set)[0])
        return ret
            
    def all_good_genes(self):
        ret = ''
        for set in self.get_pos_gen_traitsets():
            if len(set) > 1:
                ret += make_b('OR',
                              '\n'.join([assign('trait', x) for x in set]))
            else:
                ret += assign('trait', list(set)[0])
        return ret
    
    #TODO: Figure out how to rewrite this function.
    def _get_traitsets(self, container=None, filter=None):
        potential = []
        for trait in container:
            if not filter or filter(trait):
                this_set = set()
                this_set.add(trait)
                opp = self.get_opposites(trait)
                # print trait
                # print opp
                if opp:
                    for other in opp:
                        if not filter or filter(other):
                            this_set.add(other)
                if not filter or filter(trait):
                    potential.append(this_set)
        master = []
        for thing in potential:
            if master.count(thing) == 0:
                master.append(thing)
        return master

    def get_traitsets(self, filter=None):
        return self._get_traitsets(self.all_traits, filter)

    def get_genetic_traitsets(self, filter=None):
        '''
        builds 'sets' of traits that are exclusive.
        :param filter: Optional function to filter
            results.
        '''
        return self._get_traitsets(self._all_genetic, filter)

    def is_epidemic(self, trait):
        return self._get_value(trait, 'is_epidemic')

    def is_health(self, trait):
        health = self._get_value(trait, 'is_health')
        illness = self._get_value(trait, 'is_illness')
        epidemic = self._get_value(trait, 'is_epidemic')
        return health and not (epidemic or illness)

    def is_illness(self, trait):
        illness = self._get_value(trait, 'is_illness')
        epidemic = self._get_value(trait, 'is_epidemic')
        return illness and not epidemic

    def get_health_traitsets(self):
        return self.get_traitsets(self.is_health)

    def get_illness_traitsets(self):
        return self.get_traitsets(self.is_illness)

    def get_pos_gen_traitsets(self):
        return self.get_genetic_traitsets(self.is_positive)

    def get_neg_gen_traitsets(self):
        return self.get_genetic_traitsets(self.is_negative)

    def _trait_cmp(self, trait1, trait2):
        all_stronger = True
        all_weaker = True
        both_pos = self.is_positive(trait1) and self.is_positive(trait2)
        both_neg = self.is_negative(trait1) and self.is_negative(trait2)
        if not (both_neg or both_pos):
            # no valid comparison
            return False, False
        for bonus in self.attribute_bonuses:
            t1v = self._get_value(trait1, bonus)
            t2v = self._get_value(trait2, bonus)
            if t1v:
                if not t2v:
                    all_stronger = False
                    all_weaker = False
                else:
                    if both_pos:
                        if t1v < t2v:
                            all_stronger = False
                        else:
                            all_weaker = False
                    if both_neg:
                        if t1v < t2v:
                            all_stronger = False
                        else:
                            all_weaker = False
        print trait1, trait2, both_pos, both_neg, all_stronger, all_weaker
        return all_stronger, all_weaker

    def is_stronger_version(self, trait1, trait2):
        '''
        is trait 1 a stronger version of trait 2.
        :param trait1: trait 1 name
        :param trait2: trait 2 name
        '''
        if '{}->{}'.format(trait1, trait2) in self._traitseries:
            return True
        return False
        a, b = self._trait_cmp(trait1, trait2)
        return a

    def is_weaker_version(self, trait1, trait2):
        '''
        is trait 1 a lesser version of trait 2.
        :param trait1: trait 1 name
        :param trait2: trait 2 name
        '''
        if '{}->{}'.format(trait2, trait1) in self._traitseries:
            return True
        return False
        a, b = self._trait_cmp(trait1, trait2)
        return b

    def is_equivalent(self, trait1, trait2):
        return self._total_benefit(trait1) == self._total_benefit(trait2)

    def is_positive(self, trait):
        return self._total_benefit(trait) > 0

    def is_negative(self, trait):
        return self._total_benefit(trait) < 0

    def _total_benefit(self, trait):
        if trait in self._total_benefits_memo:
            return self._total_benefits_memo[trait]                    
        all_effects = self.all_traits[trait].get_all_effects()
        val = sum(all_effects.values())
        self._total_benefits_memo[trait] = val
        return val

    def get_name_localization(self, trait):
        return self.all_traits[trait].name_l()
        #if trait in self._localizations:
            #return self._localizations[trait]
        #return None

    def get_desc_localization(self, trait):
        if trait + '_desc' in self._localizations:
            return self._localizations[trait + '_desc']
        return None

    def localize_trait(self, trait, lines):
        ret = []
        for line, loca in zip(lines, self._localizations[trait]):
            if loca == '':
                loca = trait
            ret.append(line.format(loca))
        return ret

    def add_localizations(self, localization_dict):
        for trait in self.all_traits:
            if trait in localization_dict:
                self.all_traits[trait].localized_names = localization_dict[trait]
            if trait + '_desc' in localization_dict:
                self._localizations[
                    trait + '_desc'] = localization_dict[trait + '_desc']

    def _get_value(self, trait, variable):
        if trait not in self.all_traits:
            print trait, 'not found...'
            return None
        for thing in self.all_traits[trait]:
            if hasattr(thing, 'value'):
                if thing.variable == variable:
                    return thing.value
        return None

    def get_inherit_chance(self, trait):
        return self._get_value(trait, 'inherit_chance')
        for thing in self.all_traits[trait]:
            if hasattr(thing, 'value'):
                if thing.variable == 'inherit_chance':
                    return thing.value
        return None

    def get_opposites(self, trait):
        for thing in self.all_traits[trait]:
            if hasattr(thing, '__name__') and thing.__name__ == 'opposites':
                return [str(x).partition('#')[0].replace('\n', '')
                        for x in thing]
        return []

    def num_good(self):
        return len(self._positive_genetic.keys())

    def is_genetic(self, trait):
        return trait in self._all_genetic.keys()


    def report(self):
        print 'positive genetic', self._positive_genetic.keys()
       # print 'positive agnatic', self._positive_agnatic.keys()
        print 'no_marriage', self._no_marriage.keys()
        print 'bad', self.bad_traits.keys()

    def good_genes(self):
        return self.or_traitlist(self._positive_genetic)

    def or_traitlist(self, list_of_traits):
        ret = ''
        for trait in list_of_traits:
            ret += assign('trait', trait)
        if len(list_of_traits) > 1:
            return make_b('OR', ret)
        return ret

    def not_traitlist(self, list_of_traits):
        ret = ''
        for trait in list_of_traits:
            ret += assign('trait', trait)
        if len(list_of_traits) >= 1:
            return make_b('NOT', ret)
        return ret

    def and_traitlist(self, list_of_traits):
        ret = ''
        for trait in list_of_traits:
            ret += assign('trait', trait)
        if len(list_of_traits) >= 1:
            return make_b('AND', ret)
        return ret

    def bloodline(self):
        # practically speaking, not terribly useful
        return self.or_traitlist(self._agnatic)

    def _bad_trait_string(self):
        ret = ''
        for trait in self._bad_traits:
            ret += assign('trait', trait)
        return ret

    def bad_traits(self):
        return self.or_traitlist(self._positive_genetic)
        return make_b('OR', self._bad_trait_string())

    def no_bad_traits(self):
        return make_b('NOT', self._bad_trait_string())

    def _marriage_blocking_traits_string(self):
        ret = ''
        for trait in self._no_marriage:
            ret += assign('trait', trait)
        return ret

    def add_sprites(self, interfaceparse):
        for thing in interfaceparse:
            if thing.__name__ == 'spriteTypes':
                for sprite in thing:
                    if hasattr(sprite, '__name__') and sprite.__name__ == 'spriteType':
                        if 'GFX_trait' in str(sprite):
                            traitname = str(sprite).partition(
                                'GFX_trait_')[2].partition('"')[0]
                            # print 'found ', traitname
                            self.sprites[traitname] = sprite

    def load_path(self, path):
        raw = ck2.get_mod_traits(path)
        for trait in raw:
            self.add(trait)
        self.add_localizations(ck2.get_mod_localization(path))
        self.add_sprites(ck2.get_mod_interface(path))

    def add_all(self, iterable):
        for trait in iterable:
            self.add(trait)

    def update(self, other_traitset):
        self.update_traits(other_traitset)
        self.update_loca(other_traitset)
        self.update_sprites(other_traitset)

    def update_traits(self, other_traitset):
        self._positive_genetic.update(other_traitset._positive_genetic)
        self._agnatic.update(other_traitset._agnatic)
        self._no_marriage.update(other_traitset._no_marriage)
        self._bad_traits.update(other_traitset._bad_traits)
        self._all_genetic.update(other_traitset._all_genetic)
        self.all_traits.update(other_traitset.all_traits)

    def update_sprites(self, other_traitset):
        self.sprites.update(other_traitset.sprites)

    def update_loca(self, other_traitset):
        self._localizations.update(other_traitset._localizations)

    def not_marriable(self):
        return make_b('OR', self._marriage_blocking_traits_string())

    def marriable(self):
        return make_b('NOT', self._marriage_blocking_traits_string())
