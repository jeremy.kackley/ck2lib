from .. import utilities as ck2
import re


class TitleSet:

    def __init__(self):
        self.titles = {}
        self.mercenaries = set()
        self.holy_orders = set()
        self.religion = {}
        self.culture = {}
        self._titles_from_path = {}
        self.localizations = {}

    def update(self, other):
        self.titles.update(other.titles)
        self.mercenaries = self.mercenaries.union(other.mercenaries)
        self.holy_orders = self.holy_orders.union(other.mercenaries)
        self.religion.update(other.religion)
        self.culture.update(other.culture)
        self._titles_from_path.update(other._titles_from_path)
        self.update_loca(other)

    def update_loca(self, other):
        self.localizations.update(other.localizations)

    # localization convience functions
    def english(self, title):
        return self.localizations[title][0]

    def french(self, title):
        ret = self.localizations[title][1]
        if not ret:
            ret = self.english(title)
        return ret

    def german(self, title):
        ret = self.localizations[title][2]
        if not ret:
            ret = self.english(title)
        return ret

    def spanish(self, title):
        ret = self.localizations[title][3]
        if not ret:
            ret = self.english(title)
        return ret

    def mercenaries_in_last_mod(self):
        return self.mercenaries_in_mod(self._last_path)

    def mercenaries_in_mod(self, path):
        return len(self._titles_from_path[path].intersection(self.mercenaries))

    def titles_in_last_mod(self):
        return self.titles_in_mod(self._last_path)

    def titles_in_mod(self, path):
        return len(self._titles_from_path[path])

    def get_value(self, title, variable):
        for thing in self.titles[title]:
            if hasattr(thing, 'value'):
                if thing.variable == variable:
                    return thing.value
        return None

    def get_block(self, title, blockname):
        for thing in self.titles[title]:
            if hasattr(thing, '__name__'):
                if thing.__name__ == blockname:
                    return thing
        return None

    def _update_set(self, dict, trait, variable):
        trait = trait.__name__
        value = self.get_value(trait, variable)
        if value:
            if value not in dict:
                dict[value] = set()
            dict[value].add(trait)

    def get_duchies(self, title):
        titlestr = str(self.titles[title])
        return re.findall('d_\w+', titlestr)

    def get_counties(self, title):
        counties = {}
        titlestr = str(self.titles[title])
        all = re.findall('d_\w+|c_\w+', titlestr)
        current = None
        for county in all:
            if 'd_' in county:
                current = county
            else:
                if current:
                    counties[county] = current
        return counties

    def _initialize_locas(self, title):
        self.localizations[title] = ['', '', '', '']

    def load_path(self, path):
        self._last_path = path
        self._titles_from_path[path] = set()
        # print 'loading path',path
        raw = ck2.get_mod_titles(path)
        for title in raw:
            if hasattr(title, '__name__'):
                self.titles[title.__name__] = title
                self._titles_from_path[path].add(title.__name__)
                merc = self.get_value(title.__name__, 'mercenary')
                holy_order = self.get_value(title.__name__, 'holy_order')
                if merc == 'yes' and not holy_order == 'yes':
                    self.mercenaries.add(title.__name__)
                elif holy_order == 'yes':
                    self.holy_orders.add(title.__name__)
                self._update_set(self.religion, title, 'religion')
                self._update_set(self.culture, title, 'culture')
                if not title.__name__ in self.localizations:
                    self._initialize_locas(title.__name__)
        self.localizations.update(ck2.get_mod_localization(path))
