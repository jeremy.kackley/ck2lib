# this file holds defines common to CK2 that don't
#   change very often.


import re
import ctypes.wintypes


def find_current_user_docs_dir():
    csidl_personal = 5  # My Documents
    shgfp_type_current = 0  # Want current, not default value
    buf = ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(
        0, csidl_personal, 0, shgfp_type_current, buf)
    my_docs_base = buf.value
    return my_docs_base

EXTRA_HOLDING_TYPES = ['fort', 'trade_post', 'hospital']

BLOCK_GENERATES_EMPTY_ITEMS = True
PRETTIFY_WITH_SPACES = False
PRETTY_SPACES_TO_USE = 4
# move this to Config File
CK2DIR = r"D:\Games\steam\steamapps\common\Crusader Kings II"
try:
    MODSFOLDER = find_current_user_docs_dir(
    ) + "\\Paradox Interactive\\Crusader Kings II\\mod\\"
except:
    print "Locating mods folder failed, please set MODSFOLDER manually."
    MODSFOLDER = "..\\"
    raise
VERBOSE_SIMPLIFY = True
NOTIFY_MISSING_DIRECTORIES = False
# GENERATE_EMPTY_ITEMS if true, generates code for empty things
# PRETTIFY_WITH_SPACES if fales uses PRETTY_SPACES_TO_USE spaces instead of tab.
# PRETTY_SPACES_TO_USE controls how many spaces for each 'tab' if
# PRETTIFY_WITH_SPACES is enabled.

# global vars
LINES_PARSED = 0

# import __cfunc__

MODIFIERS = [
    "diplomacy",
    "diplomacy_penalty",
    "stewardship",
    "stewardship_penalty",
    "martial",
    "martial_penalty",
    "intrigue",
    "intrigue_penalty",
    "learning",
    "learning_penalty",
    "fertility",
    "fertility_penalty",
    "health",
    "health_penalty",
    "combat_rating",
    "threat_decay_speed",
    "demesne_size",
    "vassal_limit",
    "global_revolt_risk",
    "local_revolt_risk",
    "disease_defence",
    "culture_flex",
    "religion_flex",
    "short_reign_length",
    "moved_capital_months_mult",
    "assassinate_chance_modifier",
    "arrest_chance_modifier",
    "plot_power_modifier",
    "murder_plot_power_modifier",
    "defensive_plot_power_modifier",
    "plot_discovery_chance",
    "tax_income",
    "global_tax_modifier",
    "local_tax_modifier",
    "nomad_tax_modifier",
    "monthly_character_prestige",
    "liege_prestige",
    "add_prestige_modifier",
    "monthly_character_piety",
    "liege_piety",
    "add_piety_modifier",
    "monthly_character_wealth",
    "ai_rationality",
    "ai_zeal",
    "ai_greed",
    "ai_honor",
    "ai_ambition",
    "ai_feudal_modifier",
    "ai_republic_modifier",
    "build_cost_modifier",
    "build_cost_castle_modifier",
    "build_cost_city_modifier",
    "build_cost_temple_modifier",
    "build_cost_tribal_modifier",
    "build_time_modifier",
    "build_time_castle_modifier",
    "build_time_city_modifier",
    "build_time_temple_modifier",
    "build_time_tribal_modifier",
    "local_build_cost_modifier",
    "local_build_cost_castle_modifier",
    "local_build_cost_city_modifier",
    "local_build_cost_temple_modifier",
    "local_build_cost_tribal_modifier",
    "local_build_time_modifier",
    "local_build_time_castle_modifier",
    "local_build_time_city_modifier",
    "local_build_time_temple_modifier",
    "local_build_time_tribal_modifier",
    "ambition_opinion",
    "vassal_opinion",
    "sex_appeal_opinion",
    "same_opinion",
    "same_opinion_if_same_religion",
    "opposite_opinion",
    "liege_opinion",
    "general_opinion",
    "twin_opinion",
    "dynasty_opinion",
    "male_dynasty_opinion",
    "female_dynasty_opinion",
    "child_opinion",
    "oldest_child_opinion",
    "youngest_child_opinion",
    "spouse_opinion",
    "same_religion_opinion",
    "infidel_opinion",
    "christian_opinion",
    "christian_church_opinion",
    "church_opinion",
    "temple_opinion",
    "temple_all_opinion",
    "town_opinion",
    "city_opinion",
    "castle_opinion",
    "feudal_opinion",
    "tribal_opinion",
    "unreformed_tribal_opinion",
    "rel_head_opinion",
    "free_invest_vassal_opinion",
    "clan_sentiment",
    "levy_size",
    "global_levy_size",
    "levy_reinforce_rate",
    "land_morale",
    "land_organisation",
    "regiment_reinforcement_speed",
    "garrison_size",
    "global_garrison_size",
    "garrison_growth",
    "fort_level",
    "global_defensive",
    "land",
    "global_supply_limit",
    "global_winter_supply",
    "supply_limit",
    "max_attrition",
    "attrition",
    "days_of_supply",
    "siege_defence",
    "siege_speed",
    "global_movement_speed",
    "local_movement_speed",
    "galleys_perc",
    "retinuesize",
    "retinuesize_perc",
    "retinue_maintenence_cost",
    "horde_maintenence_cost",
    "tech_growth_modifier",
    "commander_limit",
    "max_tradeposts",
    "tradevalue",
    "trade_route_wealth",
    "trade_route_value",
    "global_trade_route_wealth",
    "global_trade_route_value",
    "global_tradevalue",
    "global_tradevalue_mult",
    "population_growth",
    "manpower_growth",
    "max_population_mult",
    "max_manpower_mult",
    "max_population",
]

TRAIT_NUMBERS =[
    'birth',
    'caste_tier',
    'inherit_chance',
    'both_parent_has_trait_inherit_chance',
    'leadership_traits',
]
TRAIT_STRINGS =[
    'agnatic',
    'blinding',
    'cached',
    'can_hold_titles',
    'cannot_inherit',
    'cannot_marry',
    'customizer',
    'education',
    'attribute',
    'enatic',
    'hidden',
    'immortal',
    'in_hiding',
    'inbred',
    'incapacitating',
    'is_epidemic',
    'is_health',
    'is_illness',
    'leader',
    'childhood',
    'lifestyle',
    'personality',
    'prevent_decadence',
    'priest',
    'pilgrimage',
    'random',
    'rebel_inherited',
    'religious',
    'religious_branch',
    'same_trait_visibility',
    'hidden',
    'hidden_from_others',
    'vice',
    'virtue',
    'succession_gfx',
    'is_symptom',
]
TRAIT_BLOCKS=[
    'opposites',
    'is_visible',
]

MINOR_TITLE_NUMBERS=[
    'dignity',
    'grant_limit',
    'opinion_effect',    
    'monthly_salary',
    'monthly_prestige',
]
MINOR_TITLE_STRINGS = [
    'dignity',
    'is_high_prio',
    'show_as_title',
    'realm_in_name',
    'is_voter',
    'attribute',
    'is_unique',
    'message',
    'patrician_heir'
]
MINOR_TITLE_BLOCKS = [
    'allowed_to_hold',
    'allowed_to_grant',
    'gain_effect',
    'revoke_trigger',
    'lose_effect',
    'retire_effect',
]