#pylint: disable=E1101
from basic import BLOCK, ASSIGNMENT
from .. import defines
from collections import OrderedDict


class Trait:
    def __init__(self, raw_parse=None):
        """Just a little class with some accessors.

        Args:
            raw_parse (TYPE): Description
        """
        self._raw_parse = raw_parse
        self.localized_names = {}
        self.clear()
        if raw_parse:
            for thing in raw_parse:
                if isinstance(thing,BLOCK):
                    self.__dict__[thing.__name__]=thing
                if isinstance(thing,ASSIGNMENT):
                    if thing.variable in defines.TRAIT_NUMBERS or defines.MODIFIERS:
                        self.__dict__[thing.variable]=int(float(thing.value))
                    else:
                        self.__dict__[thing.variable]=thing.value           
    
    def clear(self):
        #initialization
        for thing in defines.TRAIT_NUMBERS:
            self.__dict__[thing] = None
        for thing in defines.TRAIT_STRINGS:
            self.__dict__[thing] = None
        for thing in defines.TRAIT_BLOCKS:
            self.__dict__[thing] = None
            
    def get_all_effects(self):
        ret = OrderedDict()
        for thing in self.__dict__.keys():
            if thing in defines.MODIFIERS:
                if self.__dict__[thing]:
                    ret[thing]=self.__dict__[thing]
        return ret

    def get_all_positive_effects(self):
        ret = OrderedDict()
        for thing,value in self.get_all_effects().items():
            if value>0:
                ret[thing]=value
        return ret

    def get_all_negative_effects(self):
        ret = OrderedDict()
        for thing,value in self.get_all_effects().items():
            if value<0:
                ret[thing]=value
        return ret

    def is_beneficial(self):
        return len(self.get_all_positive_effects)>0

    def is_negative(self):
        return len(self.get_all_negative_effects)>0

    def is_inheritable(self):
        return self.has_inherit_chance() or self.agnatic or self.enatic

    def has_inherit_chance(self):
        if self.inherit_chance>0 or self.both_parent_has_trait_inherit_chance>0:
            if self.inherit_chance<100 or self.both_parent_has_trait_inherit_chance<100:
                return True
        return False
    
    def is_genetic(self):
        return self.has_inherit_chance() and not not (self.is_health or self.is_illness or self.is_epidemic) 

    def name(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.__name__

    def name_l(self, language='en'):
        """Summary

        Args:
            language (str, optional): Description

        Returns:
            TYPE: Description
        """
        #language is english,french,german,spanish
        language = language.lower()
        if self.localized_names.has_key(language):
            # has the key, e.g. was initialized properly
            if self.localized_names[language]:
                # the key is not null
                return self.localized_names[language]
        if self.localized_names.has_key('en'):
            # initalized properly
            if self.localized_names['en']:
                # english key was set
                return self.localized_names['en']
        # otherwise, I guess we default to the name.
        return self.name()