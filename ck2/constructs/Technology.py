
class Technology:
    def __init__(self, group,raw_parse):
        """Just a little class with some accessors.

        Args:
            raw_parse (TYPE): Description
        """
        self.group=group
        self._raw_parse = raw_parse
        self.localized_names = {}
    def name(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.__name__
    def modifier(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.get_block('modifier')
    def local_build_time_modifier(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self.modifier().get_assignment('LOCAL_BUILD_TIME_MODIFIER',exact=False)
    def local_build_cost_modifier(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self.modifier().get_assignment('LOCAL_BUILD_COST_MODIFIER',exact=False)         