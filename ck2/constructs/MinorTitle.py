
from basic import BLOCK, ASSIGNMENT
from .. import defines

class MinorTitle:
    def __init__(self, raw_parse=None):
        """Just a little class with some accessors.

        Args:
            raw_parse (TYPE): Description
        """
        self._raw_parse = raw_parse
        self.localized_names = {}
        self.clear()
        if raw_parse:
            for thing in raw_parse:
                if isinstance(thing,BLOCK):
                    self.__dict__[thing.__name__]=thing
                if isinstance(thing,ASSIGNMENT):
                    if thing.variable in defines.MINOR_TITLE_NUMBERS:
                        self.__dict__[thing.variable]=int(float(thing.value))
                    else:
                        self.__dict__[thing.variable]=thing.value           
    
    def clear(self):
        #initialization
        for thing in defines.MINOR_TITLE_NUMBERS:
            self.__dict__[thing] = None
        for thing in defines.MINOR_TITLE_STRINGS:
            self.__dict__[thing] = None
        for thing in defines.MINOR_TITLE_BLOCKS:
            self.__dict__[thing] = None
            

    def name(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.__name__

    def name_l(self, language='en'):
        """Summary

        Args:
            language (str, optional): Description

        Returns:
            TYPE: Description
        """
        #language is english,french,german,spanish
        language = language.lower()
        if self.localized_names.has_key(language):
            # has the key, e.g. was initialized properly
            if self.localized_names[language]:
                # the key is not null
                return self.localized_names[language]
        if self.localized_names.has_key('en'):
            # initalized properly
            if self.localized_names['en']:
                # english key was set
                return self.localized_names['en']
        # otherwise, I guess we default to the name.
        return self.name()