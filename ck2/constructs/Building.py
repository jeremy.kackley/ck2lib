# -*- coding: cp1252 -*-
"""Summary

Attributes:
    TYPE_LOCALIZATION (TYPE): Description
"""
import re

from ..code.base import assign, make_b
from basic import Comment
from ..defines import EXTRA_HOLDING_TYPES

TYPE_LOCALIZATION = {
    'fort': {
        'en': 'fort',
        'fr': 'fort',
        'de': 'fort',
        'es': 'fuerte',
    },
    'trade_post': {
        'en': 'trade post',
        'fr': 'Poste de commerce',
        'de': 'Handelsposten',
        'es': 'Puesto comercial',
    },
    'family_palace': {
        'en': 'family palace',
        'fr': 'Palais de la famille',
        'de': 'Familienpalast',
        'es': 'Palacio de la familia',
    },
    'hospital': {
        'en': 'hospital',
        'fr': 'h�pital',
        'de': 'Krankenhaus',
        'es': 'hospital',
    },
    'castle': {
        'en': 'castle',
        'fr': 'Ch�teau',
        'de': 'Schloss',
        'es': 'castillo',
    },
    'city': {
        'en': 'city',
        'fr': 'ville',
        'de': 'Stadt',
        'es': 'ciudad',
    },
    'temple': {
        'en': 'temple',
        'fr': 'temple',
        'de': 'Tempel',
        'es': 'templo',
    },
    'nomad': {
        'en': 'nomad',
        'fr': 'nomade',
        'de': 'Nomade',
        'es': 'n�mada',
    },
    'tribal': {
        'en': 'tribal',
        'fr': 'tribal',
        'de': 'Stammes-',
        'es': 'tribal',
    },
}


class Building:
    '''
    Will handle parsing/storing of info related to a
    given building; also handles generation of events.

    Attributes:
        localized_names (dict): Description
    '''

    def __init__(self, holding_type, building_parse):
        """Summary

        Args:
            holding_type (TYPE): Description
            building_parse (TYPE): Description
        """
        self._holding_type = holding_type
        self._building_parse = building_parse
        self.localized_names = {}
        self._technology=None

    def name_l(self, language='en'):
        """Summary

        Args:
            language (str, optional): Description

        Returns:
            TYPE: Description
        """
        #language is english,french,german,spanish
        language = language.lower()
        if self.localized_names.has_key(language):
            # has the key, e.g. was initialized properly
            if self.localized_names[language]:
                # the key is not null
                return self.localized_names[language]
        if self.localized_names.has_key('en'):
            # initalized properly
            if self.localized_names['en']:
                # english key was set
                return self.localized_names['en']
        # otherwise, I guess we default to the name.
        return self.name()

    def get_raw_parse(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._building_parse

    def requires_port(self):
        """Summary

        Returns:
            TYPE: Description
        """
        req = self._building_parse.get_assignment('port')
        if req and req == 'yes':
            return True
        return False

    def type(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._holding_type

    def type_l(self, language):
        """Summary

        Args:
            language (TYPE): Description

        Returns:
            TYPE: Description
        """
        return TYPE_LOCALIZATION[self.type()][language]

    def name(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._building_parse.__name__

    def potential(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._building_parse.get_block('potential')

    def build_cost(self):
        """Summary

        Returns:
            TYPE: Description
        """
        # pre 2.2
        bc = self._building_parse.get_assignment('build_cost')
        if not bc:
            # post 2.2
            return self._building_parse.get_assignment('gold_cost')
        return bc

    def build_cost_int(self):
        """Summary

        Returns:
            TYPE: Description
        """
        if self.build_cost():
            return int(float(self.build_cost()))
        return None

    def build_cost_is_positive_integer(self):
        """Summary

        Returns:
            TYPE: Description
        """
        cost = self.build_cost()
        if cost and int(float((cost))) > 0:
            return True
        return False

    def prestige_cost(self):
        '''
        This is only a factor for tribal buildings...probably.
        '''
        # prestige_cost
        return self._building_parse.get_assignment('prestige_cost')

    def prestige_cost_int(self):
        """Summary

        Returns:
            TYPE: Description
        """
        if self.prestige_cost():
            return int(float(self.prestige_cost()))
        return None

    def build_time(self):
        """A string value representing the build time of the building.

        Returns:
            string: The build time as a string, or None.
        """
        return self._building_parse.get_assignment('build_time')

    def build_time_int(self):
        """Returns the build time of this building as an integer, or None.

        Returns:
            int: building.build_time() casted to an integer, or None.
        """
        if self.build_time():
            return int(float(self.build_time()))
        return None

    def tech_required(self):
        """Extracts the technology required to build the building from the trigger block.

        TODO: verify that only technology is contained in the trigger block.

        Returns:
            list: A list of technology assignment statements (e.g. CONSTRUCTION = 5) or None
        """
        # I need to revisit when and why this is called.
        block = self._building_parse.get_block('trigger')        
        if block:
            ret = []
            for thing in block.get_all_assignments():
                if thing.variable in self._technology:
                    ret.append(assign(thing.variable, thing.value))
            if ret:
                return ret
        return None

    def exclusive_with(self):
        """Returns a list of buildings that the building is exclusive with, e.g. it cannot be built if they exist.  In game this is defined by the not_if_x_exists block.

        Returns:
            list: a list of strings, or None.
        """
        block = self._building_parse.get_block('not_if_x_exists')
        if block:
            ret = self._extract_text_from_block(block)
            if ret:
                return ret
        return None

    def _extract_text_from_block(self, block):
        """Walks a block (as returned by the parser, see utilities.py) and extracts all the text elements from it.

        Args:
            block (BLOCK): A Block returned from the parser.

        Returns:
            list: A list containing all the strings in the parsed block.  
        """
        ret = []
        for thing in block:
            not_comment = not isinstance(thing, Comment)
            if not_comment and hasattr(thing, 'text'):
                if ' ' in thing.text:
                    ret = ret + re.split(' ', thing.text)
                else:
                    ret.append(thing.text)
        return ret

    def prerequisites(self):
        """Returns a list of buildings that must exist in order for this building to be build.

        Returns:
            list: A list of strings representing building names.
        """
        block = self._building_parse.get_block('prerequisites')
        if block:
            ret = self._extract_text_from_block(block)
            if ret:
                return ret
        return []

    def upgrades_from(self):
        """returns the upgrades_from assignment from the building.  This assignment indicates the building that this upgrades, e.g. ca_walls_2 to ca_walls_3.

        Returns:
            string: a building name, or none
        """
        return self._building_parse.get_assignment('upgrades_from')

    def desc(self):
        """returns the desc assignment from the building.  This is the localization key for the building description.

        Returns:
            string: a localization string, or none
        """
        return self._building_parse.get_assignment('desc')

    def bonuses(self):
        """Attempts to parse the block contents to determine the building bonuses for this building.

        Returns:
            list: A list of building bonuses.
        """
        ret = []
        for thing in self._building_parse:
            if isinstance(thing, ck2.ASSIGNMENT):
                if thing.variable not in [
                        'build_cost',
                        'build_time',
                        'desc',
                        'ai_creation_factor',
                        'upgrades_from',
                        'extra_tech_building_start',
                        'add_number_to_name',
                        'prerequisites',
                        # 'replaces',
                        '-no',  # not sure what this is...
                        '365',
                        '300',
                        'auto_add_at_year',
                        'port',
                ]:
                    ret.append(thing)
        return ret

    def is_extra_holding_type(self):
        """Returns True if building is an extra holding type; e.g. not a title and uses commands/conditions from the province scope.

        Returns:
            bool: True if is extra holding type, False otherwise.
        """
        return self.type() in EXTRA_HOLDING_TYPES

    def has_block(self):
        """returns the appropraite building exists block, e.g. has_building = buildingname

        Returns:
            string: a string of CK2 code
        """
        if self.type() == 'trade_post':
            return assign('trade_post_has_building', self.name())
        elif self.type() == 'hospital':
            return assign('hospital_has_building', self.name())
        else:
            return assign('has_building', self.name())

    def add_block(self):
        """Returns the appropraite add building block, e.g. add_to_extra_holding or add_building

        Returns:
            string: a string of CK2 code
        """
        ret = ''
        if self.is_extra_holding_type():
            ret = assign('type', self.type())
            ret += assign('building', self.name())
            ret = make_b('add_to_extra_holding', ret)
        else:
            ret = assign('add_building', self.name())
        return ret.replace('\n', ' ') + '\n'

    def remove_block(self):
        """returns the appropraite destroy building block, e.g. remove_building or destroy_in_extra_holding

        Returns:
            string: a string of CK2 code
        """
        ret = ''
        if self.is_extra_holding_type():
            ret = assign('type', self.type())
            ret += assign('building', self.name())
            ret = make_b('destroy_in_extra_holding', ret)
        else:
            ret = assign('remove_building', self.name())
        return ret.replace('\n', ' ') + '\n'

    def remove_precursor_block(self):
        """returns the appropraite remove precursor building block

        e.g. remove_building = building_i_replace or 
        destroy_in_extra_holding = building_i_replace

        Returns:
            string: a string of CK2 code
        """
        ret = ''
        if self.replaces():
            if self.is_extra_holding_type():
                ret = assign('type', self.type())
                ret += assign('building', self.replaces())
                ret = make_b('destroy_in_extra_holding', ret)
            else:
                ret = assign('remove_building', self.replaces())
            ret = ret.replace('\n', ' ') + '\n'
        return ret

    def replaces(self):
        """Summary

        Returns:
            string: A building name that I replace, or None
        """
        return self._building_parse.get_assignment('replaces')
