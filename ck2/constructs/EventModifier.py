
class EventModifier:
    def __init__(self,raw_parse):
        """Just a little class with some accessors.

        Args:
            raw_parse (TYPE): Description
        """
        self._raw_parse = raw_parse
        self.localized_names = {}
    def name(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.__name__
    def local_build_time_modifier(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.get_assignment('local_build_time_modifier')
    def local_build_cost_modifier(self):
        """Summary

        Returns:
            TYPE: Description
        """
        return self._raw_parse.get_assignment('local_build_cost_modifier')            