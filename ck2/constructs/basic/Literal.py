from collections import namedtuple

class Literal(namedtuple('LiteralBase', ['text', 'comment', 'parent'])):
    __slots__ = ()

    def __eq__(self, other):
        if isinstance(other, Literal):
            if self.text == other.text:
                return True
       # print 'bad assignment',self,other
        return False

    def __str__(self):
        tail = '\n'
        if self.comment:
            tail = str(self.comment)
        return str(self.text) + tail

# class Literal(Ck2TextObject):
#     __slots__ = ('__parent__', 'text', '__comment__')
#     def __init__(self, text, comment=None):
#         Ck2TextObject.__init__(self, text)
#         self.__comment__ = None
#         if comment:
#             self.__comment__ = Comment(comment)
#
#
#     def __repr__(self):
#         return 'Literal("{}","{}"'.format(self.text, self.__comment__)
#
#     def __eq__(self, other):
#         if isinstance(other, Literal):
#             if self.text == other.text:
#                 return True
# print 'bad assignment',self,other
#         return False
#
#     def __str__(self):
#         tail = '\n'
#         if self.__comment__:
#             tail = str(self.__comment__)
#         return str(self.text) + tail

# add naming,comments, and such.
# class Ck2NamedObject(Ck2Object):
# def __init__(self,__text):
# Ck2TextObject.__init__(self)
# if comment:
# self.__comment__=Comment(comment)

# considering making literals and
# namedtuple('AssignmentBase', ['variable', 'value', 'comment'])