import Assignment as AssignmentModule
import Block as BlockModule
import Literal as LiteralModule
import Comment as CommentModule

ASSIGNMENT = AssignmentModule.ASSIGNMENT
BLOCK = BlockModule.BLOCK
Literal = LiteralModule.Literal
Comment = CommentModule.Comment