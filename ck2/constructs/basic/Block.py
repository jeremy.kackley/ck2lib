from Assignment import ASSIGNMENT
from ...code.prettify import prettify as pretty
from ... import defines

def strip_custom_tooltip_from_string(block,string):
    for custom_tooltip in block.get_all_blocks('custom_tooltip'):         
        string=string.replace(str(custom_tooltip).strip(),'')
    return string.replace('\n\n','\n')

def string_compare(string1,string2):    
    if string1==string2:
        return True
    if string1==string2.swapcase():
        return True
    if string1==string2.upper():
        return True
    if string1==string2.lower():
        return True
    if string1==string2.capitalize():
        return True
    if string1==string2.title():
        return True
    return False

class BLOCK(list):
    __slots__ = ('__parent__',
                 '__contents__',
                 '__closedbyparser__',
                 '__name__',
                 '__comment__')

    def __init__(self, name, contents=None, comment=None):
        if not contents:
            contents = []
        list.__init__(self, contents)
        self.__parent__ = None
        # print 'new block',name,type(contents)
        self.__name__ = name
        self.__comment__ = None
        if comment:
            # self.__comment__ = Comment(comment)
            self.__comment__ = comment
        self.update(contents)
        self.__closedbyparser__ = False

    def update(self, contents):
        self[:]
        if isinstance(contents, type('')):
            contents = [parse(contents)]
        self[0:] = contents[0:]
        # self.__contents__ = tuple(contents)
        self.__update_children__()

    def remove(self, thing):
        contents = [x for x in self if x != thing]
        self.update(contents)    

    def get_assignment(self, variable, exact=True):
        for thing in self:
            if isinstance(thing, ASSIGNMENT):
                if exact:
                    if thing.variable == variable:
                        return thing.value
                else:
                    if string_compare(thing.variable,variable):
                        return thing.value
        return None

    def get_all_assignments(self,variable=None,exact=True):
        ret = []        
        for thing in self:
            if isinstance(thing, ASSIGNMENT):
                if variable:
                    if exact:
                        if thing.variable == variable:
                            ret.append(thing) 
                    else:
                        if string_compare(thing.variable,variable):
                            ret.append(thing) 
                else:
                    ret.append(thing)
        for thing in self:
            if isinstance(thing, BLOCK):
                ret=ret+thing.get_all_assignments(variable,exact)            
        return ret

    def get_block(self, name):
        for thing in self:
            if isinstance(thing, BLOCK):
                if thing.__name__ == name:
                    return thing
        return None
    
    def get_all_blocks(self,name):
        ret = []
        for thing in self:
            if isinstance(thing, BLOCK):
                if thing.__name__ == name:
                    ret.append(thing)
        for thing in self:
            if isinstance(thing, BLOCK):
                if thing.__name__ != name:
                    ret=ret+thing.get_all_blocks(name)
        return ret

#     def append(self, thing):
#         contents = [x for x in self]
#         contents.append(thing)
#         self.update(contents)
#
#     def count(self, thing):
#         return self.__contents__.count(thing)

    def is_empty(self):
        return len(self) == 0

    def __update_children__(self):
        for athing in self:
            if hasattr(athing, '__parent__'):
                athing.__parent__ = self
            else:
                athing = athing._replace(parent=self)

    # def __dir__(self):
        # return [y.__name__
                # for y in self
                # if hasattr(y, '__name__')]

    def __getattr__(self, name):
        # print dir(self)
        if name in ['__add__', '__class__', '__contains__', '__delattr__',
                    '__delitem__', '__delslice__', '__doc__', '__eq__',
                    '__format__', '__ge__', '__getattribute__', '__getitem__',
                    '__getslice__', '__gt__', '__hash__', '__iadd__', '__imul__',
                    '__init__', '__iter__', '__le__', '__len__', '__lt__', '__mul__',
                    '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__',
                    '__reversed__', '__rmul__', '__setattr__', '__setitem__',
                    '__setslice__', '__sizeof__', '__str__', '__subclasshook__',
                    'append', 'count', 'extend', 'index', 'insert', 'pop',
                    'remove', 'reverse', 'sort']:
            return self.__dict__[name]
        for athing in self:  # .__dict__['__iter__']():
            if hasattr(athing, '__name__'):
                if athing.__name__ == name:
                    return athing
        raise AttributeError

#     def __setattr__(self, name, value):
#         if name in self.__dict_:
#             self.__dict__[name] = value
#             return
#         for athing in self:
#             if isinstance(athing, BLOCK):
# write a modify...
# consider namedtuples for assignments...
#                 if athing.__name__ == name:
#                     self[self.index(athing)] = value
#                     return
#             elif isinstance(athing, ASSIGNMENT):
#                 if athing.variable == name:
#                     self[self.index(athing)] = ASSIGNMENT(name, value)
#                     return
#         raise AttributeError

    def pretty(self,strip_custom_tooltip=False):
        string = str(self)
        if strip_custom_tooltip:
            string=strip_custom_tooltip_from_string(self,string)
        return pretty(string)
    
    def pretty_contents(self,strip_custom_tooltip=False):
        ret = str(self)
        ret=ret.partition('{')[2]
        ret=ret.rpartition('}')[0]        
        if strip_custom_tooltip:
            ret=strip_custom_tooltip_from_string(self,ret)
        return pretty(ret)

    def __str__(self):
        ret = ''
        # if self.__name__!='':
        pre = self.__name__ + ' = {'
        post = '}'
        if len(self) != 0:
            spc = '\n'
            if len(self) == 1:
                test = str(self[0])
                if '\n' not in test and len(test) < 81:
                    spc = ' '
            contents = [str(x) for x in self]
            contents.reverse()
            ret = pre + spc + spc.join(contents) + spc + post + spc
        else:
            if defines.BLOCK_GENERATES_EMPTY_ITEMS:
                ret = pre + '\n' + post + '\n'
        if self.__comment__:
            if ret[-1:] == '\n':
                ret = ret[0:-1]
            ret = ret + str(self.__comment__)
        return ret

    def __repr__(self):
        return 'BLOCK("{}",{})'.format(self.__name__, [x for x in self])

    def __ne__(self, other):
        return not(self == other)

    def __eq__(self, other):
        if not isinstance(other, BLOCK):
            return False
        if self.__name__ != other.__name__:
            return False
        for athing in self:
            if athing not in other:
                return False
        return True

    def __contains__(self, other):
        # for thing in self:
            # if thing == other:
         #       return True
        # return False
        # not sure why this was here...
        return self.__contains__(other)
        # maybe ti was supposed to be this?
        # return list.__contains__(self, other)

#     def __iter__(self):
#         return self.__contents__.__iter__()

#     def __len__(self):
#         return len(self.__contents__)

    def __chain__(self):
        ret = [x for x in self.__iter__()]
        chains = []
        for thing in self.__iter__():
            if hasattr(thing, '__chain__'):
                chains.append(thing.__chain__())
            ret.append(thing)
        return itertools.chain(itertools.chain(ret), *chains)