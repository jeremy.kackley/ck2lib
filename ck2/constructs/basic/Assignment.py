from Ck2Object import Ck2Object

class ASSIGNMENT(Ck2Object):
    # __slots__=()
    __slots__ = ('__parent__', 'variable', 'value', '__name__', '__comment__')

    def __init__(self, variable, value, comment=None):
        Ck2Object.__init__(self)
        self.variable = variable
        self.value = str(value)
        self.__name__ = variable
        self.__comment__ = None
        if comment:
            # self.__comment__ = Comment(comment)
            self.__comment__ = comment

    def __str__(self):
        ret = str(self.variable) + ' = ' + str(self.value) + ' '
        if self.__comment__:
            comstr = str(self.__comment__)
            if comstr[0] != '#':
                comstr = '#' + comstr
            ret = ret + comstr
        if self.__parent__ is None:
            ret = ret + '\n'
        return ret

    def __repr__(self):
        quote = ''
        if '"' in self.value:
            quote = "'"
        return 'ASSIGNMENT("{}",{}{}{})'.format(self.variable,
                                                quote,
                                                self.value,
                                                quote)

    def __eq__(self, other):
        if isinstance(other, ASSIGNMENT):
            if self.variable == other.variable and other.value == self.value:
                return True
       # print 'bad assignment',self,other
        return False

    def __len__(self):
        return len(str(self))