from collections import namedtuple

class Comment(namedtuple('CommentBase', ['text', 'parent'])):
    __slots__ = ()

    def __str__(self):
        return '# ' + str(self.text) + '\n'

    def __eq__(self, other):
        if isinstance(other, Comment):
            if self.text == other.text:
                return True
