import basic
import Building
import JobAction
import Technology
import EventModifier
import MinorTitle
import Trait

ASSIGNMENT = basic.ASSIGNMENT
BLOCK = basic.BLOCK
Literal = basic.Literal
Comment = basic.Comment

Building=Building.Building
Technology=Technology.Technology
JobAction=JobAction.JobAction
EventModifier=EventModifier.EventModifier
MinorTitle=MinorTitle.MinorTitle
Trait=Trait.Trait