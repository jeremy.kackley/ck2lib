# expanded from code snipped found at
# http://stackoverflow.com/questions/19472530/representing-graphs-data-structure-in-python
from collections import defaultdict

# possible enhancements:
#   add support for edge labels.

from jinja2 import Environment, FileSystemLoader, Template
from os.path import abspath, dirname, join
import defines

env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')),
    lstrip_blocks=True,
    trim_blocks=True)


class Graph(object):
    """ Graph data structure, undirected by default. """

    def __init__(self, connections=None, directed=False):
        self._graph = defaultdict(set)
        self._directed = directed
        self._node_id = {}  # used for some output formats
        self._total_nodes = 0
        self._reclaimed_ids = []
        self._labels={}
        if connections:
            self.add_connections(connections)
    
    def __len__(self):
        return self._total_nodes

    def add_connections(self, connections):
        """ Add connections (list of tuple pairs) to graph """

        for node1, node2 in connections:
            self.add(node1, node2)
    
    def add_label(self,node,label):
        self._labels[node]=label

    def get_label(self,node):
        if self._labels.has_key(node):        
            return unicode(self._labels[node],'utf8','ignore')
        return node

    def get_id(self, node):
        return self._node_id[node]

    def _check_id(self, node1):
        if node1 not in self._node_id:
            newid = None
            if len(self._reclaimed_ids) > 0:
                newid = self._reclaimed_ids.pop()
            else:
                newid = self._total_nodes
                self._total_nodes += 1
            self._node_id[node1] = newid        

    def get_nodes(self):
        return self._graph.keys()

    def get_neighbors(self, node):
        return self._graph[node]    

    def add(self, node1, node2):
        """ Add connection between node1 and node2 """

        node1=unicode(node1,'utf8','ignore')
        node2=unicode(node2,'utf8','ignore')
        self._check_id(node1)
        self._check_id(node2)
        self._graph[node1].add(node2)
        if not self._directed:
            self._graph[node2].add(node1)

    def remove(self, node):
        """ Remove all references to node """

        self._reclaimed_ids.append(self._node_id[node])
        self._node_id.pop(node)  # raises keyerror if not found.
        for n, cxns in self._graph.iteritems():
            try:
                cxns.remove(node)
            except KeyError:
                pass
        try:
            del self._graph[node]
        except KeyError:
            pass

    def is_connected(self, node1, node2):
        """ Is node1 directly connected to node2 """

        return node1 in self._graph and node2 in self._graph[node1]

    def find_path(self, node1, node2, path=[]):
        """ Find any path between node1 and node2 (may not be shortest) """

        path = path + [node1]
        if node1 == node2:
            return path
        if node1 not in self._graph:
            return None
        for node in self._graph[node1]:
            if node not in path:
                new_path = self.find_path(node, node2, path)
                if new_path:
                    return new_path
        return None

    def to_dot(self):
        template = env.get_template('graph_dot.j2')
        return template.render(graph=self)

    def to_tgf(self):
        template = env.get_template('graph_tgf.j2')
        return template.render(graph=self)

    def to_gml(self):
        # https://en.wikipedia.org/wiki/Graph_Modelling_Language
        max_name_length = max((len(x) for x in self._node_id)) * 6
        template = env.get_template('graph_gml.j2')
        return template.render(graph=self, max_name_length=max_name_length)

    def to_graphml(self):
        # yml specific format
        raise NotImplementedError

    def __str__(self):
        return '{}({})'.format(self.__class__.__name__, dict(self._graph))
