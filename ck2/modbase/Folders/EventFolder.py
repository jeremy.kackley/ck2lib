from . import ModFolder
from .. import Containers


class EventFolder(ModFolder.ModFolder):
    def __init__(self,
                 path='events\\',
                 container=Containers.EventContainer,
                 default_file='_events.txt'):
        ModFolder.ModFolder.__init__(self, path, container, default_file)

    def add(self, data, filename=None, namespace=None):
        if not namespace:
            namespace = data.namespace()
        if not filename:
            filename = namespace+self._default_file
        ModFolder.ModFolder.add(self, data, filename)
        self.files[filename].namespaces.append(namespace)