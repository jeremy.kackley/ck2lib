from .. import Containers
import os
from collections import OrderedDict

class ModFolder:
    def __init__(self,
                 path,
                 container=None,
                 default_file='.txt'):
        self.path = path
        self.files = OrderedDict()
        self._container = container or Containers.ContainerBase
        self._default_file = default_file
        self._preserialzied_files = {}
        self._written = 0

    def things_written(self):
        return self._written

    def add_preserialized(self, string, filename):
        '''
        This allows support for 'canned' files written
        by hand and otherwise not generated.
        :param string:
        :param filename:
        '''
        self._preserialzied_files[filename] = string

    def init_file(self, filename):
        self.files[filename] = self._container()

    def add(self, data, filename=None):
        if not filename:
            filename = self._default_file
        if filename not in self.files:
            self.init_file(filename)
        self.files[filename].append(data)

    def _write_data(self, folder, fn, data):
        if not os.path.exists(folder):
            os.makedirs(folder)
        with open(folder + fn, 'w') as _fp:
            try:
                _fp.write(data)
            except:
                _fp.write(data.encode('utf-8'))

    def to_string(self):
        ret = ''
        for value in self.files.values():
            ret += value.serialize()
        return ret

    def write_zip(self, azip, short, filename_mask):
        self._written=0
        for afile in self.files:
            if self.files[afile]:
                # print afile
                fn = filename_mask.format(short, afile)
                azip.writestr(self.path + fn, self.files[afile].serialize())
                self._written+=1
        for afile in self._preserialzied_files:
            fn = filename_mask.format(short, afile)
            azip.writestr(self.path + fn, self._preserialzied_files[afile])
            self._written+=1

    def write(self, folder, short, fn_mask):
        self._written=0
        dest = os.path.join(folder, self.path)
        for afile in self.files:
            if self.files[afile]:
                fn = fn_mask.format(short, afile)
                self._write_data(dest, fn, self.files[afile].serialize())
                self._written+=1
        for afile in self._preserialzied_files:
            fn = fn_mask.format(short, afile)
            self._write_data(dest, fn, self._preserialzied_files[afile])
            self._written+=1

    def clear(self):
        self.files = OrderedDict()