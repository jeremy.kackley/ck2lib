from . import ModFolder
from .. import Containers


class LocalizationFolder(ModFolder.ModFolder):
    def __init__(self,
                 path='localisation\\',
                 container=Containers.LocalizationContainer,
                 default_file='locs.csv'):
        ModFolder.ModFolder.__init__(self, path, container, default_file)

    def add(self, data, filename=None):
        if not filename:
            if data.filename:
                filename = data.filename
        ModFolder.ModFolder.add(self, data, filename)