from . import ModFolder
from .. import Containers


class BuildingFolder(ModFolder.ModFolder):
    def __init__(self,
                 path='common\\buildings\\',
                 container=Containers.BuildingContainer,
                 default_file='buildings.txt'):
        ModFolder.ModFolder.__init__(self, path, container, default_file)