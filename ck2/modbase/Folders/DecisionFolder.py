from . import ModFolder
from .. import Containers

class DecisionFolder(ModFolder.ModFolder):
    def __init__(self,
                 path='decisions\\',
                 container=Containers.DecisionContainer,
                 default_file='decisions.txt'):
        ModFolder.ModFolder.__init__(self, path, container, default_file)

    def add(self, data, filename=None, atype=None):
        if not filename:
            filename = self._default_file
        if filename not in self.files:
            self.init_file(filename)
        if not atype and hasattr(data,'type'):
            atype = data.type
        if atype:            
            self.files[filename].types.append(atype)
            ModFolder.ModFolder.add(self, data, filename)
        else:
            self.add_preserialized(data,filename)