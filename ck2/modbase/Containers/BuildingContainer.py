from ...utilities import pretty
from ...code.base import make_b


class BuildingContainer(list):

    def serialize(self):
        cur_type = ''
        ret = ''
        temp = ''
        for thing in self:
            if cur_type != thing.type:
                if temp:
                    ret += make_b(cur_type, temp)
                    temp = ''
                cur_type = thing.type
            temp += str(thing) + '\n'
        if temp:
            ret += make_b(cur_type, temp)
        return pretty(ret)