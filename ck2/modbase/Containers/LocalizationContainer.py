from ...utilities import pretty

class LocalizationContainer(list):

    def serialize(self):
        # ret = ''
        # for x in self:
            # print x.name
            # ret +=str(x)#.decode('utf-8')
        # return ret
        # return ''.join((str(x).decode('utf-8')
                        # for x in self))
        ret = u''.join([unicode(x)
                        for x in self])
        return ret