from ...utilities import pretty

class ContainerBase(list):

    def serialize(self):
        return pretty('\n'.join([str(x) for x in self]))
