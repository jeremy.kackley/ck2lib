from ...utilities import pretty
from ...code.ScriptedBaseContainer import ScriptedBaseContainer


class ScriptedObjectContainer(list):
    def __init__(self):
        list.__init__(self)
        self._sbc = ScriptedBaseContainer()
    
    def append(self,item):
        try:
            self._sbc.add_object(item)
        except:
            list.append(self,item)

    def __len__(self):        
        return max([list.__len__(self),len(self._sbc)])

    def serialize(self):  
        ret=''
        if len(self._sbc)>0:
            ret = str(self._sbc)+'\n'
        if len(self)>0:
            ret += pretty('\n'.join([str(x) for x in self]))
        return ret
