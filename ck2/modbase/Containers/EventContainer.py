from ...utilities import pretty
from ...code.base import assign

class EventContainer(list):

    def __init__(self):
        list.__init__(self)
        self.namespaces = []

    def serialize(self):
        cur_ns = ''
        ret = ''
        for thing, namespace in zip(self, self.namespaces):
            if cur_ns != namespace:
                cur_ns = namespace
                if namespace != '':
                    # namespace might be intentionally blank.
                    ret += assign('namespace', namespace)
            ret += str(thing) + '\n'
        return pretty(ret)