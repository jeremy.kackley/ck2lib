from ...utilities import pretty
from ...code.base import make_b


class DecisionContainer(list):

    def __init__(self):
        list.__init__(self)
        self.types = []

    def _do_serialize(self, ret, atype, acont):
        if len(acont) > 0:
            string = '\n'.join([str(x) for x in acont])
            if atype == '':
                # it might be a constant string
                ret += string
            else:
                ret += make_b(atype, string)
        return ret

    def serialize(self):
        cur_type = ''
        cur_cont = []
        ret = ''
        for thing, atype in zip(self, self.types):
            if atype != cur_type:
                ret = self._do_serialize(ret, cur_type, cur_cont)
                cur_type = atype
                cur_cont = []
            cur_cont.append(thing)
        ret = self._do_serialize(ret, cur_type, cur_cont)
        return pretty(ret)