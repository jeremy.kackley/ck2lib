
class Localization:

    def __init__(self, name, english='', french='', german='', spanish=''):
        self.name = name
        self.english = english
        self.french = french
        self.german = german
        self.spanish = spanish
        self.langs = ('en', 'fr', 'de', 'es')  # possible languages
        # checked by LocalizationFolder if filename is not set.
        self.filename = None

    def walk(self, mask):
        ''' Walks a localization mask, defined like:
            'EXAMPLE':{
                'en':'asdf',
                'fr':'asdf',
                'de':'asdf',
                'es':'asdf',
                },
        '''
        masks = []
        langs = []
        for lang in self.langs:
            masks.append(mask[lang])
            langs.append(lang)
        return zip(masks, langs)

    def add(self, string, language):
        if language.lower() in ['en', 'english']:
            self.english = string
        elif language.lower() in ['fr', 'french']:
            self.french = string
        elif language.lower() in ['de', 'german']:
            self.german = string
        elif language.lower() in ['es', 'spanish']:
            self.spanish = string
        else:
            self.english = string
        # raise exception?  Default to english?

    def get_english(self):
        # return self.english#.decode('utf-8')
        return self._sanitize(self.english)

    def get_french(self):
        if not self.french:
            return self.get_english()
        # return self.french#.decode('utf-8')
        return self._sanitize(self.french)

    def get_german(self):
        if not self.german:
            return self.get_english()
        # return self.german#.decode('utf-8')
        return self._sanitize(self.german)

    def _sanitize(self, string):
        # print 'working on ',repr(string)
        try:
            return string.decode('utf-8', 'ignore')
        except:
            return string
        # return string.decode('unicode_escape').encode('ascii','ignore')

    def get_spanish(self):
        if not self.spanish:
            return self.get_english()
        return self._sanitize(self.spanish)
        # return self.spanish#.decode('utf-8')

    def __unicode__(self):
        linemask = u'{};{};{};{};;{};;;;;;;;;x\n'
        try:
            return linemask.format(self.name,
                                   self.get_english(),
                                   self.get_french(),
                                   self.get_german(),
                                   self.get_spanish())
        except:
            print repr(self.get_english())
            print repr(self.get_spanish())
            print repr(self.get_french())
            print repr(self.get_german())

    def __str__(self):
        # # ret = self.name+';'
        # # ret += self.get_english()+';'
        # # ret += self.get_french()+';'
        # # ret += self.get_german()+';;'
        # # ret += self.get_spanish()+';;;;;;;;;x\n'
        # # print 'in __str__, str built, str is...'
        # # print ret
        # # print "Returning from __str__"
        # # return ret
        linemask = '{};{};{};{};;{};;;;;;;;;x\n'
        return linemask.format(self.name,
                               self.get_english(),
                               self.get_french(),
                               self.get_german(),
                               self.get_spanish())

    def __add__(self, other):
        return str(self) + str(other)