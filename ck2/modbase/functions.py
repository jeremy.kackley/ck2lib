
from datetime import datetime
from functools import wraps

def format_time(time):
    if time.total_seconds() < 1:
        if time.microseconds < 10000:
            time = '{} microseconds'.format(time.microseconds)
        else:
            time = '{:.2f} seconds'.format(time.microseconds / 1000000.0)
    elif time.total_seconds() < 60:
        time = '{} seconds'.format(time.total_seconds())
    else:
        time = str(time).partition('.')[0]
    return time

def gen_readme_report(name='things',readme_file='script_output.txt', verb='Generating'):
    """
    Adds the time a method takes to execute to the classes add_readme method.  

    Intended for use with ModBase
    """
    mask_no_num = verb+' {} took {}.\n'
    mask_num = verb+' {} {} took {}.\n'

    def decorating_function(some_function):
        @wraps(some_function)
        def wrapper(*args, **kwargs):        
            start = datetime.now()
            number = some_function(*args, **kwargs)
            time = format_time(datetime.now() - start)
            if number:
                elapsed = mask_num.format(number,name,time)
            else:                
                elapsed = mask_no_num.format(name,time)
            args[0].add_readme(readme_file,elapsed)
            #should be self.
            return number
        return wrapper
    return decorating_function