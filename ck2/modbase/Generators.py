from functions import format_time, gen_readme_report
from datetime import datetime

STANDARD_GENERATORS = [
    'events',
    'decisions',
    'scripted_effects',
    'scripted_triggers',
    'on_actions',
    'event_modifiers',
    'buildings',
    'traits',
    'titles',
    'interface',
]

class Generators:
    def __init__(self,modbase):
        self.modbase=modbase#my modbase, e.g. where readme points
        for container in STANDARD_GENERATORS:
            self.__dict__[container]=[]
    
    @gen_readme_report('localizations')
    def localize(self):
        count=0
        for thing in STANDARD_GENERATORS:
            for container in getattr(self,thing):
                for loca in container.localize():
                    self.modbase.localizations.add(loca)
                    count+=1
        return count

    @gen_readme_report('everything')
    def generate(self):
        mask = 'Generating {} {} took {}.\n'
        for thing in STANDARD_GENERATORS:
            thingcount=0
            start = datetime.now()
            for container in getattr(self,thing):
                for result in container.generate():
                    if hasattr(container,'filename'):
                        getattr(self.modbase,thing).add(result,container.filename+'.txt')
                    else:
                        getattr(self.modbase,thing).add(result)
                    thingcount+=1
            if thingcount>0:
                time = format_time(datetime.now() - start)  
                elapsed=mask.format(thingcount,thing,time)
                self.add_readme('script_output.txt',elapsed)


    def add_readme(self, name, text):
        self.modbase.add_readme(name,text)