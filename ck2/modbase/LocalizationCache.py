
import xml.etree.ElementTree as ET
class LocalizationCache:

    def __init__(self, path=None):
        self.path = path
        self.tree = None
        self.root = None
        self.changes = 0

    def open(self):
        if not os.path.exists(self.path):
            sp = os.path.split(self.path)
            if sp[0] and not os.path.exists(sp[0]):
                os.makedirs(sp[0])
        if os.path.isfile(self.path):
            self.tree = ET.parse(self.path)
            self.root = self.tree.getroot()
        else:
            self.root = ET.Element('variables')
            self.tree = ET.ElementTree(self.root)

    def get(self, variable, language):
        existing = self.root.find(variable)
        if existing is not None:
            language_key = existing.find(language)
            if language_key is not None:
                return language_key.text
        return None

    def set(self, variable, language, string):
        existing = self.root.find(variable)
        if existing is None:
            existing = ET.SubElement(self.root, variable)
        language_key = existing.find(language)
        if language_key is None:
            language_key = ET.SubElement(existing, language)
        language_key.text = string
        self.changes += 1
        if self.changes > 1000:
            self.changes = 0
            self.write()

    def write(self):
        self.tree.write(self.path, encoding="utf-8")
