

class CK2ComponentSet:

    def __init__(self, root):
        self.myset = set()
        self.root = root

    def add(self, key):
        self.myset.add(key)

    def __iter__(self):
        for thing in self.myset:
            return self.root.components[thing]

    def __check_thing__(self, thing, name, value, match_pairs):
        if name and 'name' in thing._fields:
            if name != thing.name:
                return thing
        if value and 'value' in thing._fields:
            if value == thing.value:
                return thing
        if match_pairs and isinstance(thing, NewBlock):
            for key, value in match_pairs:
                for child in thing.children():
                    pass
                # need a scheme for children first.
        return None

    def find(self, name=None, value=None, match_pairs=None):
        for thing in self:
            test = self.__check_thing__(thing, name, value, match_pairs)
            if test:
                return test
        return None

    def findall(self, name=None, value=None, match_pairs=None):
        ret = []
        for thing in self:
            test = self.__check_thing__(thing, name, value, match_pairs)
            if test:
                ret.append(thing)
        return ret