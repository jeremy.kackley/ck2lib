
class CK2Syntax:

    def __init__(self):
        self.components = {}
        self.children = {}
        self.parents = {}
        self.component_comments = {}
        # instead of sets, these can be a custom class
        #    for things like __iter__
        #        and searching.
        self.blocks = set()
        self.variables = set()
        self.comments = set()
        self.literals = set()
        self._id = 0

    def __increment_component_id__(self):
        new_comp_id = self._id
        self._id += 1
        return new_comp_id

    def add_comment(self, value):
        _id = self.__increment_component_id__()
        self.components[_id] = NewComment(value)
        self.comments.add(_id)
        return _id

    def __handle_comment__(self, comment, _id):
        comment = self.add_comment(comment)
        self.component_comments[_id] = comment

    def add_literal(self, value, comment=None):
        _id = self.__increment_component_id__()
        if comment:
            self.__handle_comment__(comment, _id)
        self.components[_id] = NewLiteral(value)
        self.literals.add(_id)
        return _id

    def add_variable(self, variable, value, comment=None):
        _id = self.__increment_component_id__()
        self.__handle_comment__(comment, _id)
        if comment:
            self.__handle_comment__(comment, _id)
        self.variables.add(_id)
        self.components[_id] = NewVariable(variable, value)
        return _id

    def add_block(self, name, children=None, comment=None):
        _id = self.__increment_component_id__()
        if comment:
            self.__handle_comment__(comment, _id)
        self.components[_id] = NewBlock(name)
        return _id

#     def add(self, component, children=None):
#         self.components[new_comp_id] = component
#         if isinstance(component, NewBlock):
#             self.blocks.add(new_comp_id)
#             for child in component:
#                 self.components[child].parent = new_comp_id
#                 component.repo = self
#
#         if isinstance(component, NewVariable):
#             self.variables.add(new_comp_id)
#
#         if isinstance(component, NewLiteral):
#             self.literals.add(new_comp_id)
#
#         if isinstance(component, NewComment):
#             self.comments.add(new_comp_id)