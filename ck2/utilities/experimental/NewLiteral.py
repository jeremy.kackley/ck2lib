

class NewLiteral(
        namedtuple('LiteralBase', ['id', 'value', 'comment', 'parent'])):
    __slots__ = ()

    def __eq__(self, other):
        if isinstance(other, Literal):
            if self.value == other.text:
                return True
       # print 'bad assignment',self,other
        return False

    def __str__(self):
        tail = '\n'
        if self.comment:
            tail = '#' + self.comment + '\n'
        return self.value + tail
