

    # character scopes
character_blocks = [
    'FROM',
    'ROOT',
    'any_backed_character',
    'any_child',
    'any_courtier',
    'any_current_enemy',
    'any_defender',
    'any_dynasty_member',
    'any_friend',
    'any_independent_ruler',
    'any_liege',
    'any_playable_ruler',
    'any_realm_character',
    'any_realm_lord',
    'any_rival',
    'any_sibling',
    'any_vassal',
    'any_vassal',
    'any_ward',
    'enemy',
    'father',
    'father_even_if_dead',
    'guardian',
    'host',
    'liege',
    'lover',
    'mother',
    'random_vassal',
    'religion_group',
    'religion_head',
    'rightful_religious_head_scope',
    'spouse',
    'spouse_even_if_dead',
    'top_liege',
]
# province
province_blocks = [
    'any_demesne_province',
    'any_neighbor_province',
    'any_province_character',
    'any_province_lord',
    'any_realm_province',
    'capital_scope',
    'county',
    'create_character',
    'create_random_soldier',
    'crusade_target',
    'duchy',
    'empire',
    'kingdom',
    'location',
    'new_character',
    'owner',
    'random_demesne_province'
    'random_province_lord',
    'random_province_character',
    'ruler',
    'tooltip',
]
# titles
title_blocks = [
    'any_demesne_title',
    'primary_title',
    'random_demesne_title',
]
other_blocks = [
    'AND',
    'IF',
    'NOT',
    'OR',
    'ai_chance',
    'had_global_flag',
    'holder_scope',
    'limit',
    'opinion',
    'opinion_levy_raised_days',
    'random_claim',
    'reverse_opinion',
    'siege',
    'trigger',
]


def __debug_print__(alist):
    stripped = sorted([x.strip() for x in alist])
    __text = repr(stripped).replace('[', '[ ')
    __text = __text.replace(']', ' ]')
    return __text.replace(' ', '\n    ')

defined_blocks = character_blocks + \
    province_blocks + title_blocks + other_blocks

# print __debug_print__(character_blocks)
# print __debug_print__(province_blocks)
# print __debug_print__(title_blocks)
# print __debug_print__(other_blocks)


def province(prov_id_or_name, contents):
    return BLOCK(prov_id_or_name, contents)

for __tmp__ in defined_blocks:
    __tmp__ = __tmp__.strip()
    exec('def {}(*args): return BLOCK("{}",args)'.format(__tmp__, __tmp__))