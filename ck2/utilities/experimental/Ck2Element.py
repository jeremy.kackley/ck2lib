#problems so far:
#   how to distinguish between literals and empty blocks?  
#       dummy...literals are just strings.  
#   what about things like if...
class Ck2Element:
    def __init__(*args):
        self = args[0]
        self.__name = args[1]
        self.__contents = []     
        if len(args) > 2:
            args = args[2:]
            for arg in args:
                self.__contents.append(arg)
                if isinstance(arg,Ck2Element):
                    if arg.getname() not in self.__dict__:
                        self.__dict__[arg.getname()] = arg       
    def is_block(self):
        if self.__contents:
            if isinstance(self.__contents[0],Ck2Element):
                return True
        if len(self.__contents)>1:
            return True        
        return False
    def is_assignment(self):
        if self.__contents:
            if self.__name!='#':
                return True
        return False
    def is_comment(self):
        return self.__name=='#'
    def value(self):
        #value only makes sense for assignments
        if len(self.__contents)!=1:          
            return None
        return self.__contents[0]
    def set_value(self,value):        
        if len(self.__contents)>1:          
            return False
        if len(self.__contents)<1:
            self.__contents.append(value)
        else:
            self.__contents[0]=value
        return True
    def __str__(self):
        if self.is_block():
            return self.__name + '={'+'{}'.format(' '.join([str(x) for x in self.__contents if x]))+'}\n'
        if self.is_comment():
            return '#{}\n'.format(self.__contents[0])
        if self.is_assignment():
            return '{}={}'.format(self.__name,self.__contents[0])
        return '' # no need to draw 'empty' ck2 elements.
    def __repr__(self):
        contents = ','.join([repr(x) for x in self.__contents if x])
        if len(contents) > 0:
            contents = ',' + contents
        #repring is fine, since you could 'add' components.
        return 'ck2tag("{}"{})'.format(self.__name,contents)

    def getname(self):
        return self.__name

    def __setitem__(self, key, item): 
        self.__dict__[key] = item
        self.__contents.append(item)

    def __getitem__(self, key): 
        return self.__dict__[key]

    def append(self,item):
        self.__contents.append(item)        
        if isinstance(arg,Ck2Element):
            if arg.getname() not in self.__dict__:
                self.__dict__[arg.getname()] = arg      


def province_event():
    a=['hide_window','is_triggered_only',
                    'notification',
                    'major',
                    'is_friendly',
                    'is_hostile',
                    'hide_from',
                    'hide_new',
                    'show_root',
                    'show_from_from',
                    'show_from_from_from',
                    'only_playable',
                    'is_part_of_plot',
                    'only_rulers',
                    'religion',
                    'religion_group',
                    'min_age',
                    'max_age',
                    'only_independent',
                    'only_men',
                    'only_capable',
                    'capable_only',
                    'lacks_dlc',
                    'has_dlc',
                    'friends',
                    'rivals',
                    'prisoner',
                    'ai',
                    'is_patrician',
                    'is_female',
                    'is_married',
                    'is_sick',
                    'has_character_flag',
                    'has_global_flag',
                    'war',
                    'culture',
                    'culture_group',
                    ]
    b=['weight_multiplier',
                 'mean_time_to_happen',
                'trigger',
                'immediate',
                ]

def any_realm_lord(string):
    return Ck2Element('any_realm_lord',string)

if __name__ == '__main__':
    test=Ck2Element('province_event',Ck2Element('id',5),Ck2Element('days',100),Ck2Element('#','things are rough'),Ck2Element('people',"daniel","ares"))
    print test
    print repr(test)
    print str(repr(test))
    print test.id
    print test.days
    print test.people

    #also need to put some thought into how you are going to use this.  This is execllent for returning a parse tree...but
    #   less so for building a new thing, e.g.
    #       event = province_event(id(5),days(100))
    #   would be better.
    #       event = Ck2('province_event',Ck2('id',5),Ck2('days',100))) 
    #   is a bit clunky.  
    #
    #   tl/dr there might still be a place for my classes that know how to properly generate a string.  
    test2=Ck2Element('if',Ck2Element('limit',Ck2Element('has_character_flag','bob_flag')),Ck2Element('party_time','yes'))
    print test2.limit.has_character_flag

    #it might make a reasonable alternative to dealing with strings as a base type.
    test3 = Ck2Element('any_realm_lord',test2)
    print test3
    #what does addition do?  create a list?
    #how do I add things into one?  insert?
    #   e.g. test3['if']['limit']
    #       test3.find('limit')
    #       test3.append(Ck2Element('#','added a comment'))
    #       test3['if']['limit']=5
    #       look at how python xml library work, it's probably worth mirroring them.
    #       also, what about templating?
    print test3['if'].limit.has_character_flag.value()
    print test3['if'].limit.has_character_flag.set_value('asdf')
    print test3['if'].limit.has_character_flag.value()
