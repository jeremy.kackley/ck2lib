
# should probably make this the standard; then I could
#    implement a parent() method
class NewBlock(namedtuple(
        'BlockBase', ['id', 'name', 'comment', 'start', 'end', 'parent', 'root'])):
    __slots__ = ()

    def __str__(self):
        # if self.__name__!='':
        pre = self.name + ' = {'
        post = '}'
        # i'm thinking this is 3 iters; and I only relly need
        #    to do it once...
        if self.comment:
            post = post + '#' + self.comment
        spc = '\n'
        body = "\v".join([str(x) for x in self])

        if '\n' not in body and len(body) < 81 - len(pre + post):
            spc = ' '
        body = body.replace('\v', spc)

        if not body and not GENERATE_EMPTY_ITEMS:
            return ''
        return pre + spc + body + spc + post + spc
    # consider a method to get children by name; at least vars/blocks
    #    get implies has, since you can try to get it and check the return value
    # consider a recursive write method.

    def get(self, name=None, value=None, comment=None, limit=1):

        def do_return(ret, limit):
            if isinstance(ret, type([])):
                if limit:
                    ret = ret[:limit]
                if limit == 1:
                    if len(ret) > 1:
                        return ret[0]
            return ret

        def over_limit(ret, limit):
            if limit:
                if isinstance(ret, type([])):
                    if len(ret) >= limit:
                        return True
            return False

        def aggregate(ret, partial, limit):
            if partial:
                if limit and limit == 1:
                    return partial
                return ret + partial
            return ret

        ret = []
        if name or value or comment:
            partial = self.get_var(name, value, comment, limit)
            ret = aggregate(ret, partial, limit)
            if limit:
                if over_limit(ret, limit):
                    return do_return(ret, limit)
                if limit != 1:
                    limit -= len(ret)
        if name or comment:
            partial = self.get_block(name, comment, limit)
            ret = aggregate(ret, partial, limit)
            if limit:
                if over_limit(ret, limit):
                    return do_return(ret, limit)
                if limit != 1:
                    limit -= len(ret)
        if value or comment:
            partial = self.get_literal(value, comment, limit)
            ret = aggregate(ret, partial, limit)
            if limit:
                if over_limit(ret, limit):
                    return do_return(ret, limit)
                if limit != 1:
                    limit -= len(ret)
        if comment:
            partial = self.get_comment(comment, limit)
            ret = aggregate(ret, partial, limit)
            if limit:
                if over_limit(ret, limit):
                    return do_return(ret, limit)
                if limit != 1:
                    limit -= len(ret)
        return do_return(ret, limit)

    def get_var(self, name=None, value=None, comment=None, limit=1):
        return self.root.root.variables.find(
            name, value, comment, self.id, limit)

    def get_block(self, name=None, comment=None, limit=1):
        return self.root.root.blocks.find(name, comment, self.id, limit)

    def get_comment(self, value=None, limit=1):
        return self.root.root.comments.find(value, self.id, limit)

    def get_literal(self, value=None, comment=None, limit=1):
        return self.root.root.literals.find(value, comment, self.id, limit)

    def __iter__(self):
        return self.children().__iter__()

    def delete(self):
        # for x in self:
            # x.delete()
        self.root.execute('DELETE FROM blocks WHERE id=?',
                          (self.id,))
        self.root.execute('DELETE FROM component_types WHERE c_id=?',
                          (self.id,))

    def children(self):
        return self.root.root.get_children(self.id)

    def blocks(self):
        return self.root.root.blocks.get_children(self.id)

    def variables(self):
        return self.root.root.variables.get_children(self.id)

    def comments(self):
        return self.root.root.comments.get_children(self.id)

    def literals(self):
        return self.root.root.literals.get_children(self.id)

    # this doesn't work for this; I still end up with a dict of lists...
#     def get_child(self,child_id):
#         if
#         raise AttributeError

# def __eq__(self, other):
# if isinstance(other, NewBlock):
# if self.name == other.name:
# for thing in
# print 'bad assignment',self,other
# return False

    # def __len__(self):
        # return len(str(self))