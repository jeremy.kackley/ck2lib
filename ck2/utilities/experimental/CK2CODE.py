


# maybe make these functions
#   get_blocks(savefile.dynasties)
#   perhaps I should change the way I store things.
#    if i drop the silliness with setattr, i can probably force this into block


class CK2CODE(BLOCK):

    def __init__(self, alist=None):
        if alist:
            if isinstance(alist, type('')):
                alist = parse(alist)
        self.__contents__ = alist or []
        # self.__slots__ = ('__contents__')

    # def append(self,thing):

    def __iter__(self):
        return self.__contents__.__iter__()

    def __len__(self):
        return len(self.__contents__)

    def __repr__(self):
        return 'CK2CODE([{}])'.format(self.__contents__)

    def __str__(self):
        return pretty(self.__contents__)

    # need to rethink this; whatever it does, it doesn't really work.

    def __chain__(self):
        ret = [self.__contents__]
        chains = []
        for thing in self:
            if hasattr(thing, '__chain__'):
                chains.append(thing.__chain__())
            ret.append(thing)
        return itertools.chain(itertools.chain(ret), *chains)

    def grab_first_match(self, check):
        for athing in self.__chain__():
            if check(athing):
                return athing
        return None

    # might be able ot make this faster by using
    #   itertools.chain
    def check_all(self, check):
        return (x for x in self.__chain__() if check(x))
        ret = []
        for athing in self.__chain__():
            if check(athing):
                ret.append(athing)
        return ret

    def __variable_checker__(self, var, varname, value):
        if not isinstance(var, ASSIGNMENT):
            return False
        if not varname or var.variable.lower() == varname.lower():
            if not value:
                return True
            if var.value == value:
                return True
        return False

    def find_v(self, name=None, value=None):
        return self.grab_first_match(
            lambda x: self.__variable_checker__(x, name, value))

    def findall_v(self, name=None, value=None):
        return self.check_all(
            lambda x: self.__variable_checker__(x, name, value))

    # consider using re.match instead of ==
    def __block_checker__(self, block, name=None, match_pairs=None):
        if not isinstance(block, BLOCK):
            return False
        if not name or block.__name__.lower() == name.lower():
            if not match_pairs:
                return True
            for var, value in match_pairs:
                if hasattr(block, var):
                    if not value:
                        return True
                    for thing in block:
                        if self.__variable_checker__(thing, var, value):
                            return True
                        # failed a check
                    return False
        return False

    def find_b(self, name=None, match_pairs=None):
        return self.grab_first_match(
            lambda x: self.__block_checker__(x, name, match_pairs))

    def findall_b(self, name=None, match_pairs=None):
        return self.check_all(
            lambda x: self.__block_checker__(x, name, match_pairs))

    def getall_b(self):
        return self.check_all(lambda x: isinstance(x, BLOCK))

    def getall_v(self):
        return self.check_all(lambda x: isinstance(x, ASSIGNMENT))

    def getall_c(self):
        return self.check_all(lambda x: isinstance(x, Comment))

    def getall_l(self):
        return self.check_all(lambda x: isinstance(x, Literal))

    def getall(self):
        return self.check_all(lambda x: True)