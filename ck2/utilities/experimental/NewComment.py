

class NewComment(namedtuple('CommentBase', ['id', 'value', 'parent'])):
    __slots__ = ()

    def __str__(self):
        return '#' + self.value + '\n'

    def __eq__(self, other):
        if isinstance(other, NewComment):
            if self.value == other.text:
                return True