

class NewVariable(
        namedtuple('VarBase', ['id', 'name', 'value', 'comment', 'parent'])):
    __slots__ = ()

    def __str__(self):
        ret = '{} = {} '.format(self.name,
                                self.value)
        if self.comment:
            ret = ret + '#' + self.comment + '\n'

        if not self.parent:
            ret = ret + '\n'
        # if self.__parent__:
        # if self.__parent__ == None:
        # ret = ret + '\n'
        return ret

    def __eq__(self, other):
        if isinstance(other, NewVariable):
            if self.name == other.name and other.value == self.value:
                return True
       # print 'bad assignment',self,other
        return False

    def __len__(self):
        return len(str(self))