


    

def __check_potential_syntax__(defined_blocks, defined_assignments):
    with open('potential_syntax.txt') as _af:
        __text = _af.read()
        assignments = re.findall('([\w_]+)[ ]*=[ ]*\w+', __text)
        assignments = list(set(assignments))
        blocks = re.findall('([\w_]+)[ ]*=[ ]*{', __text)
        blocks = list(set(blocks))
        for label, aset in ('assignments', assignments), ('blocks', blocks):
            missing = []
            for athing in aset:
                athing = athing.strip()
                for variation in athing, athing.upper(), athing.lower():
                    if variation not in defined_blocks:
                        if variation not in defined_assignments:
                            missing.append(athing)

            missing = sorted(set(missing))
            print label, '=', __debug_print__(missing)