from collections import deque

#pretty sure this was something I cooked up in a fugue.
#   seems like it'd be less work to just
#   use update...though maybe there
#   was some reason I wanted the first occurance
#   of a thing, and not the latter?
class ComboDict:

    def __init__(self, *args):
        self._dicts = deque()
        for arg in args:
            if arg:
                self.add(arg)
        t = {}

    def __contains__(self, key):
        for adict in self._dicts:
            if adict.__contains__(key):
                return True
        return False
    # i have no idea how to efficeintly implement
    # iter at this point.

    def __len__(self):
        ret = []
        for adict in self._dicts:
            ret.append(len(adict))
        return max(ret)

    def __getitem__(self, key):
        for adict in self._dicts:
            if adict.has_key(key):
                return adict[key]
        # lazy way to raise the default error.
        return adict[key]

    def add(self, adict):
        self._dicts.appendleft(adict)

    def has_key(self, key):
        for adict in self._dicts:
            if adict.has_key(key):
                return True
        return False