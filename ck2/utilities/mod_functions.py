from ..defines import MODSFOLDER
from parse_functions import parse, __fix_dir__
from ModInfo import ModInfo
import os

def find_mods_in_dir(tdir=MODSFOLDER):
    tdir = __fix_dir__(tdir)
    ret = []
    for __a, __b, files in os.walk(tdir):
        for afile in files:
            if '.mod' in afile:
                with open(tdir + afile) as _fp:
                    __text = _fp.read()
                    name_line = __text.partition('name')[2].partition('\n')[0]
                    raw_name = name_line.partition('#')[0].partition('=')[2]
                    mod_name = raw_name.replace('"', '').strip()
                    if 'archive' not in __text:
                        path_line = __text.partition('path')[2].partition('\n')[0]
                        path = path_line.partition('#')[0].partition('=')[2]
                        mod_path = path.partition('/')[2].replace('"', '').strip()
                        mod_path=os.path.join(tdir, mod_path)
                    else:
                        mod_path=tdir
                    ret.append([mod_name, mod_path + '\\', __text])
        break

    return ret


def parse_mods_in_dir(tdir=MODSFOLDER):
    mods = []
    for mod, path, raw in find_mods_in_dir(tdir):
        mods.append(ModInfo(mod, path, raw))
    return mods