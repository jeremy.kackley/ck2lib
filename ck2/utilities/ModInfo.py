from CK2Info import CK2Info
from ComboDict import ComboDict
from collections import OrderedDict
import tempfile, zipfile, os
import shutil
import re, sys

def merge_replace(list1,list2):
    merged=OrderedDict()
    for thing in list1:
        merged[thing.name()] = thing
    for thing in list2:
        merged[thing.name()] = thing
    return merged.values()

class ModInfo(CK2Info):
    def __init__(self, name, path, raw):
        CK2Info.__init__(self, name, path)
        self.raw = raw
        self._updated = False
        self._unzipped=False

    def __enter__(self):
        if self.is_zip():
            self.unzip()
        return self

    def __exit__(self, type, value, traceback):
        self.cleanup_temp_files()
        
    def __del__(self):
        self.cleanup_temp_files()
    
    def cleanup_temp_files(self):        
        if self._unzipped:
            shutil.rmtree(self.path)
            self._unzipped=False

    def unzip(self):
        if not self._unzipped:
            #create temp directory
            temp_path=tdir = tempfile.mkdtemp()
            #unzip contents to temp directory
            zfn=os.path.join(self.path,self.get_archive_name())
            zip=zipfile.ZipFile(zfn,'r')
            zip.extractall(temp_path)
            #update path1
            self.real_path=self.path
            self.path=temp_path
            self._unzipped=True
    
    def get_archive_name(self):
        if not self.is_zip():
            return None
        ret=self.raw.partition('archive')[2].partition('\n')[0]
        for thing in ['"',"'"]:
            if thing in ret:
                ret=ret.partition(thing)[2].partition(thing)[0]    
        ret=ret.partition('/')[2]
        return ret
    
    def convert_to_zip(self):        
        '''        
        This method converts the mod in-place to an archive
        based mod.
        '''              
        if self.is_zip():
            return #do nothing if I'm a zip
        archive_name='{}.zip'.format(self.name.strip())
        old_folder=re.findall(r'\npath=(.+)\n',self.raw)[0].replace('"','').replace("'",'').replace("mod/",'')
        folder_path=os.path.join(self.path,old_folder)
        output_path=os.path.join(self.path,archive_name)
        #zip up everything        
        parent_folder = self.path
        # Retrieve the paths of the folder contents.
        contents = os.walk(folder_path)
        try:
            zip_file = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)
            for root, folders, files in contents:
                # Include all subfolders, including empty ones.
                for folder_name in folders:
                    absolute_path = os.path.join(root, folder_name)
                    relative_path = absolute_path.replace(parent_folder + '\\',
                                                        '')
                    print "Adding '%s' to archive." % absolute_path
                    zip_file.write(absolute_path, relative_path)
                for file_name in files:
                    absolute_path = os.path.join(root, file_name)
                    relative_path = absolute_path.replace(parent_folder + '\\',
                                                        '')
                    print "Adding '%s' to archive." % absolute_path
                    zip_file.write(absolute_path, relative_path)
            print "'%s' created successfully." % output_path
        except IOError, message:
            print message
            sys.exit(1)
        except OSError, message:
            print message
            sys.exit(1)
        except zipfile.BadZipfile, message:
            print message
            sys.exit(1)
        finally:
            zip_file.close()        
        #change raw to point to the new zip
        self.raw=re.sub(r'\n(path=.+)\n','\narchive="{}"\n'.format(archive_name),self.raw)        
        #remove folder
        folder_to_remove=os.path.join(self.path,old_folder)
        print folder_to_remove
        #shutil.rmtree(folder_to_remove)

        
    def is_zip(self):        
        if 'archive=' in self.raw.replace(' ',''):            
            return True
        return False

    def replaces_landed_titles(self):
        if 'common/landed_titles' in self.raw:
            return True
        if 'common\landed_titles' in self.raw:
            return True
        return False

    def replaces_minor_titles(self):
        if 'common/minor_titles' in self.raw:
            return True
        if 'common\minor_titles' in self.raw:
            return True
        return False

    def replaces_on_actions(self):
        if 'common/on_actions' in self.raw:
            return True
        if 'common\on_actions' in self.raw:
            return True
        return False

    def replaces_job_actions(self):
        if 'common/job_actions' in self.raw:
            return True
        if 'common\job_actions' in self.raw:
            return True
        return False

    def replaces_traits(self):
        if 'common/traits' in self.raw:
            return True
        if 'common\traits' in self.raw:
            return True
        return False

    def replaces_buildings(self):
        if 'common/buildings' in self.raw:
            return True
        if 'common\buildings' in self.raw:
            return True
        return False

    def replaces_event_modifiers(self):
        if 'common/event_modifiers' in self.raw:
            return True
        if 'common\event_modifiers' in self.raw:
            return True
        return False

    def replaces_common(self):
        if 'replace_path = "common"' in self.raw:
            return True
        return False

    def __str__(self):
        return self.name    

    def update(self, vanilla):
        '''
        this method takes a vanilla CK2Info and updates its findings based on whats there
        Note that it is lazy, and only updates things that vanilla instance has parsed.
        '''         
        if self._updated:
            self.clear()
        if not self.replaces_buildings() and len(vanilla._buildings) > 0:
            # buildings are merged, with the mod building overriding the base
            # building.
            self._buildings=merge_replace(vanilla.buildings(),self.buildings())

        if not self.replaces_event_modifiers() and len(vanilla._event_modifiers)>0:
            self._event_modifiers=merge_replace(vanilla.event_modifiers(),self.event_modifiers())

        if not self.replaces_minor_titles() and vanilla.parsed_minor_titles():
            self._minor_titles=merge_replace(vanilla.minor_titles(),self.minor_titles())

        if not self.replaces_traits() and vanilla._traits:
            # traits are merged, but both versions exists.  Confusing.
            self._traits = vanilla.traits() + self.traits()

        # on action merging is disabled for now because it's complicated, per the wiki:
            # Merge. Since patch 2.4 on_actions are merged, with new events being added to
            # the previous list for a particular on_action, instead of replacing the list.
        # if not self.replaces_on_actions() and vanilla._on_actions:
            #self._on_actions=vanilla.on_actions() + self.on_actions()
            
        if vanilla._technology and len(self.technology())==0:
            #for now I'm going to assume that no merging of the technology.txt file happens, and
            #that mods that define a technology.txt completely replace vanilla tech.
            self._technology=vanilla._technology

        if not self.replaces_job_actions() and vanilla._job_actions:
            # the wiki does not apparently know how job actions work, so going
            # to assume they are merged until I know otherwise.
            self._job_actions=merge_replace(vanilla.job_actions(),self.job_actions())

        if not self.replaces_landed_titles() and vanilla._landed_titles:
            # landed titles are complicated, but I think this is actually correct fro them
            # since all exist, and duplicates get merged (sort of) at the title
            # level.
            self._landed_titles = vanilla.landed_titles() + \
                self.landed_titles()
                
        self._localizations = ComboDict(
            vanilla.localizations(), self.localizations())
        self._updated = True
