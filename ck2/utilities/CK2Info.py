
import parse_functions as pf                        


class CK2Info:

    def __init__(self, name, path):
        self.name = name
        self.path = path
        self.clear()

    def clear(self):
        # lazily parse the mod and get info.
        self._localizations = []
        self._buildings = []
        self._traits = []
        self._interface = []
        self._on_actions = []
        self._landed_titles = []
        self._job_actions = []
        self._technology = []
        self._event_modifiers=[]
        self._minor_titles=[]    

    def parsed_job_actions(self):
        if self._job_actions:
            return True
        return False

    def job_actions(self):
        if not self.parsed_job_actions():
            self._job_actions = pf.parse_job_actions(self.path)
        return self._job_actions

    def parsed_minor_titles(self):
        if self._minor_titles:
            return True
        return False

    def minor_titles(self):
        if not self.parsed_minor_titles():
            self._minor_titles = pf.parse_minor_titles(self.path)
            loca = self.localizations()
            for minor in self._minor_titles:
                if loca.has_key(minor.name()):
                    try:
                        minor.localized_names['en'] = loca[minor.name()][0]
                        minor.localized_names['fr'] = loca[minor.name()][1]
                        minor.localized_names['de'] = loca[minor.name()][2]
                        minor.localized_names['es'] = loca[minor.name()][4]
                    except:
                        print 'weird localization for minor:',loca[minor.name()]
        return self._minor_titles

    def parsed_buildings(self):
        if self._buildings:
            return True
        return False

    def buildings(self):
        if not self.parsed_buildings():
            #print self.name,'parsing buildings at',self.path
            self._buildings = pf.parse_buildings(self.path)
            loca = self.localizations()
            tech = self.technology()
            tech_names = tuple([x.name() for x in tech])
            for building in self._buildings:
                if loca.has_key(building.name()):
                    building.localized_names['en'] = loca[building.name()][0]
                    for key,offset in [('fr',1),('de',2),('es',4)]:
                        if len(loca[building.name()])>offset+1:
                            building.localized_names[key] = loca[building.name()][offset]                    
                        else:
                            print "{} offset for {} failed, using english".format(key,building.name())
                            building.localized_names[key] = loca[building.name()][0]                    
                    #building.localized_names['fr'] = loca[building.name()][1]
                    #building.localized_names['de'] = loca[building.name()][2]
                    #building.localized_names['es'] = loca[building.name()][4]
                building._technology = tech_names
        return self._buildings

    def parsed_localizations(self):
        if self._localizations:
            return True
        return False

    def localizations(self):
        if not self.parsed_localizations():
            self._localizations = pf.parse_localization(self.path)
        return self._localizations

    def parsed_technology(self):
        if self._technology:
            return True
        return False

    def technology(self):
        if not self._technology:
            self._technology=pf.parse_technology(self.path)
        return self._technology

    def parsed_on_actions(self):
        if self._on_actions:
            return True
        return False

    def on_actions(self):
        if not self._on_actions:
            self._on_actions = pf.parse_onactions(self.path)
        return self._on_actions

    def parsed_landed_titles(self):
        if self._landed_titles:
            return True
        return False

    def landed_titles(self):
        if not self._landed_titles:
            self._landed_titles = pf.parse_titles(self.path)
        return self._landed_titles

    def parsed_interface(self):
        if self._interface:
            return True
        return False

    def interface(self):
        if not self._interface:
            self._interface = pf.parse_interface(self.path)
        return self._interface

    def parsed_traits(self):
        if self._traits:
            return True
        return False

    def traits(self):
        if not self._traits:
            self._traits = pf.parse_traits(self.path)
        return self._traits
    
    def parsed_event_modifiers(self):
        if self._event_modifiers:
            return True
        return False

    def event_modifiers(self):
        if not self._event_modifiers:
            self._event_modifiers = pf.parse_event_modifiers(self.path)
        return self._event_modifiers
