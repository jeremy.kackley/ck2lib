
# might be faster to just have a parent table?
# then i just update that...
# but it makes retreival slightly complicated; since it would require a join.
# at BEST this is N queries per block; so it's going to be slow.


class CK2SQLBlocks(CK2SQLCompContainer):
    insert_query_str = "INSERT INTO blocks VALUES (?,?,?,?,?)"

    def __init__(self, root):
        CK2SQLCompContainer.__init__(self, root)
        self.table_name = 'blocks'
        self.tuple_class = self.__make_block__
        self.type = 3
        self.create_index()
        self.__parent_queue__ = []

    def flush(self):
        """flushes the insert buffer to database."""
        # first, force a conventional flush of myself
        CK2SQLCompContainer.flush(self)
        # force a flush on my other containers...
        for thing in self.root.__containers__:
            if thing != self:
                thing.flush()
        # now add logic to flush parents.
        for _id, contents in self.__parent_queue__:
            self.set_parents(contents, _id)
        self.__parent_queue__ = []

    def __make_block__(self, *blah):
        t = list(blah)
        t.append(self)
        return NewBlock(*t)

    def __create_tables__(self):
        self.execute('''CREATE TABLE blocks
             (id INTEGER PRIMARY KEY UNIQUE,
             name text,
             comment text,
             start INTEGER,
             end INTEGER)''')

    def add(self, name, contents=None, comment=None, start=None, end=None):
        _id = self.__increment__()
        # print _id, name, repr(contents)
        self.__queue_insert__(_id, (_id, name, comment, start, end))
#         try:
#             self.execute("INSERT INTO blocks VALUES (?,?,?)",
#                   (_id, name,
#                    comment))
#         except:
#             self.execute("INSERT INTO blocks VALUES (?,?,?)",
#                   (_id, self.__encode__(name),
#                    self.__encode__(comment)))
#         self.__do_type_insert__(_id)
        if contents:
            self.__parent_queue__.append((_id, contents))
            # self.set_parents(contents, _id)
#             for child in contents:
#                 c.execute('''insert into children values
#                         (?,?)''',
#                         (_id, child))
            # this is the same speed, practically...
            # for thing in self.root.__containers__:
            # thing.set_parents(contents, _id)
            # as this.
            # tweak this!
#             query = "Select c_id, c_type from component_types WHERE c_id"
#             if len(contents) > 1:
#                 c.execute(query + ' in' + str(tuple(contents)))
#             else:
#                 c.execute(query + '=?', tuple(contents))
#             by_type = {}
#             for cid, ctype in c:
#                 if ctype not in by_type:
#                     by_type [ctype] = []
#                 by_type [ctype].append(cid)
#             for ctype, cids in by_type.iteritems():
#                 self.root.__type_lookup__[ctype].set_parents(cids, _id)

        return _id

    def find(self, name=None, comment=None, parent=None, limit=None):
        query = self.__get_find_query__((('name', name),
                                         ('comment', comment)), parent)
        return self.__do_find__(query, limit)

    def find_blockswith(self, name=None, comment=None, limit=None):
        query = self.__get_find_blocks_query__((('name', name),
                                                ('comment', comment)))
        return self.__do_find__(query, limit)