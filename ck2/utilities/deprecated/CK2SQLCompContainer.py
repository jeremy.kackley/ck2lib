# this is probably fast enough; and totally can't beat the storage.
#   insertions are a bit slow; iterating seems
#   fast enough
#
#   all non-empty blocks
#   SELECT DISTINCT * from blocks where blocks.id IN (
#       select parent_id from variables where variables.parent_id IS NOT NULL)
#
#   blocks with variable aname
#   SELECT DISTINCT * from blocks where blocks.id IN (
#       select parent_id from variables where variables.name=aname)

#   blocks named attribute with a literal
#   SELECT DISTINCT * from blocks where blocks.name == 'attribute'
#       AND
#        blocks.id IN (
#          select parent_id from literals where literal.parent_id IS NOT NULL)

# how to handle block 'building blocks' since I don't want to deal with
#    scads of queries every time I return a BLOCK...
#    maybe SQLPrimitives and regular Primitives;
#        and a mechanism for translating...e.g.
#            import / export of components.
#
#    maybe store the relationships separately?
#    table parents id parent_id
#        since every table has a fricking parent.
#    and create a unique ID for each..
#
#    also, maybe I can break this out into subclasses.
#    e.g. ck2sql.comments.add
#         ck2sql.comments.__iter__
#         ck2sql.comments.find
#         ck2sql.comments.findall
#    why not just look up the position (and parent) later, if needed
#        e.g. postions (id,position)
#             parent (id,parent_id)


#    group together X inserts
#        and then do a bulk insert.
#  INSERT INTO 'tablename'
#       SELECT 'data1' AS 'column1', 'data2' AS 'column2'
# UNION SELECT 'data3', 'data4'
# UNION SELECT 'data5', 'data6'
# UNION SELECT 'data7', 'data8'

class CK2SQLCompContainer:

    def __init__(self, root):
        self.root = root
        self.table_name = None
        self.tuple_class = None
        self.type = None
        try:
            self.__create_tables__()
        except:
            pass
        self.__insert_buffer__ = []
        self.__insert_ids__ = []
        # self.create_index()

    def flush(self):
        """flushes the insert buffer to database."""
        if self.__insert_buffer__:
            c = self.get_cursor()
#             if hasattr(self, 'bulk_insert_str'):
#                 inserts = len(self.__insert_buffer__)
#                 union = self.bulk_insert_union * (inserts - 1)
#                 try:
#                     c.execute(self.bulk_insert_str + union,
#                           tuple(itertools.chain(*self.__insert_buffer__)))
#                 except:
#                     t = self.bulk_insert_str + union
#                     print 'what the fuck!'
#                     print len(tuple(itertools.chain(*self.__insert_buffer__)))
#                     print self.type, inserts, t.count('?')
# raise#sqlite3.OperationalError: too many SQL variables
#             else:
            # print '\n'.join((str(x) for x in self.__insert_buffer__))
            c.executemany(self.insert_query_str,
                          self.__insert_buffer__)
            c.executemany('INSERT into component_types values (?,' + str(self.type) + ',NULL)',
                          [(x,) for x in self.__insert_ids__])
            self.__insert_buffer__ = []
            self.__insert_ids__ = []

    def __queue_insert__(self, _id, values):
        if not any(values):
            print 'what the hey?', values
        self.__insert_buffer__.append(values)
        self.__insert_ids__.append(_id)
        if len(self.__insert_buffer__) == 500:
            self.flush()

    @lru_cache()
    def get_cursor(self):
        return self.root.conn.cursor()

    def execute(self, *vars):
        # print repr(vars)
        return self.get_cursor().execute(*vars)

    def count(self):
        c = self.execute("Select number from counts WHERE table_name=?",
                         (self.table_name,))
        return c.fetchone()[0]

    def __do_type_insert__(self, _id):
        self.execute(
            'INSERT into component_types values (?,' + str(self.type) + ',NULL)', (_id,))

    def __increment__(self):
        # cnt = self.count()
        # self.execute("UPDATE counts SET number=? WHERE table_name=?",
                     # (cnt + 1, self.table_name))
        self.root.position += 1
        _id = self.root.position
        return _id

    @lru_cache()
    def __get_query__(self):
        select = 'select ' + self.table_name + '.*, component_types.parent_id'
        join = ' INNER JOIN component_types ON '
        on = self.table_name + '.id=component_types.c_id'
        query = select + ' from ' + self.table_name + join + on
        return query

    def __make_where__(self, values):
        where = []
        for column, value in values:
            if value:
                if hasattr(value, '__iter__'):
                    value = ', '.join(("'" + str(value) + "'"
                                       for x in value))
                else:
                    value = "'" + str(value) + "'"
                where.append(" {}.{}={}".format(self.table_name,
                                                column,
                                                value))
        return where

    def __get_find_query__(self, values, parent):
        where = self.__make_where__(values)
        if parent:
            where.append(' component_types.parent_id={} '.format(parent))
        if where != []:
            return self.__get_query__() + ' WHERE ' + 'AND'.join(where)
        return None

    def __do_find__(self, query, limit=None, tuple_class=None):
        if query:
            if not tuple_class:
                tuple_class = self.tuple_class
            if limit:
                query = query + ' LIMIT {}'.format(limit)
            c = self.execute(query)
            # print 'executed'
            if limit == 1:
                # print query + ' LIMIT 1'
                res = c.fetchone()
                if res:
                    return tuple_class(*res)
            else:
                return [tuple_class(*x) for x in c]
        return None

    def __get_find_blocks_query__(self, values):
        where = self.__make_where__(values)
        if where:
            query = "SELECT parent_id from component_types where c_id"
            where = ' WHERE ' + 'AND'.join(where)
            instr = " IN (SELECT id from " + self.table_name + where + ')'
            blocksel = " SELECT blocks.*,NULL from blocks where id IN ("
        #   SELECT parent_id from component_types where c_id IN
        #        (SELECT id from comments where ???)

            return self.root.blocks.__get_query__(
            ) + ' WHERE id IN (' + query + instr + ')'
        return None

    def __iter__(self):
        c = self.execute(self.__get_query__())
        return (self.tuple_class(*x) for x in c)

    def get(self, _id):
        where = ' where ' + self.table_name + '.id=?'
        query = self.__get_query__()
        c = self.execute(query + where + ' LIMIT 1',
                         (_id,))
        res = c.fetchone()
        if res:
            return self.tuple_class(*res)
        return res

    def get_several(self, ids):
        if len(ids) < 2:
            return [self.get(ids[0])]
        where = ' where ' + self.table_name + '.id IN ' + repr(tuple(ids))
        query = self.__get_query__() + where
        c = self.execute(query)
        return [self.tuple_class(*x) for x in c]

    def set_parents(self, _ids, pid):
        updatestr = "UPDATE component_types SET parent_id=? WHERE c_id"
        if len(_ids) > 1:
            self.execute(updatestr + " in " + str(tuple(_ids)), (pid,))
        elif len(_ids) == 1:
            self.execute(updatestr + '=?', (pid, _ids[0]))

    def __encode__(self, text):
        if text:
            try:
                return str(text, "latin1")
            except:
                try:
                    return str(text, "utf-8")
                except:
                    print "failed to decode ", text
                    raise

    def get_root(self):
        query = self.__get_query__()
        c = self.execute(query + '''
                where component_types.parent_id IS NULL''')
        return (self.tuple_class(*x) for x in c)

    # @lru_cache()
    def get_children(self, pid):
        query = self.__get_query__()
        c = self.execute(query + '''
                where component_types.parent_id=?''', (pid,))
        return (self.tuple_class(*x) for x in c)

    def create_index(self):
        return
#         rindex = '''CREATE INDEX {} ON {} (id)
#                 WHERE parent_id IS NULL
#                 '''.format(self.table_name + '_r',
#                            self.table_name)
#         print rindex
        # self.execute(rindex)
#         cindex = '''CREATE INDEX {} ON {} ( parent_id)
#             '''.format(self.table_name + '_c',
#                                                       self.table_name)
#         self.execute(cindex)

    @lru_cache()
    def __make_select__(self, where, orderby=None):
        query = 'select * from ' + self.table_name + ' where ' + where
        if orderby:
            query += 'ORDER BY ' + orderby
        return query

    def __create_tables__(self):
        # this guy should be implemented by subclasses
        return

# might be unable to make this one generic.
#     def add(self, vars):
#         _id = self.root.__inc_count__(self.table_name)
#         insert_str = "INSERT INTO {} VALUES ({})".format(
#             self.table_name, self.value_string)
#         self.execute(insert_str, self.__make_value_tuple__(*vars))
#         return _id