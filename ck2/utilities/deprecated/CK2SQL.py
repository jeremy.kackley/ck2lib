
# use sqlalchemy orm instead?
class CK2SQL:

    def __init__(self, dbfile=':memory:'):
        self.position = 0  # e.g. insert position
        self.conn = sqlite3.connect(dbfile)
        self.conn.text_factory = str
        # self.conn = sqlite3.connect('')
        # self.conn.isolation_level =None
        self.comments = CK2SQLComments(self)
        self.literals = CK2SQLLiterals(self)
        self.variables = CK2SQLVars(self)
        self.blocks = CK2SQLBlocks(self)
        self.__containers__ = (self.comments,
                               self.literals,
                               self.variables,
                               self.blocks)

        c = self.conn.cursor()
        c.execute(' PRAGMA count_changes=OFF')
        c.execute(' PRAGMA default_cache_size=16384;')
        if dbfile == ':memory:':
            c.execute(' PRAGMA journal_mode=MEMORY;')
            c.execute(' PRAGMA synchronous=OFF')
        # c.execute(' PRAGMA locking_mode=EXCLUSIVE;')
       # c.execute(' PRAGMA page_size = 4096')
        # c.execute(' PRAGMA cache_size = 16384')
        # probably should move parent_id to types(or another table)
        #    and index it.
        #    this would greatly speed up get_children, and wouldn't
        #    terribly affect the dedicated oens (still a single query)
        #        select * from table where c_type = type and parent_id = id
        #    and
        #        select * from table where parent_id = id
        try:
            c.execute('''CREATE TABLE component_types
                 (c_id INTEGER PRIMARY KEY  UNIQUE ,
                  c_type INTEGER,
                 parent_id INTEGER)''')
    #         c.execute('''CREATE TABLE children
    #              (id INTEGER,
    #              child_id INTEGER)''')
    #         c.execute('''CREATE INDEX children_idx
    #          ON children (id)''')
            c.execute('''CREATE INDEX component_types_pidx
             ON component_types (parent_id)''')
        except:
            pass
        # c.execute('''CREATE TABLE counts
            # (table_name text PRIMARY KEY  UNIQUE , number INTEGER)''')


#
#     def get(self, _id):
#         where = ' where ' + self.table_name + '.id=?'
#         query = self.__get_query__()
#         c = self.execute(query + where + ' LIMIT 1',
#                           (_id,))
#         res = c.fetchone()
#         if res:
#             return self.tuple_class(*res)
#         return res

        casewhens = ''
        self.__type_lookup__ = {}
        for thing in self.__containers__:
            self.__type_lookup__[thing.type] = thing
            # c.execute("INSERT INTO counts VALUES (?,0)", (thing.table_name,))
#             where = ' where ' + thing.table_name + '.id=cid LIMIT 1'
#             casewhens += "WHEN c_type = {} THEN {}({}.*,parent_id) {};\n".format(
#                 thing.type, thing.__getfuncname__,
#                 thing.table_name, where)
#
#         self.__children_query = """
#         SELECT   "Object" =
#             CASE
#                 """ + casewhens + """
#             END
#         FROM component_types;
#         ORDER BY c_id;
#         """
#         print self.__children_query

    # this is an optimization for calling code to generate
    #    a table out of blocks with common-ish
    #    values
    # make_table(characters,'name',birth_name,father...',characters)
    def make_table_from_blocks(self, name, columns, blocks):
        # consider grouping my updates and using
        #    executemany
        mask = 'CREATE TABLE {} (id INTEGER PRIMARY KEY  UNIQUE , block_name text, {})'
        rows = ','.join((x + ' text' for x in columns))
        print mask.format(name, rows)
        c = self.conn.execute('DROP TABLE IF EXISTS {}'.format(name))
        c.execute(mask.format(name, rows))
        updatemask = 'UPDATE {} SET  {}={} where id={}'
        # now do the data insertion
        for thing in blocks:
            c.execute('INSERT into {} (id) VALUES ({})'.format(name, thing.id))
            nameupdate = updatemask.format(
                name,
                'block_name',
                thing.name,
                thing.id)
            c.execute(nameupdate)
            # this works for variables...
            for child in itertools.chain(thing.variables()):
                if child.name in columns:
                    if child.value:
                        value = child.value
                        if value == 'yes':
                            value = '"yes"'
                        update = updatemask.format(name,
                                                   child.name,
                                                   value,
                                                   thing.id)
                        # print update
                        # print update
                        c.execute(update)
            # how shall I handle blocks?
            #    i could store the string...
            #    but probably better to not.
        # c.execute('select * from {}'.format(name))
        # for x in c:
            # print x
        self.conn.commit()

    def flush(self):
        for thing in self.__containers__:
            thing.flush()

    # might be I could make a better str....
    #    since I know I will print *everything* perhaps
    #        I should grab whole tables;
    #        less queries and such.
    def __str__(self):
        ret = ''
        for a in self.get_root():
            ret += str(a)
        return ret

    def pretty(self):
        return pretty(self.get_root())
        # return pretty(ret)

    def report(self):
        c = self.conn.cursor()
        for name in ['comments',
                     'literals',
                     'variables',
                     'blocks']:
            c.execute('SELECT COUNT(*) FROM ' + name)
            print name, c.fetchone()[0]

    # def __ordered_generator__(self, iterators):
        # for itr in iterators:

        # generator that ...something.
        #    caches things temporarily, so
        #    that the id's are always sequential.
        # return
    # ooh I know...
    #    make a user function
    #    to return the object...
    #    e.g. get_object(id,type)
    # it might be faster to serialize the
    #    children of blocks by type.
    def __return_sorted_values__(self, c):
        types = {}
        for aid, atype in c:
            if atype not in types:
                types[atype] = []
            types[atype].append(aid)
        ret = []
        for atype in types:
            ret += self.__type_lookup__[atype].get_several(types[atype])
        # print repr(ret)
        return sorted(ret, key=lambda x: x.id)

    # twould be expensive, but I could always store serialized objects...
    def get_children(self, _id):
        c = self.conn.cursor()
#         c.execute('''select children.child_id,component_types.c_type
#                  from children INNER JOIN component_types
#                  ON children.child_id=component_types.c_id
#                  where children.id=?
#                  ORDER BY children.child_id
#                  ''',
#                   (_id,))
        c.execute('select c_id,c_type from component_types where parent_id=?',
                  (_id,))
        return self.__return_sorted_values__(c)
        types = {}
        for aid, atype in c:
            if atype not in types:
                types[atype] = []
            types[atype].append(aid)
        ret = []
        for atype in types:
            ret += self.__type_lookup__[atype].get_several(types[atype])
        # print repr(ret)
        return sorted(ret, key=lambda x: x.id)
        # return (self.__type_lookup__[atype].get(aid)
        # for aid, atype in c)
        # iters = (x.get_children(_id)
        # for x in self.__containers__)
        # return sorted(itertools.chain(*iters), key=lambda x: x.id)

    # this works, but could be better...
    def get_root(self):
        c = self.conn.cursor()
        c.execute('''select c_id,c_type
        from component_types where parent_id IS NULL''')
        return self.__return_sorted_values__(c)
        return (self.__type_lookup__[atype].get(aid)
                for aid, atype in c)
#         iters = (x.get_root()
#                  for x in  self.__containers__)
#         return sorted(itertools.chain(*iters), key=lambda x: x.id)
# c.execute('''Select * from
# variables
# where parent_id IS NULL
# ORDER BY position''')
# variables=[x for x in c]
# c.execute('''Select * from
# blocks
# where parent_id IS NULL
# ORDER BY position''')
# blocks=[x for x in c]