
# save space:
# list (or similar) of strings at class level for
#     variable_names and block_names
#
# e.g. dict[id]=name dict[name]=id;
#
# transparently replace this with the string at retrieval
#
# store this index in the database in a speical table.
#
# other potential candidates:
# values #most overlap
#     literals
# comment_strings #least
#
# the downside: storing a good-chunk of the data in memory.


class CK2SQLVars(CK2SQLCompContainer):
    insert_query_str = "INSERT INTO variables VALUES (?,?,?,?)"
    bulk_insert_str = """
            INSERT INTO variables
            SELECT ? AS 'id',? as 'name', ? AS 'value', ? as 'comment'
            """
    bulk_insert_union = " UNION SELECT ?,?,?,? "

    def __init__(self, root):
        CK2SQLCompContainer.__init__(self, root)
        self.table_name = 'variables'
        self.tuple_class = NewVariable
        self.type = 2
        self.create_index()
        self.integer_index = {}

    # def tuple_class(self, *blah):
        # blah[1] = self.integer_index[blah[1]]
        # return NewVariable(*blah)

    def __create_tables__(self):
        self.execute('''CREATE TABLE variables
                 (id INTEGER PRIMARY KEY  UNIQUE,
                 INTEGER text,
                 value text,
                 comment text)''')

    def add(self, name, value, comment=None):
        # if name not in self.integer_index
        _id = self.__increment__()
        self.__queue_insert__(_id, (_id, name, value, comment))
#         try:
#             self.execute("INSERT INTO variables VALUES (?,?,?,?)",
#                   (_id,
#                    name,
#                    value,
#                    comment))
#         except:
#             self.execute("INSERT INTO variables VALUES (?,?,?,?)",
#                   (_id,
#                    self.__encode__(name),
#                    self.__encode__(value),
#                    self.__encode__(comment)))
#         self.__do_type_insert__(_id)
        return _id

    def find(self, name=None, value=None,
             comment=None, parent=None, limit=None):
        query = self.__get_find_query__((('name', name),
                                         ('value', value),
                                         ('comment', comment)), parent)
        return self.__do_find__(query, limit)

    def find_blockswith(self, name=None, value=None, comment=None, limit=None):
        query = self.__get_find_blocks_query__((('name', name),
                                                ('value', value),
                                                ('comment', comment)))
        # print query
        return self.__do_find__(query, limit, self.root.blocks.tuple_class)
