
class CK2SQLLiterals(CK2SQLCompContainer):
    insert_query_str = "INSERT INTO literals VALUES (?,?,?)"
    bulk_insert_str = """
            INSERT INTO literals
            SELECT ? AS 'id', ? AS 'value', ? as 'comment'
            """
    bulk_insert_union = " UNION SELECT ?,?,? "

    def __init__(self, root):
        CK2SQLCompContainer.__init__(self, root)
        self.table_name = 'literals'
        self.tuple_class = NewLiteral
        self.type = 1
        self.create_index()

    def __create_tables__(self):
        self.execute('''CREATE TABLE literals
             (id INTEGER PRIMARY KEY  UNIQUE,
             value text,
             comment text)''')

    def add(self, value, comment=None):
        _id = self.__increment__()
        self.__queue_insert__(_id, (_id, value.strip(), comment))
#         try:
#             self.execute("INSERT INTO literals VALUES (?,?,?)",
#                          (_id,
#                           value,
#                           comment
#                           ))
#         except:
#             self.execute("INSERT INTO literals VALUES (?,?,?)",
#                          (_id,
#                           self.__encode__(value),
#                           self.__encode__(comment)
#                           ))
#         self.__do_type_insert__(_id)
        return _id

    def find(self, value=None, comment=None, parent=None, limit=None):
        query = self.__get_find_query__((('value', value),
                                         ('comment', comment)), parent)
        return self.__do_find__(query, limit)

    def find_blockswith(self, value=None, comment=None, limit=None):
        query = self.__get_find_blocks_query__((('value', value),
                                                ('comment', comment)))
        return self.__do_find__(query, limit, self.root.blocks.tuple_class)
