


class CK2SQLComments(CK2SQLCompContainer):
    # only need one instance of these per class.
    insert_query_str = "INSERT INTO comments VALUES (?,?)"
    bulk_insert_str = """
            INSERT INTO comments
            SELECT ? AS 'id', ? AS 'value'
            """
    bulk_insert_union = " UNION SELECT ?,?"

    def __init__(self, root):
        CK2SQLCompContainer.__init__(self, root)
        self.table_name = 'comments'
        self.tuple_class = NewComment
        self.type = 0
        self.create_index()

    def __create_tables__(self):
        self.execute('''CREATE TABLE comments
            (id INTEGER PRIMARY KEY  UNIQUE,
            value text)''')

    # if i make an insert, I need to merge _id into the query...
    def add(self, comment):
        _id = self.__increment__()
        self.__queue_insert__(_id, (_id, comment.strip()))
        # try:
        # self.execute("INSERT INTO comments VALUES (?,?)",
        #            (_id, comment))
        # except:
        #   self.execute("INSERT INTO comments VALUES (?,?)",
        # (_id, self.__encode__(comment)))
        return _id

    def find(self, value=None, parent=None, limit=None):
        query = self.__get_find_query__([('value', value)], parent)
        return self.__do_find__(query, limit)

    def find_blockswith(self, value=None, limit=None):
        query = self.__get_find_blocks_query__([('value', value)])
        return self.__do_find__(query, limit, self.root.blocks.tuple_class)
