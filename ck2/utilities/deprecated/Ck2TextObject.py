
class Ck2TextObject(Ck2Object):

    def __init__(self, __text):
        Ck2Object.__init__(self)
        __text = __text.replace('\n', ' ')
        self.text = __text

    def __str__(self):
        return str(self.text) + '\n'

    def __len__(self):
        return len(self.text)
