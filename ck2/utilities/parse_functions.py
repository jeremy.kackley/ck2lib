import os
import re

from Parser import Parser

from .. import constructs
from .. import defines


def verify(string, parsed=None):
    """An attempt to write a verification function.

    Suspect that comments break it up.

    """
    __p = Parser()
    if not parsed:
        __p.parse(string)
    else:
        __p.pre_hash = __p.__compress_string__(string)
        __p.post = parsed
    return __p.verify()


def parse(string):
    __p = Parser()
    return __p.parse(string)


# this is just broken, I need to reconsider how to do this.


def __get_other_children__(block, item):
    newa = [other for other in block if other != item]
    return newa


def __merge_item_into_block__(block, item):
    # get all items that are not the item
    newa = __get_other_children__(block, item)
    block.update([x for x in item] + newa)


def __remove_empty_or_singleton__(code):
    mask2 = 'removing empty or singleton {} block.'
    for __type in ['AND', 'OR']:
        for block in code.findall_b(__type):
            if len(block) < 2:
                if block.__parent__ is not None:
                    if VERBOSE_SIMPLIFY:
                        print mask2.format(block.__name__)
                    # print block
                    __merge_item_into_block__(block.__parent__, block)


def __remove_redundancy__(code):
    """report/eliminate redundant limits and scopes.

    (OR->OR; AND->AND; NOT->NOT)

    """
    mask = 'removing redundant block: {}->{}'
    for inner, outer in [
        ['AND', 'AND'],
        ['OR', 'OR'],  # or or can safely be combined
        ['NOT', 'NOT'],  # not not = yes
    ]:
        for block in code.findall_b(outer):
            for item in block:
                if hasattr(item, '__name__') and item.__name__ == inner:
                    if VERBOSE_SIMPLIFY:
                        print mask.format(outer, inner)
                    __merge_item_into_block__(block, item)
                    # newa = __get_other_children__(block, item)
                    # block.update([x for x in item] + newa)
                    # newa = __get_other_children__(block.__parent__, block)
                    # block.__parent__.update([x for x in block] + newa
                # this checks for and inside any block other than an or.
                #    all other scopes default to and.
    for block in code.getall_b():
        if block.__name__ != 'OR':
            for item in block:
                if hasattr(item, '__name__') and item.__name__ == 'AND':
                    if VERBOSE_SIMPLIFY:
                        print mask.format(block.__name__, item.__name__)
                    __merge_item_into_block__(block, item)


#                    newa = [other for other in block if other != item]
# __update_block_contents__(block, item, newa)


def __root_to_top__(code):
    """attempt move ROOT restrictions to top most level.

    #this works, but is very likely to break code and is disabled
    by default.

    e.g.
        potential -> any_vassal -> ROOT = {a}
    would become potential -> and{a} any_vassal

    OR interrupts this, though, so
        potential -> or -> any_vassal -> root = {a}
    would become potential -> or -> a any_vassal;
    though
        potential -> or -> and -> any_v -> root {a}
    would instead be
        potential -> or -> and -> a any_v

    """
    mask = 'Moving ROOT block to parent {} block.'
    for rootscope in ['potential']:
        for block in code.findall_b('ROOT'):
            top = block
            inside_not = False
            while top.__parent__ is not None and top.__name__.lower() != 'or':
                top = top.__parent__
                # print 'new top', top.__name__, top.__parent__
                if top.__name__.lower() == 'not':
                    inside_not = True
                is_rootscope = top.__name__.lower() == rootscope
                is_or = top.__name__.lower() == 'or'
                parent_is_or = top.__parent__ is not None and top.__parent__.__name__.lower(
                ) != 'or'
                is_and_in_or = top.__name__.lower() == 'and' and parent_is_or
                is_or = is_or or is_and_in_or

                if is_rootscope or is_or:
                    if VERBOSE_SIMPLIFY:
                        print mask.format(top.__name__)
                    new_contents = [x for x in block]
                    # root={} is implicitly an and.
                    new_contents = [AND(*new_contents)]
                    # if i'm inside a NOT block; I need
                    #    to add a NOT to me.
                    if inside_not:
                        new_contents = [NOT(*new_contents)]

                    # add to rootscope
                    top.update(new_contents + [x for x in top])
                    # remove from immediate parent
                    newpc = __get_other_children__(block.__parent__, block)
                    block.__parent__.update(newpc)
                    break


def __remove_duplicate_checks__(code):
    """attempts to remove identical checks within the same scope."""
    for block in code.getall_b():
        mask = 'Removing duplicate {} from {} block.'
        culled = []
        for thing in block:
            if thing not in culled:
                # this only adds multiples..got it.
                for __t in range(0, block.count(thing) - 1):
                    culled.append(thing)
        if culled != []:
            for thing in culled:
                if VERBOSE_SIMPLIFY:
                    print mask.format(repr(thing)[:10] + '...', block.__name__)
                block.remove(thing)
    return code


# for the rest; a better understanding of parsed syntax is necessary; e.g.
#       need to drastically modify the tree...
#   RootToTop:  if in potential block, ROOT restrictions can be directly under potential
#               in effect, it's trickier...
# def root_to_top(parsed_syntax):

# more generic idea:
# write a parser; parse blocks and assignments, keeping track of scope changes and restrictions.
# ROOT (limit) -> Any vassal (limit)
# GROUP_LIMITS: move common restrictions to a limit block (e.g. any_vassal -> OR(AND(A,B)AND(A,C) = any_vassal(lim(A),OR(B,C))
# ADD_BRANCH: if many sub-blocks have common elements, consider breaking into multiple blocks. (e.g. 2 or more any_vassals)
# pick 'most common elements
# EXTRA_CHECKS: e.g. not = {has_title lady} and is_female = no
# fuzzy on what I was thinking; possibly that has_title = lady requires is_female = yes.
# #
# for each operation:
# check length before/after, abort if size increases.


def simplify(asdf):
    if isinstance(asdf, type('')):
        asdf = parse(asdf)
    code = CK2CODE(asdf)
    prelen = len(pretty(code))

    # first modify the parse tree
    # __root_to_top__(code)
    decreasing = True
    cur_len = prelen
    while decreasing:

        # now remove any __string-existing redundancy
        #    AND any created redundancy.
        __remove_redundancy__(code)
        __remove_empty_or_singleton__(code)
        # do this at the end, since when
        #    things move around, they tend
        #    to become outright duplicates.
        __remove_duplicate_checks__(code)
        new_len = len(pretty(code))
        if new_len < cur_len:
            cur_len = new_len
        else:
            decreasing = False

    postlen = len(pretty(code))
    print 'Removed {} characters from file.'.format(prelen - postlen)
    return code.__contents__


# i can probably cook up a simplier parser/maker.  maybe.
#   I think some arbitrary tags are always {}'s though.
#       maybe if contents is a string, it's an assignment, if
#       contents is a block, it has {}
#           I always try to parse the string, and if it has no curlies
#           the parse treats it as an assignment.


def parse_localization(tdir):
    ret = {}
    locadir = os.path.join(tdir, 'localisation')
    files = parse_files_in_dir(locadir)
    for afile in files:
        # print afile,files[afile]
        if isinstance(files[afile], dict):
            ret.update(files[afile])
    return ret


def parse_traits(tdir, verify_parse=False):
    ret = []
    traitdir = os.path.join(tdir, 'common', 'traits')
    if os.path.exists(tdir):
        files = parse_files_in_dir(traitdir, verify_parse=verify_parse)
        for afile in files:
            # print afile,files[afile]
            for athing in files[afile]:
                if isinstance(athing, constructs.BLOCK):                    
                    trait = constructs.Trait(athing)
                    ret.append(trait) 
    return ret

def parse_titles(tdir, verify_parse=False):
    ret = []
    traitdir = os.path.join(tdir, 'common', 'landed_titles')
    files = parse_files_in_dir(traitdir, verify_parse=verify_parse)
    for afile in files:
        # print afile,files[afile]
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                ret.append(athing)
    return ret


def parse_onactions(tdir, verify_parse=False):
    ret = []
    parser = Parser()
    parser.is_on_action_file = True
    traitdir = os.path.join(tdir, 'common', 'on_actions')
    files = parse_files_in_dir(
        traitdir, verify_parse=verify_parse, parser=parser)
    for afile in files:
        # print afile,files[afile]
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                ret.append(athing)
    return ret


def parse_job_actions(tdir, verify_parse=False):
    ret = []
    parser = Parser()
    parser.is_on_action_file = True
    traitdir = os.path.join(tdir, 'common', 'job_actions')
    files = parse_files_in_dir(
        traitdir, verify_parse=verify_parse, parser=parser)
    for afile in files:
        # print afile,files[afile]
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                ret.append(constructs.JobAction(athing))
    return ret


def parse_buildings_raw(tdir, verify_parse=False):
    ret = []
    traitdir = os.path.join(tdir, 'common', 'buildings')
    files = parse_files_in_dir(traitdir, verify_parse=verify_parse)
    for afile in files:
        # print afile,files[afile]
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                ret.append(athing)
    return ret


def parse_minor_titles(tdir, verify_parse=False):
    minors = []
    tdir = os.path.join(tdir, 'common', 'minor_titles')    
    files = parse_files_in_dir(tdir, verify_parse=verify_parse)
    for afile in files:
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                minor = constructs.MinorTitle(athing)
                minors.append(minor)
    return minors


def parse_event_modifiers(tdir, verify_parse=False):
    event_modifiers = []
    tdir = os.path.join(tdir, 'common', 'event_modifiers')
    files = parse_files_in_dir(tdir, verify_parse=verify_parse)
    for afile in files:
        # print afile,files[afile]
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                event_modifier = constructs.EventModifier(athing)
                event_modifiers.append(event_modifier)
    return event_modifiers


def parse_buildings(tdir, verify_parse=False):
    buildings_found = []
    for building_type in parse_buildings_raw(tdir):
        for raw_building in building_type:
            if isinstance(raw_building, constructs.BLOCK):
                building = constructs.Building(building_type.__name__,
                                               raw_building)
                buildings_found.append(building)
    return buildings_found


def parse_interface(tdir, verify_parse=False):
    ret = []
    traitdir = os.path.join(tdir, 'interface')
    files = parse_files_in_dir(traitdir, verify_parse=verify_parse)
    for afile in files:
        # print afile,files[afile]
        for athing in files[afile]:
            if isinstance(athing, constructs.BLOCK):
                ret.append(athing)
    return ret


def __fix_dir__(tdir):
    if tdir[-1:] != '\\':
        tdir = tdir + '\\'
    return tdir


def strip_comments_from_string(string):
    # printed = []
    while '#' in string:
        comment = '#' + string.partition('#')[2].partition('\n')[0]
        # if comment not in printed:
        # print 'killing', comment
        # printed.append(comment)
        string = string.replace(comment, '')
    return string


def parse_technology(tdir, verify_parse=False):
    ret = []
    traitdir = os.path.join(tdir, 'common')
    contents = parse_file(traitdir, 'technology.txt')
    if contents:
        for thing in contents:
            if isinstance(thing, constructs.BLOCK):
                group = thing.__name__
                for tech in thing:
                    if isinstance(tech, constructs.BLOCK):
                        ret.append(constructs.Technology(group, tech))
    return ret


def parse_files_in_dir(tdir, recurse=False, verify_parse=False, parser=None):
    tdir = __fix_dir__(tdir)
    ret = {}
    if os.path.exists(tdir):
        for __a, __b, files in os.walk(tdir):
            for afile in files:
                main = parse_file(tdir, afile, parser, verify_parse)
                if main:
                    ret[afile] = main
        if not recurse:
            return ret
    else:
        if defines.NOTIFY_MISSING_DIRECTORIES:
            print "Path does not exist.", tdir
    return ret


def parse_file(tdir, afile, parser=None, verify_parse=None):
    main = None
    target = os.path.join(tdir, afile)
    target = str(target)
    # print target
    # this shouldn't be necessary, but I had a situation
    #    where os.walk was reporting a nonexistant
    #    file, for some reason.  This prevents a crash.
    if os.path.exists(target):
        with open(target) as _af:
            __text = _af.read()
            # print tdir+afile
            if '.csv' in afile:
                main = parse_csv(__text)
            else:
                #            print 'parsing',tdir+afile
                # I have no earthly idea why this messes with my
                #    parser.
                __text = __text.replace('#1.5y\n', '\n')
                try:
                    if parser:
                        main = parser.parse(__text)
                    else:
                        main = parse(__text)
                except:
                    print 'error parsing file', afile
                    raise
                if verify_parse:
                    if not verify(__text, main):
                        print tdir, afile, 'failed to verify'
    return main


def parse_csv(__text):
    main = {}
    lines = re.split('\n', __text)
    for line in lines:
        asdf = re.split(';', line)
        main[asdf[0]] = asdf[1:]
    return main
