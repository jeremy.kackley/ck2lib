import re
import itertools
import traceback
import shlex

from ..defines import LINES_PARSED

from ..constructs.basic.Comment import Comment
from ..constructs.basic.Assignment import ASSIGNMENT
from ..constructs.basic.Block import BLOCK
from ..constructs.basic.Literal import Literal

# novel idea:
#    make the parser parse different value types;
#    e.g. recognize ints and floats

# Comment edge cases
#
# k_ushu_tandai = {
#     color={ 128 152 155 } # Minatonezumi
#     color2={ 220 220 220 }
#
#     title = "Ushu-Tandai"
#     foa = "Ushu-Tandai_FOA"
#     short_name = yes
#     location_ruler_title = yes
# #    dynasty_title_names = no
#
#     # Controls a religion
# #    controls_religion =
#
#     # Cannot be held as a secondary title
#     primary = yes
#       desc = "#blah"
#      desc = "adsfa#asdf"
#  # asdf "1"
#  desc = "asdf"  #bob
# }
#
#             c_otter_tail-pelican = {# 370
#                 color = { 196 149 147 }
#
# #    capital = # Kyoto

# this is a recipe from itertools wiki page
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return itertools.izip(a, b)

class Parser:

    def __init__(self, report_progress=False, auto_hash=False):
        self.is_on_action_file = False
        self.report_progress = report_progress
        self.auto_hash = auto_hash
        # default for now, until I get around
        #    to implementing searchs
        self.CUSTOM_POST = False
        self.USE_SQL = False
        self.__clear__()
        self.__compile_res__()

    def __compile_res__(self):
        self.__re_quot_cbracket = re.compile('[\'|"]}[\'|"]')
        self.__re_quote_obracket = re.compile('[\'|"]{[\'|"]')
        # self.__re_quot_cbracket = re.compile('["]}["]')
        # self.__re_quote_obracket = re.compile('"]{["]')
        self.__re_wp_tokens = {}
        for sym in '=\n{}':
            self.__re_wp_tokens[sym] = re.compile('[ \t]*' + sym + '[\t\n ]*')
        self.__re_ws_bet_eq_brkt = re.compile('[\n \t]=[ \t\n]+{')
        # self.__re_comment = re.compile('[^"\'](#.*\n)|^(#.*\n)')
        # (?:[^"\'])(#[^"\'\n]*\n)
        # self.__re_comment = re.compile('((?:[^"\'])#.*\n|#.*\n)')
        # self.__re_comment = re.compile('^.*(#.*\n)', flags=re.M)
        # self.__re_comment = re.compile('^.*(#.*?\n)', flags=re.M)
        self.__re_comment = re.compile('^.*?(#.*\n)', flags=re.M)
        self.__re_blockname = re.compile('([_.\w\']+)[ ]*=')
        self.__re_squote = re.compile('(")')
        self.__re_dquote = re.compile("(')")
        self.__re_eq_or_wp = re.compile('=| ')
        self.__re_newline = re.compile('\n')
        self.__re_block_opening_wsp = re.compile('=\s+{')
        # (?x) free spacing mode might simplify these... e.g. (?x)={}
        # these both match blocks e.g x = { }
        # self.__re_blocksplit = re.compile('([_.\w]+[ \t\n\r]*=[ \t\n\r]*{|})')
        self.__re_blocksplit = re.compile('([_.\w\']+\s*=\s*{|})')
        # self.__re_blocksplit = re.compile('(?x)(={|})')
        # self.__re_blockfind_itr = re.compile('([_.\w]+[ \t\n\r]*=[ \t\n\r]*{|})')
        self.__re_blockfind_itr = re.compile('([_.\w\']+\s*=\s*{|})')
        # self.__re_blockfind_itr = re.compile('(?x)(={|})')

    def __clear__(self):
        self.__bstack = []
        self.__comments = []
        self.__string = ''
        self.__slice_offset = 0
        self.pre_hash = None
        if not self.CUSTOM_POST:
            if self.USE_SQL:
                self.post = CK2SQL()
            else:
                self.post = []
        self.__assignment_replacements = []
        self.parse_progress = 0
        self.report_list = range(0, 100, 10)
        # last point in file that we found a block; implicitly
        self.offset = 0
        self.last_block_offset = 0
        self.last_comment_processed = 0
        self.slicesize = None

    def __remove_quoted_brackets__(self):
        self.__string = self.__re_quot_cbracket.sub('\vC\v', self.__string)
        self.__string = self.__re_quote_obracket.sub('\vO\v', self.__string)

    def __sanitize_string__(self, string):
        string = str(string)
        string = string.replace('\t', ' ')
        # string=string.replacestring('\n',' ')
        string = string.replace('  ', ' ')
        string = string.replace('\n ', '\n')
        string = string.replace(' \n', ' \n')
        # string = string.replace('\n{', '{')
        string = self.__re_block_opening_wsp.sub('={', string)
        for sym in self.__re_wp_tokens:
            string = self.__re_wp_tokens[sym].sub(sym, string)
        # remove all whitespace and new lines between
        #  = and {
        string = self.__re_ws_bet_eq_brkt.sub('={', string)
        return string

    # if i know the offset, I can store the actual position
    #    and not the relative position.
    # if I do that, i need to convert the starting point to
    #    the actual position, e.g. relative + offset
    #    need to remove comments without changing file-size.
    def __process_comments_iter__(self):
        """utilizes finditer to find all the comments within the string."""
        comment_str = self.__string
        if self.offset > 0:
            relative = self.last_comment_processed - self.offset
            if relative > 0:
                comment_str = comment_str[relative:]
                print self.offset, self.last_comment_processed, relative
        new_comments = []
        # this is retarded, but an easy way to ensure the file
        comment_str += '\n'
        # doesn't end in a comment
        replacements_to_do = []
        for a in self.__re_comment.finditer(comment_str):
            pre = comment_str[a.start():a.start(1)]
            cnt = pre.count('"')
            if cnt == 0:
                cnt = pre.count('\'')
            if not cnt % 2 or cnt == 0:
                # if first:
                # first = False
                # print repr(self.__string[0:a.start()])
                if a.start() + self.offset >= self.last_comment_processed:
                    a_comment = comment_str[a.start(1) + 1:a.end(1)].rstrip()
                    new_comments.append(
                        a_comment)
                    replacements_to_do.append((a.start(1), a.end(1)))
                    # print 'comment:', a.start(), a.end()
                    self.last_comment_processed = a.start(1) + self.offset
            # print repr(self.__string[a.start():a.end()])
        # if b:
            # b.start(), len(self.__string)
        # self.__comments_pos = self.comments + new_positions
        for a, b in replacements_to_do:
            a_comment = comment_str[a:b]
            pre = self.__string[:a]
            if self.__string[a] != '#':
                if len(self.__string[a].strip()) > 0:
                    print repr(self.__string[a]), repr(a_comment)
                    pre = pre + self.__string[a]
            post = self.__string[b:]
            replacement_text = '#' * (len(a_comment) - 1) + '\n'
            self.__string = pre + replacement_text + post
            # print a_comment, a, b, len(a_comment), len(replacement_text)
        # print self.__string
        new_comments.reverse()
        # print new_comments
        self.__comments = self.__comments + new_comments
#         for comment in new_comments:
#             replacement = '#' * (len(comment))
#             # print 'replacing ', comment, 'with', replacement
#             # don't fuck with the newlines and shit.
#             # if replacement > 2:
#             # replacement = replacement[:-2] + '\n'
#             # print comment, replacement
#             # self.__string = self.__string.replace(comment, replacement, 1)
#             # prestr = self.__string
#             # self.__string = self.__re_comment.sub(replacement, self.__string, 1)
#             # pre_cnt = self.__string.count('#' + comment)
#             self.__string = self.__string.replace(
#                 '#' +
#                 comment,
#                 replacement,
#                 1)
        # if prestr == self.__string:
        # print 'failed to replace: ', repr(comment), repr(replacement)
        # if '#' + comment in self.__string:
        # print 'why no work', repr('#' + comment), pre_cnt

        # self.__string = self.__re_comment.sub('#COMMENT\n', self.__string)

    def __parse_comments__(self):
        self.__comments = self.__re_comment.findall(self.__string)
        self.__string = self.__re_comment.sub('#COMMENT\n', self.__string)
        self.__comments = [x[1:] for x in self.__comments]
        self.__comments.reverse()

    def __parse_open_block__(self, block):
        try:
            name = self.__re_blockname.findall(block)[0]
            # print 'block is named ', name
        except:
            # print 'no block name?'
            name = ' '
            pass
        # self.__bstack.append(BLOCK(name, None, self.__cur_comment))
        tmp = []
        self.__bstack.append([name,
                              tmp,
                              self.__cur_comment,
                              self.start])
        self.__cur_comment = None

    def __sanitize_assign_line__(self, line):
        self.__assignment_replacements = []
        quoted_things = self.__re_squote.split(line)
        quoted_things += self.__re_dquote.split(line)
        # remove empty strings
        quoted_things = [x for x in quoted_things if x != '']
        while len(quoted_things) >= 4:
            self.__assignment_replacements.append(''.join(quoted_things[1:4]))
            quoted_things = quoted_things[4:]
        for repl in self.__assignment_replacements:
            line = line.replace(repl, '#REPLACED#')
        self.__assignment_replacements.reverse()
        return line

    def unsanitize_assign_line__(self, line):
        while '#REPLACED#' in line:
            line = line.replace('#REPLACED#',
                                self.__assignment_replacements.pop())
        return line

    def __replace_quoted_brackets__(self, string):
        string = string.replace('\vO\v', '"{"')
        string = string.replace('\vC\v', '"}"')
        return string

    def __process_assignment_statements_shlex__(self, line):
        """my custom code is faster..."""
        line = self.__replace_quoted_brackets__(line)
        name = []
        value = []
        comments = []
        lexer = shlex.shlex(line)
        lexer.wordchars += '.-'
        isval = False
        for token in lexer:
            if token != '=':
                if not isval:
                    name.append(token)
                    isval = True
                else:
                    value.append(token)
                    isval = False
                    comments.append('')
        # if len(comments) > 0:
        comments.pop()
        comments.append(self.__cur_comment)
        for a, b, c in itertools.izip(name, value, comments):
            if c:
                self.__make_var(a, b, c)
            else:
                self.__make_var(a, b)
            # self.add_to_open_block(var)
        self.__cur_comment = None

    def add_to_open_block(self, thing):
        if self.__bstack != []:
            self.__bstack[-1:][0][1].append(thing)
        else:
            if not self.USE_SQL:
                # list is empty, i need to go ahead and add it to the out
                self.post.append(thing)

    def __make_var(self, name, value, comment=None):
        if self.USE_SQL:
            var = self.post.variables.add(name.strip(), value.strip(), comment)
        else:
            var = ASSIGNMENT(name.strip(), value.strip(), comment)
        self.add_to_open_block(var)

    def __process_assignment_statements__(self, line):
        """Takes a line with multiple a = b and turns it into assignments"""
        # return self.__process_assignment_statements_shlex__(line)
        line = self.__sanitize_assign_line__(line)
        pas = self.__re_eq_or_wp.split(self.__replace_quoted_brackets__(line))
        pas = [x for x in pas if x != '']
        # print pas
        for __a, __b in itertools.izip(
                itertools.islice(pas, 0, len(pas) - 2, 2),
                itertools.islice(pas, 1, len(pas) - 2, 2)):
            __b = self.unsanitize_assign_line__(__b)
            self.__make_var(__a, __b)
        __a = pas[len(pas) - 2]
        __b = pas[len(pas) - 1]
        __b = self.unsanitize_assign_line__(__b)
        self.__make_var(__a, __b, self.__cur_comment)
        self.__cur_comment = None

    def __no_open_blocks_on_bstack__(self):
        """Checks for any open blocks on stack, and if none are found, returns
        whatever is on the stack."""
        dumpable = []
        for athing in self.__bstack:
            if isinstance(athing, BLOCK):
                if not athing.__closedbyparser__:
                    return None
            else:
                dumpable.append(athing)
        return dumpable

    def __process_close_block__(self):
        """This handles } which close out blocks."""
        # first we figure outw hat assigns there are on the stack
        # self.__assigns = []
        # assigns is now the list of completed statements
        #    bstack is now a list of tuples

        if len(self.__bstack) > 0:
            # if len< 1 then it means misplaced brackets
            top = self.__bstack.pop()
            top.append(self.end)
            # print 'closing: ', top, self.__assigns, self.__bstack
            # curlies
            top[1].reverse()
            if self.USE_SQL:
                newb = self.post.blocks.add(*top)
            else:
                newb = BLOCK(top[0], top[1], top[2])
#
#             if hasattr(self, 'pritneverything'):
#                 if self.pritneverything < 155:
#                     print 'closing', top[0], [x[0]for x in self.__bstack], self.start, self.end, self.last_block_in_slice()
#             if top[0] in [
# 'character',
# '160258',
#                           '1000183970',
# '1000183972'
#                           ]:
#                 self.pritneverything = 0
#                 print 'closing ', top[0]
#                 print 'BSTACK', [x[0]for x in self.__bstack]
#                 print 'LBIS/LBIF:', self.last_block_in_slice(), self.last_block_in_file()
#                 print 'theslice', repr(self.theslice)
#                 print 'start:', self.start, self.start + self.offset
#                 print 'end:', self.end, self.end + self.offset
#                 print 'SLICESIZE:', self.slicesize
#                 print repr(self.__string[self.start - 267:self.end + 267])
            self.add_to_open_block(newb)

    def __process__comment_line__(self, line):
        # print self.__comments
        # print 'lineline:', line
        # if len(self.__comments) < 1:
            # print 'lineline:',line
        if len(self.__comments) > 0:
            self.__cur_comment = self.__comments.pop()
        # there is some weirdness here.
        # if len(self.__cur_comment) != line.count('#'):
        # print 'whaa?', repr(self.__cur_comment), repr(line),
        # len(self.__cur_comment), line.count('#')
        # if self.is_on_action_file:
        #    while '##' in line:
        #        line = line.replace('##', '#')
        # else:
        # print line
        line = line.replace('#', '')
        return line

    def __process_literal__(self, line):
        if self.USE_SQL:
            new_literal = self.post.literals.add(line, self.__cur_comment)
        else:
            comment = Comment(self.__cur_comment, None)
            new_literal = Literal(line, comment, None)
        self.add_to_open_block(new_literal)
        self.__cur_comment = None

    def __process_line__(self, block, line):
        # print line
        #         if hasattr(self, 'pritneverything'):
        #             if self.pritneverything < 155:
        #                 self.pritneverything += 1
        #                 print '{} {} {}: {}'.format(self.pritneverything, self.end,
        # self.last_block_in_slice(), repr(line))

        global LINES_PARSED
        LINES_PARSED += 1
        self.__cur_comment = None
        processed = False
        # print line
        if '#' in line:
            line = self.__process__comment_line__(line)
        if '{' in line:
            processed = True
            self.__parse_open_block__(block)
            line = line.partition('{')[2]
        if '=' in line:
            processed = True
            # self.__process_assignment_statements__(line)
            self.__process_assignment_statements_shlex__(line)  # shlex works
        if '}' in line:
            processed = True
            self.__process_close_block__()
        if not processed and line.replace(' ', '') != '':
            processed = True
            self.__process_literal__(line)
        if self.__cur_comment and not processed:
            # if all else fails, append cur_comment
            if self.USE_SQL:
                comment = self.post.comments.add(self.__cur_comment)
            else:
                comment = Comment(self.__cur_comment, None)
            self.add_to_open_block(comment)

    def __process_block__(self, block):
        # print self.__bstack, self.__assigns
        if block != '':
            # print 'block:', repr(block)
            for line in self.__re_newline.split(block):
                # print 'line:' , repr(line)
                line = line.strip()
                if line:
                    self.__process_line__(block, line)

    def __culling_string_slicer__(self, start, end):
        """this deletes used parts of the string as they are 'finished with'
        not used, though it works identically to __process_blocks_iter__"""
        original_end = end
        start = start - self.__slice_offset
        end = end - self.__slice_offset
        if end < 0:
            end = len(self.__string)
        self.__process_string_slice__(start, end)

        self.__slice_offset = original_end
        self.__string = self.__string[end:]

    def __update_progress__(self, end):
        self.parse_progress = end / float(len(self.__string))
        if self.report_progress:
            percent = int(self.parse_progress * 100)
            if percent in self.report_list:
                print '{} percent complete.'.format(percent)
                self.report_list.remove(percent)

    def __process_string_slice__(self, start, end):
        """Takes begin and ending positions and calls __process_block__ with
        the slice."""
        # store the 'absolute' file position of the last
        # complete block.
        if end <= self.last_block_in_slice():
            # don't reparse things we've already parsed...
            return
        if self.slicesize and start >= self.slicesize:
            # don't start blocks greater than the slicesize..
            return
#         if start < self.last_block_offset:
#             print "processing slice {} -> {}".format(start + self.offset,
#                                                  end + self.offset)
#             start = self.last_block_offset
        # self.last_block_offset = end
        self.last_block_offset = end
        theslice = self.__string[start:end]
#
        self.theslice = theslice
        self.start = self.offset + start
        self.end = self.offset + end
        # self.__process_block__(self.__sanitize_string__(theslice))
        # self.__update_progress__(end)
        # return
        theslice = self.__sanitize_string__(theslice)
        for block in self.__re_blocksplit.split(theslice):
            self.__process_block__(block)
        self.__update_progress__(end)

    def last_block_in_slice(self):
        return self.last_block_offset

    def last_block_in_file(self):
        return self.last_block_offset + self.offset

    def __process_blocks_iter__(self):
        """utilizes finditer to find all the 'blocks' within the string."""

        # the idea is start{ Start{ end} end} thus
        # the pairwise traversal.
        for a, b in pairwise(self.__re_blockfind_itr.finditer(self.__string)):
            if self.last_block_offset == 0:
                self.__process_string_slice__(0, a.start())
            try:
                self.__process_string_slice__(a.start(), b.start())
            except:
                print 'error parsing slice!!'
                print repr(self.__string[a.start():b.start()])
                print repr(self.__string[a.start():a.end()])
                print repr(self.__string[b.start():b.end()])
                print traceback.format_exc()
                raise
        # if b:
            # self.__process_string_slice__(b.start(), len(self.__string))

    def parse_slice(self, string, offset, slicesize):
        # print "NEW SLICE BITCHES", self.last_block_offset, slicesize
        self.offset = offset
        self.last_block_offset = self.last_block_offset - slicesize
        if offset == 0:
            self.last_block_offset = 0
        self.slicesize = slicesize
        # i somehow need to calculate the slice...but it's hard, because I beat
        #    the shit out of the string.
        # if self.last_block_offset > 0:
        # string = string[self.last_block_offset:]
        self.__string = string
        # parsing comments beforehand preserves their spacing against manipulation
        # self.__parse_comments__()
        self.__process_comments_iter__()
        self.__remove_quoted_brackets__()

        self.__process_blocks_iter__()

        # print "EOS: ", self.last_block_in_slice(), self.last_block_in_file()

    def finish_parse(self):
        # grab the end of the string...
        self.__process_string_slice__(self.last_block_in_slice(),
                                      len(self.__string))
        self.__string = ''

        if self.post == []:
            return self.__bstack
        if len(self.__bstack) > 0:
            asdf = [(x[0], len(x[1])) for x in self.__bstack]
            print 'blocks were not closed, bad file?'
            print asdf
            if self.USE_SQL:
                while len(self.__bstack) > 0:
                    self.__process_close_block__()
            else:
                self.post = self.post + self.__bstack
# nice thought, but python does this automatically, basically.
#         if self.USE_SQL:
#             self.post.conn.execute('END TRANSACTION')
#             self.post.conn.isolation_level = self.__isolation_level
        if self.USE_SQL:
            self.post.flush()
            self.post.conn.commit()
        return self.post
    # ideas for acceleration:
        #   split 'main bit' by discrete blocks
        #   each 'block' is processed by a separate parser.

    def start_parse(self):
        self.__clear__()
# nice thought, but python does this automatically, basically.
#             self.__isolation_level = self.post.conn.isolation_level
#             self.post.conn.isolation_level=None
#             self.post.conn.execute('BEGIN')

    # lets re-think the chunk system...
    #    I don't really need overlapping chunks, do i?
    #    unless a block OPENS across chunks; or a variable
    #    = across it....
    #        this implies I should stop processing before the end
    #        of the string....
    def parse_file(self, afile, chunksize=None, verbose=False):
        mask = 'parsed chunk {} of {} containing {} bytes. ({:.2%} complete)'
        size = os.stat(afile).st_size
        slicesize = chunksize or (1024 * 1024)
        if slicesize > size:
            slicesize = 0
            total = 1
        else:
            total = size / slicesize + 1
        count = 0
        self.start_parse()
        if verbose:
            print 'parsing file {}'.format(afile)
        with open(afile) as _fp:
            offset = 0
            while offset <= size:
                desired = slicesize + 1000  # int(slicesize * 1.25)
                if offset + desired > size:
                    desired = size - offset
                # print '{:10}{:10}{:10}'.format(offset,
                    # offset + desired,
                    # offset + slicesize)
                raw = mmap.mmap(_fp.fileno(),
                                length=desired,
                                offset=offset,
                                access=mmap.ACCESS_READ)
                self.parse_slice(raw, offset, slicesize)
                if verbose:
                    print mask.format(
                        count,
                        total,
                        len(self.__string),
                        count / float(total))
                count += 1
                offset = count * slicesize
                if slicesize == 0:
                    break
        return self.finish_parse()

    def parse(self, string):
        # if self.__string == string:
            # return self.post
        if self.auto_hash:
            self.pre_hash = self.__compress_string__(string)

        self.start_parse()
        self.parse_slice(string, 0, len(string))
        return self.finish_parse()

    def __compress_string__(self, string):
        string = pretty(string)
        for sym in '\n\t "\'':
            string = string.replace(sym, '')
        return md5.md5(string)

    def verify(self, string=None):
        if string:
            self.pre_hash = self.__compress_string__(string)
        post = self.__compress_string__(self.post)
        if post.digest() == self.pre_hash.digest():
            return True
        # this code became redundant when i stopped
        #    hanging onto the 'before' string.
#         self.__diff = difflib.SequenceMatcher(a=string, b=post)
#         self.diff_string = []
#         for block in self.__diff.get_matching_blocks():
#             sp1 = block[0] + block[2]
#             sp2 = block[1] + block[2]
#             ads = (string[sp1:][:25], post[sp2:][:25])
#             self.diff_string.append(ads)
        return False
